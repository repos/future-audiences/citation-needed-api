# Version 0.0.17:

As version 0.0.16, with fix for a bug in selection the Lead Section, that would prevent it from being found, in place.

# Version 0.0.16:

Using API v2 with GPT 3.5 for section selection, GPT 4 for quote selection.

# Version 0.0.15:

As version 0.0.14, but with GPT 3.5

# Version 0.0.14:

The extended section selection (API v2), in Text mode with GPT 4, and weight based keyword selection. The used keyword extraction prompt is:

Propose the title of a wikipedia article that holds information to support or refute the provided claim. Provide a few keywords and synonyms that can be used to identify sections in arbitrary texts that hold information on the claim. You can use a weight after especially important keywords by adding a colon and a number. Use the json format {"search_term": "wikipedia page title", "keywords": ["important keyword:2", "not so important keyword"]}.

# Version 0.0.13:

The extended section selection (API v2), but in Text mode with GPT 4.

# Version 0.0.12:

As version 0.0.11, but with GPT 4.

# Version 0.0.11:

Retesting the regular retrieval strategy, now called API v1, but with Text based interaction instead of JSON mode.

# Version 0.0.10:

Retesting the regular retrieval strategy, now called API v1, with the same extended QA scores.

# Version 0.0.9:

Retesting the extended section retrieval strategy, now called API v2, with additional QA scores for evaluation:

- Accuracy Score, considering partially_correct as correct
- Red - Green Failure Score
- Red - Green Failure Score, considering partially_correct as correct
- No Result or Article Score
- Accuracy Score, ignoring no_result and no_articls results

The "API v2" has a modified keyword extraction prompt:

- Propose the title of a wikipedia article that holds information to support or refute the provided claim. Provide a few keywords and synonyms that can be used to identify sections in arbitrary texts that hold information on the claim. Use the json format {'search_term': 'wikipedia page title', 'keywords': ['keyword1', 'keyword2']}.

and does not use ChatGPT for section selection. Instead, it retrievs the full text wiki articles and runs a scoring algorithm on matched keywords for every section. It then presents the top three sections for the quote extraction.

# Version 0.0.8:

Testing an extended section retrieval strategy.

# Version 0.0.7:

Analaysis of default prompts with accuracy measurement in place.

- run on chatgpt-3.5-turbo
- seed parameter on 12345
- temperature parameter set to 0
- standard prompts as in v0.0.1

# Version 0.0.6:

Same run as v0.0.5, but with accuracy measurement in place.

# Version 0.0.5:

Run with temperature set to 0 and seed parameter set, modified prompts:

- run on chatgpt-3.5-turbo
- seed parameter on 12345
- temperature parameter set to 0
- modified prompts:

  - Extract or create a few keywords that can be used for a wikipedia search from the passed statement: they should resemble a Wikipedia article title as much as possible. Try to make your keywords as predictable as possible, to maximize the probability of creating the same set each time. Use the json format {'keywords': ['keyword1', 'keyword2']}. No explanation or summary.
  - Select the three most important sections from the provided tables of content that would hold the most relevant information to find a citation for the passed statement. Use the following json format: {'Page 1': ['Section 1', 'Section 2'],'Page 2': ['Section 3']}. No explanation or summary.
  - First, supply a verification result based on whether the statement is is supported by the provided text passages: 'correct' if the statement is supported, 'incorrect' if the statement is contradicted, or 'partially_correct' if only part of the statement is supported.

  Next, write a very short explanation of the verification result.

  Next, select a short, VERBATIM, quote from one of the text passages that is most relevant to the initial statement, and provides the best support for your verification result. If there is no quote in the sections that is strongly connected to the statement, return {'no_quote_selected': true}. Otherwise, use the following json format: {'selected_quote': 'Homeopathy is a pseudoscience, a belief that is incorrectly presented ...', result: 'correct', explanation: 'There are matching sources to the claim'}. No explanation or summary, max length of 300 characters.

# Version 0.0.4:

Run with temperature set to 0 and seed parameter set:

- run on chatgpt-3.5-turbo
- seed parameter on 12345
- temperature parameter set to 0
- standard prompts as in v0.0.1

# Version 0.0.3:

Run with temperature set to 0:

- run on chatgpt-3.5-turbo
- no seed parameter set
- temperature parameter set to 0
- standard prompts as in v0.0.1

# Version 0.0.2:

Run with active Seed parameter:

- run on chatgpt-3.5-turbo
- seed parameter on 12345
- no temperature parameter
- standard prompts as in v0.0.1

# Version 0.0.1:

Setup as-is:

- run on chatgpt-3.5-turbo
- no seed parameter
- no temperature parameter
- standard prompts:
  - Extract a few keywords that can be used for a wikipedia search from the passed statement. Use the json format {'keywords': ['keyword1', 'keyword2']}.
  - Select the three most important sections from the provided tables of content that would hold the most relevant information to find a citation for the passed statement. Use the following json format: {'Page 1': ['Section 1', 'Section 2'],'Page 2': ['Section 3']}.
  - Select a short, VERBATIM, quote from one of the text passages. Choose the segment that is most relevant to the initial statement. Suply a verifiction result, either 'correct', 'incorrect' or 'partially_correct' and a very short explanation on the verification result. If there is no quote in the sections that is strongly connected to the statement, return {'no_quote_selected': true}. Otherwise, use the following json format: {'selected_quote': 'Homeopathy is a pseudoscience, a belief that is incorrectly presented ...', result: 'correct', explanation: 'There are matching sources to the claim'}. Use a max length of 300 characters.
