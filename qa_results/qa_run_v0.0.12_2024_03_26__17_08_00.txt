
QA Run
======

Average Consistency Score: 92.12%
Average Accuracy Score: 49.39%
Average Accuracy Score, considering partially_correct as correct: 60.3%
Average Red - Green Failure Score: 1.82%
Average Red - Green Failure Score, considering partially_correct as correct: 1.82%
Average No Result or Article Score: 15.76%
Average Accuracy Score, ignoring no_result and no_articls results: 49.6%
Average Latency: 16.56s

Selection: As the father of endoscopy, Chevalier Q. Jackson removed thousands of swallowed objects from his patients.
Consistency Score: 80.0%
Accuracy Score: 80.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 80.0%
Average Latency: 12.77s
Prevelent Result: correct
Results: partially_correct, correct, correct, correct, correct, partially_correct, correct, correct, correct, correct
Errors: []

------

Selection: In the 19th century, an idea to move trains via air pumps was devised by Isambard Kingdom Brunel.
Consistency Score: 80.0%
Accuracy Score: 80.0%
Accuracy Score, considering partially_correct as correct: 80.0%
Red - Green Failure Score: 20.0%
Red - Green Failure Score, considering partially_correct as correct: 20.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 80.0%
Average Latency: 22.28s
Prevelent Result: correct
Results: correct, incorrect, correct, correct, correct, incorrect, correct, correct, correct, correct
Errors: []

------

Selection: The Times once printed a politician's speech without realizing that an obscene remark -- "the speaker then said he felt inclined for a bit of fucking" -- was inserted into the middle of it.
Consistency Score: 60.0%
Accuracy Score: 0.0%
Accuracy Score, considering partially_correct as correct: 0.0%
Red - Green Failure Score: 40.0%
Red - Green Failure Score, considering partially_correct as correct: 40.0%
No Result or Article Score: 60.0%
Accuracy Score, ignoring no_result and no_articls results: 0.0%
Average Latency: 34.68s
Prevelent Result: no_result
Results: incorrect, no_result, incorrect, no_result, no_result, incorrect, no_result, no_result, incorrect, no_result
Errors: []

------

Selection: In the 70s, you could buy a kit to assemble to a tiny toy version of a cannon that fired at noon, when the sun's rays were focused through a lens.
Consistency Score: 60.0%
Accuracy Score: 0.0%
Accuracy Score, considering partially_correct as correct: 0.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 40.0%
Accuracy Score, ignoring no_result and no_articls results: 0.0%
Average Latency: 11.75s
Prevelent Result: no_articles
Results: no_articles, no_articles, no_result, no_result, no_result, no_articles, no_result, no_articles, no_articles, no_articles
Errors: []

------

Selection: Children never change: as far back as Ancient Mesopotamia, a boy wrote letters to his mother trying to shame her into giving him better clothes to impress his peers.
Consistency Score: 100.0%
Accuracy Score: 0.0%
Accuracy Score, considering partially_correct as correct: 0.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 100.0%
Accuracy Score, ignoring no_result and no_articls results: 0%
Average Latency: 12.17s
Prevelent Result: no_result
Results: no_result, no_result, no_result, no_result, no_result, no_result, no_result, no_result, no_result, no_result
Errors: []

------

Selection: Louis Armstrong's "What a Wonderful World" used to be banned from radio air time following Sept 11.
Consistency Score: 100.0%
Accuracy Score: 100.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 100.0%
Average Latency: 15.75s
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: King Charles VIII's personal advisor once lived as a hermit in a cave on his father's estate.
Consistency Score: 100.0%
Accuracy Score: 100.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 100.0%
Average Latency: 19.78s
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: Lauren Boebert reimbursed herself in 2020 for roughly 39,000 miles traveling in her car.
Consistency Score: 100.0%
Accuracy Score: 0.0%
Accuracy Score, considering partially_correct as correct: 0.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 0.0%
Average Latency: 2.48s
Prevelent Result: no_articles
Results: no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles
Errors: []

------

Selection: In Lebanon, a common everyday hello is often in English, Arabic and French all in the same expression.
Consistency Score: 60.0%
Accuracy Score: 60.0%
Accuracy Score, considering partially_correct as correct: 60.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 10.0%
Accuracy Score, ignoring no_result and no_articls results: 66.67%
Average Latency: 28.62s
Prevelent Result: correct
Results: correct, correct, correct, no_result, correct, correct, correct
Errors: [BadRequestError('Error code: 400 - {\'error\': {\'message\': "This model\'s maximum context length is 8192 tokens. However, your messages resulted in 9645 tokens. Please reduce the length of the messages.", \'type\': \'invalid_request_error\', \'param\': \'messages\', \'code\': \'context_length_exceeded\'}}'), BadRequestError('Error code: 400 - {\'error\': {\'message\': "This model\'s maximum context length is 8192 tokens. However, your messages resulted in 9645 tokens. Please reduce the length of the messages.", \'type\': \'invalid_request_error\', \'param\': \'messages\', \'code\': \'context_length_exceeded\'}}'), BadRequestError('Error code: 400 - {\'error\': {\'message\': "This model\'s maximum context length is 8192 tokens. However, your messages resulted in 9451 tokens. Please reduce the length of the messages.", \'type\': \'invalid_request_error\', \'param\': \'messages\', \'code\': \'context_length_exceeded\'}}')]

------

Selection: Candy Raisins are made from jujube and are named after their appearance, rather than their flavor.
Consistency Score: 100.0%
Accuracy Score: 0.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 0.0%
Average Latency: 14.29s
Prevelent Result: partially_correct
Results: partially_correct, partially_correct, partially_correct, partially_correct, partially_correct, partially_correct, partially_correct, partially_correct, partially_correct, partially_correct
Errors: []

------

Selection: The inventor of the Segway, Dean Kamen, once bought an island called North Dumpling Island, before seceding from the United States and referring to himself as "Lord Dumpling II".
Consistency Score: 100.0%
Accuracy Score: 100.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 100.0%
Average Latency: 14.66s
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: A bug in the original 1991 Civilization game would cause Mahatma Gandhi to become increasingly aggressive to the point of using nuclear weapons.
Consistency Score: 90.0%
Accuracy Score: 0.0%
Accuracy Score, considering partially_correct as correct: 0.0%
Red - Green Failure Score: 0%
Red - Green Failure Score, considering partially_correct as correct: 0%
No Result or Article Score: 10.0%
Accuracy Score, ignoring no_result and no_articls results: 0.0%
Average Latency: 17.18s
Prevelent Result: incorrect
Results: incorrect, no_result, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect
Errors: []

------

Selection: Edgar Allan Poe died due to a ballot-stuffing scam known as cooping, where he was kidnapped and locked in a room and beaten in order to provide votes for a political party. 
Consistency Score: 100.0%
Accuracy Score: 0.0%
Accuracy Score, considering partially_correct as correct: 0.0%
Red - Green Failure Score: 0%
Red - Green Failure Score, considering partially_correct as correct: 0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 0.0%
Average Latency: 2.69s
Prevelent Result: no_articles
Results: no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles
Errors: []

------

Selection: The Bayside Canadian Railway is the world's shortest railway, measuring only 61m in total length.
Consistency Score: 100.0%
Accuracy Score: 0.0%
Accuracy Score, considering partially_correct as correct: 0.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 0.0%
Average Latency: 2.04s
Prevelent Result: no_articles
Results: no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles
Errors: []

------

Selection: A South African politician loved karate so much as a kid he got the nickname "Tokyo".
Consistency Score: 100.0%
Accuracy Score: 100.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 100.0%
Average Latency: 12.52s
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: If you're not careful when swimming at El Paredon beach, you could get caught in a rip current, which will drag you underwater and drown you.
Consistency Score: 100.0%
Accuracy Score: 0.0%
Accuracy Score, considering partially_correct as correct: 0.0%
Red - Green Failure Score: 0%
Red - Green Failure Score, considering partially_correct as correct: 0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 0.0%
Average Latency: 2.14s
Prevelent Result: no_articles
Results: no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles
Errors: []

------

Selection: The United States and Israel helped install Guatemalan leader, General Benedicto Lucas, who was responsible for the genocide of thousands of Mayans in the 1980s.
Consistency Score: 100.0%
Accuracy Score: 0.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 0.0%
Average Latency: 43.66s
Prevelent Result: partially_correct
Results: partially_correct, partially_correct, partially_correct, partially_correct, partially_correct, partially_correct, partially_correct, partially_correct, partially_correct, partially_correct
Errors: []

------

Selection: The Klobb was by far the most useless gun in Goldeneye, but I still kind of loved it for embracing chaos.
Consistency Score: 50.0%
Accuracy Score: 50.0%
Accuracy Score, considering partially_correct as correct: 50.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 50.0%
Average Latency: 9.84s
Prevelent Result: correct
Results: no_articles, correct, no_articles, correct, no_articles, no_articles, no_articles, correct, correct, correct
Errors: []

------

Selection: The term dreamcatcher is a bit misleading, as they were originally used by the Anishinaabeg to ward off harm and bad spirits, rather than having anything to do with dreams.
Consistency Score: 100.0%
Accuracy Score: 100.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 100.0%
Average Latency: 31.9s
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: Annette Eckis was infamously involved in a SeaWorld accident where she was bitten by the legs and dragged by Shamu after a stunt gone wrong, not having been informed that Shamu was triggered by bikinis.
Consistency Score: 100.0%
Accuracy Score: 0.0%
Accuracy Score, considering partially_correct as correct: 0.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 0.0%
Average Latency: 2.93s
Prevelent Result: no_articles
Results: no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles, no_articles
Errors: []

------

Selection: Claire Van Ummersen had an academic career primarily focused on the sciences, but also found time to learn French and Latin.
Consistency Score: 100.0%
Accuracy Score: 100.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 100.0%
Average Latency: 13.77s
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: Bruce Springsteen did not pay his taxes until he was put on the cover of Time Magazine in 1975. He then spent the next few years paying off his taxes, leaving him with only $20,000 on his 30th birthday despite multiple best-selling records and tours.
Consistency Score: 100.0%
Accuracy Score: 100.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 100.0%
Average Latency: 25.22s
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: a man under the pseudonym "Kirk Allen" who became deluded that a sci-fi book series was actually the story of his life. He filled in the blanks with elaborate details and hallucinated himself in those settings. He was treated by Robert Lindner, who himself became obsessed with the books.
Consistency Score: 100.0%
Accuracy Score: 0.0%
Accuracy Score, considering partially_correct as correct: 0.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 100.0%
Accuracy Score, ignoring no_result and no_articls results: 0%
Average Latency: 14.28s
Prevelent Result: no_result
Results: no_result, no_result, no_result, no_result, no_result, no_result, no_result, no_result, no_result, no_result
Errors: []

------

Selection: Tōxcatl, an annual Aztec festival which revolved around the sacrifice of a young man who had been impersonating their god Tezcatlipoca since the last Tōxcatl festival, and the selection of a new man to take that role in the year to come
Consistency Score: 100.0%
Accuracy Score: 100.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 100.0%
Average Latency: 18.28s
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: the 'Eddie Murphy Rule' in the Dodd-Frank Wall Street Reform Act which disallows secretly giving out doctored market information so as to corner a market using false information.
Consistency Score: 100.0%
Accuracy Score: 0.0%
Accuracy Score, considering partially_correct as correct: 0.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 100.0%
Accuracy Score, ignoring no_result and no_articls results: 0%
Average Latency: 9.52s
Prevelent Result: no_result
Results: no_result, no_result, no_result, no_result, no_result, no_result, no_result, no_result, no_result, no_result
Errors: []

------

Selection: Ruth Handler, creator of the Barbie doll, got her idea when she saw her daughter Barbara playing with paper dolls and giving them adult roles, but most of the dolls in the market were of infants.
Consistency Score: 100.0%
Accuracy Score: 100.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 100.0%
Average Latency: 20.34s
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: "Dune" was rejected by at least 20 publishers before being published by Chilton, the auto-manual company.
Consistency Score: 90.0%
Accuracy Score: 90.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 90.0%
Average Latency: 19.62s
Prevelent Result: correct
Results: correct, correct, correct, correct, partially_correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: Sonny Bono is the only member of Congress in American History to have scored a Number One Hit on the Billboard Hot 100, doing so with he and his ex-wife Chers 1965 Hit "I Got You Babe".
Consistency Score: 70.0%
Accuracy Score: 70.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 70.0%
Average Latency: 18.36s
Prevelent Result: correct
Results: partially_correct, correct, correct, correct, partially_correct, correct, correct, partially_correct, correct, correct
Errors: []

------

Selection: the mother of Lee Harvey Oswald would sometimes go to Dealey Plaza and sell her autograph for five dollars
Consistency Score: 100.0%
Accuracy Score: 100.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 100.0%
Average Latency: 14.88s
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: in 1556, during the execution of the Guernsey Martyrs by burning on the stake, Perotine Massey, one of the three women executed, gave birth to a son. The Bailiff Hellier Gosselin ordered this infant to be cast in the fire
Consistency Score: 100.0%
Accuracy Score: 100.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 100.0%
Average Latency: 24.24s
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: Dr Amy Bishop shot and killed three professors in a University of Alabama biology department meeting in 2010. It was found out later that she had shot and killed her brother in 1986 and was never charged.
Consistency Score: 100.0%
Accuracy Score: 100.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 100.0%
Average Latency: 28.65s
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: the song "Hello! Ma Baby", popularized in Merrie Melodies cartoons and Spaceballs, dates to 1899 and was written about a girlfriend the singer has met only through the telephone.
Consistency Score: 100.0%
Accuracy Score: 0.0%
Accuracy Score, considering partially_correct as correct: 100.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 0.0%
Accuracy Score, ignoring no_result and no_articls results: 0.0%
Average Latency: 15.92s
Prevelent Result: partially_correct
Results: partially_correct, partially_correct, partially_correct, partially_correct, partially_correct, partially_correct, partially_correct, partially_correct, partially_correct, partially_correct
Errors: []

------

Selection: there are 17.3 million American digital nomads or people that travel freely while working remotely using technology and the internet.
Consistency Score: 100.0%
Accuracy Score: 0.0%
Accuracy Score, considering partially_correct as correct: 0.0%
Red - Green Failure Score: 0.0%
Red - Green Failure Score, considering partially_correct as correct: 0.0%
No Result or Article Score: 100.0%
Accuracy Score, ignoring no_result and no_articls results: 0%
Average Latency: 9.1s
Prevelent Result: no_result
Results: no_result, no_result, no_result, no_result, no_result, no_result, no_result, no_result, no_result, no_result
Errors: []

------
