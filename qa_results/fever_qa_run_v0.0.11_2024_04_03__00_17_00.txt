Accuracy: 0.3888888888888889

Incorrect Results:

Claim: Frank Sinatra's middle name was Albert.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': "The claim is incorrect as there is no mention of Frank Sinatra's middle name being Albert in the provided text.", 'article': {'url': 'http://en.wikipedia.org/wiki/Frank_(given_name)?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Frank_(given_name)?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Frank_(given_name)?wprov=cna1#Lead_Section', 'title': 'Frank (given name)', 'section': 'Lead Section', 'snippet': 'Frank Sinatra (1915–1998), American singer and actor', 'references': 1, 'contributors': 346, 'last_updated': '2024-03-27 22:07'}}

Claim: Samuel L. Jackson was not in the M. Night Shyamalan film Unbreakable.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'Samuel L. Jackson played the character Elijah Price/Mr. Glass in the film Unbreakable.', 'article': {'url': 'http://en.wikipedia.org/wiki/Unbreakable_(film)?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Unbreakable_(film)?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Unbreakable_(film)?wprov=cna1#Cast', 'title': 'Unbreakable (film)', 'section': 'Cast', 'snippet': 'Samuel L. Jackson as Elijah Price/Mr. Glass, a comic book theorist, and deranged domestic terrorist with brittle bone disease', 'references': 65, 'contributors': 1360, 'last_updated': '2024-04-02 11:25'}}

Claim: George W. Bush was a president.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Katie Holmes refused to ever become an actress.
Label: REFUTES
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Planet of the Apes' filming took place between May 21 and August 10, 1967.
Label: NOT ENOUGH INFO
Result: SUPPORTS
Result details: {'result': 'correct', 'explanation': 'The filming of Planet of the Apes did indeed take place between May 21 and August 10, 1967, as stated in the source.', 'article': {'url': 'http://en.wikipedia.org/wiki/Planet_of_the_Apes_(1968_film)?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Planet_of_the_Apes_(1968_film)?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Planet_of_the_Apes_(1968_film)?wprov=cna1#Filming', 'title': 'Planet of the Apes (1968 film)', 'section': 'Filming', 'snippet': 'Filming began on May 21, 1967, and wrapped on August 10.', 'references': 99, 'contributors': 1453, 'last_updated': '2024-04-01 22:10'}}

Claim: Inhumans is developed only for ABC.
Label: REFUTES
Result: NOT ENOUGH INFO
Result details: {'result': 'correct', 'explanation': 'The series Inhumans was developed in conjunction with ABC Studios and was intended to air on ABC.', 'article': {'url': 'http://en.wikipedia.org/wiki/Inhumans_premiere?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Inhumans_premiere?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Inhumans_premiere?wprov=cna1#Production', 'title': 'Inhumans premiere', 'section': 'Production', 'snippet': 'In November 2016, Marvel Television and IMAX Corporation announced the eight-episode television series Inhumans, to be produced in conjunction with ABC Studios and air on ABC', 'references': 151, 'contributors': 76, 'last_updated': '2024-03-31 06:03'}}

Claim: Pixar uses CGI animation to create films.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'no_result', 'explanation': 'No matching sections found.'}

Claim: Laurence Olivier received American film awards.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The Laurence Olivier Awards were established in his honor, not American film awards.', 'article': {'url': 'http://en.wikipedia.org/wiki/Laurence_Olivier_Awards?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Laurence_Olivier_Awards?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Laurence_Olivier_Awards?wprov=cna1#Lead_Section', 'title': 'Laurence Olivier Awards', 'section': 'Lead Section', 'snippet': 'In 1984, British actor Laurence Olivier gave his consent for the awards to be renamed in his honour and they became known as the Laurence Olivier Awards.', 'references': 15, 'contributors': 316, 'last_updated': '2024-03-24 14:30'}}

Claim: Bill Clinton was never acquitted by the U.S. Senate.
Label: REFUTES
Result: NOT ENOUGH INFO
Result details: {'result': 'incorrect', 'explanation': "The Senate voted 'not guilty' on both charges, which means Bill Clinton was acquitted.", 'article': {'url': 'http://en.wikipedia.org/wiki/Impeachment_of_Bill_Clinton?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Impeachment_of_Bill_Clinton?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Impeachment_of_Bill_Clinton?wprov=cna1#Acquittal', 'title': 'Impeachment of Bill Clinton', 'section': 'Acquittal', 'snippet': 'The perjury charge was defeated with 45 votes for conviction and 55 against, and the obstruction of justice charge was defeated with 50 for conviction and 50 against.', 'references': 145, 'contributors': 1424, 'last_updated': '2024-03-09 23:34'}}

Claim: A Nightmare on Elm Street stars Katie Cassidy and Rooney Mara.
Label: NOT ENOUGH INFO
Result: SUPPORTS
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Denial premiered at the Toronto International Film Festival in 1997.
Label: REFUTES
Result: SUPPORTS
Result details: None

Claim: Cyndi Lauper released a debut solo album.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'correct', 'explanation': "The debut solo album 'She's So Unusual' by Cyndi Lauper was released on October 14, 1983.", 'article': {'url': 'http://en.wikipedia.org/wiki/Cyndi_Lauper_discography?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Cyndi_Lauper_discography?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Cyndi_Lauper_discography?wprov=cna1#Lead_Section', 'title': 'Cyndi Lauper discography', 'section': 'Lead Section', 'snippet': "She's So Unusual|\nReleased: October 14, 1983", 'references': 138, 'contributors': 466, 'last_updated': '2024-02-27 15:23'}}

Claim: The Coen brothers have only directed Barton Fink.
Label: REFUTES
Result: NOT ENOUGH INFO
Result details: None

Claim: Frank Zappa's debut album was released in 1966.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'correct', 'explanation': "The release date of Frank Zappa's debut album, 'Freak Out!', was in 1966.", 'article': {'url': 'http://en.wikipedia.org/wiki/Freak_Out!?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Freak_Out!?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Freak_Out!?wprov=cna1#Release', 'title': 'Freak Out!', 'section': 'Release', 'snippet': "Verve released Freak Out! on June 27, 1966. The band's name was changed to the Mothers of Invention,", 'references': 42, 'contributors': 425, 'last_updated': '2024-03-17 17:34'}}

Claim: Hungary was only founded by Barack Obama.
Label: NOT ENOUGH INFO
Result: SUPPORTS
Result details: {'result': 'incorrect', 'explanation': 'Barack Obama did not found Hungary, he visited the country during his presidency.', 'article': {'url': 'http://en.wikipedia.org/wiki/List_of_international_presidential_trips_made_by_Barack_Obama?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/List_of_international_presidential_trips_made_by_Barack_Obama?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/List_of_international_presidential_trips_made_by_Barack_Obama?wprov=cna1#2010', 'title': 'List of international presidential trips made by Barack Obama', 'section': '2010', 'snippet': 'Signed the New Strategic Arms Reduction Treaty with Russian President Dmitry Medvedev. Also met with the presidents of the Czech Republic, Estonia, Latvia, and Romania; and with the prime ministers of Bulgaria, Croatia, Hungary, Lithuania, Poland, Slovakia, and Slovenia at a formal dinner.', 'references': 322, 'contributors': 438, 'last_updated': '2023-12-19 02:26'}}

Claim: Blackhat stars an actor.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: The Houston Rockets have won four Western Conference titles.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The Houston Rockets have won two Western Conference titles, not four.', 'article': {'url': 'http://en.wikipedia.org/wiki/List_of_Houston_Rockets_seasons?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/List_of_Houston_Rockets_seasons?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/List_of_Houston_Rockets_seasons?wprov=cna1#All-time_records', 'title': 'List of Houston Rockets seasons', 'section': 'All-time records', 'snippet': 'All-time post-season record (1969–present)158164', 'references': 47, 'contributors': 124, 'last_updated': '2023-06-19 13:57'}}

Claim: Syria includes Jewish people.
Label: NOT ENOUGH INFO
Result: SUPPORTS
Result details: {'result': 'incorrect', 'explanation': 'The text discusses the historical origins of the Jewish people, tracing back to the Israelites who were known as Hebrews and Israelites, not Syrians.', 'article': {'url': 'http://en.wikipedia.org/wiki/Jews?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Jews?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Jews?wprov=cna1#Origins', 'title': 'Jews', 'section': 'Origins', 'snippet': 'The Jewish people as a whole, initially called Hebrews (ʿIvrim), were known as Israelites (Yisreʾelim) from the time of their entrance into the Holy Land to the end of the Babylonian Exile (538\xa0BC).', 'references': 372, 'contributors': 3446, 'last_updated': '2024-04-01 08:27'}}

Claim: Serena Williams competes in women's singles and doubles.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'correct', 'explanation': "The text provides a list of Serena Williams' Grand Slam tournament finals in women's singles, confirming that she competes in women's singles.", 'article': {'url': 'http://en.wikipedia.org/wiki/Serena_Williams?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Serena_Williams?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Serena_Williams?wprov=cna1#Grand_Slam_tournament_finals', 'title': 'Serena Williams', 'section': 'Grand Slam tournament finals', 'snippet': 'Singles: 33 (23–10)', 'references': 777, 'contributors': 6561, 'last_updated': '2024-03-31 20:21'}}

Claim: Leonardo da Vinci is a painter.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'correct', 'explanation': 'Leonardo da Vinci is primarily known for his achievements as a painter.', 'article': {'url': 'http://en.wikipedia.org/wiki/Leonardo_da_Vinci?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Leonardo_da_Vinci?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Leonardo_da_Vinci?wprov=cna1#Paintings', 'title': 'Leonardo da Vinci', 'section': 'Paintings', 'snippet': 'Despite the recent awareness and admiration of Leonardo as a scientist and inventor, for the better part of four hundred years his fame rested on his achievements as a painter.', 'references': 257, 'contributors': 4003, 'last_updated': '2024-03-23 10:43'}}

Claim: Laura Linney was in a movie.
Label: REFUTES
Result: SUPPORTS
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Dylan O'Brien is not a film actor.
Label: REFUTES
Result: SUPPORTS
Result details: {'result': 'incorrect', 'explanation': "Dylan O'Brien has appeared in various films, including Deepwater Horizon, American Assassin, and Bumblebee.", 'article': {'url': "http://en.wikipedia.org/wiki/Dylan_O'Brien?wprov=cna1", 'page_url': "http://en.wikipedia.org/wiki/Dylan_O'Brien?wprov=cna1", 'section_url': "http://en.wikipedia.org/wiki/Dylan_O'Brien?wprov=cna1#Career", 'title': "Dylan O'Brien", 'section': 'Career', 'snippet': 'In 2016, he appeared in the disaster thriller-drama Deepwater Horizon, based on the 2010 Deepwater Horizon explosion. He starred in 2017\'s American Assassin, an action-thriller where he played titular character Mitch Rapp;Wein, Dick (July 29, 2016). "CBS Continues to Chart the Future". Update, Vol. 18, No. 878, CBS Corporation. p. 11. and voiced the CGI title character in the Transformers spin-off film Bumblebee in 2018.', 'references': 91, 'contributors': 1214, 'last_updated': '2024-03-07 20:14'}}

Claim: Asia contains places.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'correct', 'explanation': 'The statement is correct as Asia is indeed the largest continent on Earth with various places within it.', 'article': {'url': 'http://en.wikipedia.org/wiki/Asia?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Asia?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Asia?wprov=cna1#Geography', 'title': 'Asia', 'section': 'Geography', 'snippet': "Asia is the largest continent on Earth. It covers 9% of the Earth's total surface area (or 30% of its land area), and has the longest coastline", 'references': 267, 'contributors': 3870, 'last_updated': '2024-03-28 15:51'}}

Claim: Her was given a wide release on January 10, 2014.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'correct', 'explanation': 'The statement matches the information provided in the source.', 'article': {'url': 'http://en.wikipedia.org/wiki/Her_(film)?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Her_(film)?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Her_(film)?wprov=cna1#Release', 'title': 'Her (film)', 'section': 'Release', 'snippet': 'was set to have a limited release in North America on November 20, 2013, through Warner Bros. Pictures. It was later pushed back to a limited December 18, 2013 release, with a January 10, 2014 wide release in order to accommodate an awards campaign.', 'references': 153, 'contributors': 795, 'last_updated': '2024-03-30 18:18'}}

Claim: Poppy is a Tennessean.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The ownership of Bridgestone Arena does not indicate that Poppy is a Tennessean.', 'article': {'url': 'http://en.wikipedia.org/wiki/Bridgestone_Arena?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Bridgestone_Arena?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Bridgestone_Arena?wprov=cna1#Lead_Section', 'title': 'Bridgestone Arena', 'section': 'Lead Section', 'snippet': 'Bridgestone Arena is owned by the Sports Authority of Nashville and Davidson County and operated by Powers Management Company, a subsidiary of the Nashville Predators National Hockey League franchise, which has been its primary tenant since 1998.', 'references': 41, 'contributors': 481, 'last_updated': '2024-03-23 19:32'}}

Claim: Moana is a 2016 novel.
Label: REFUTES
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Just My Luck (2006 film) features Chris Pine in a lead role.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'Chris Pine is not listed in the cast of Just My Luck (2006 film).', 'article': {'url': 'http://en.wikipedia.org/wiki/Just_My_Luck_(2006_film)?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Just_My_Luck_(2006_film)?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Just_My_Luck_(2006_film)?wprov=cna1#Cast', 'title': 'Just My Luck (2006 film)', 'section': 'Cast', 'snippet': 'Cast\n Lindsay Lohan as Ashley Albright \n Chris Pine as Jake Hardin\n McFly as themselves\n Faizon Love as Damon Phillips\n Samaire Armstrong as Maggie Smith\n Bree Turner as Dana Adams\n Missi Pyle as Peggy Braden\n Makenzie Vega as Katy Hardin\n Tovah Feldshuh as Madame Z\n Jaqueline Fleming as Tiffany Richards\n Chris Carmack as David Pennington\n Carlos Ponce as Antonio Cordova\n Dane Rhodes as Mac\n J.C. Sealy as Aunt Marta Hardin', 'references': 17, 'contributors': 491, 'last_updated': '2024-03-20 01:14'}}

Claim: Miyu Irino is a Voice contestant.
Label: NOT ENOUGH INFO
Result: REFUTES
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: 100 Greatest of All Time first premiered on the Tennis Channel.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'incorrect', 'explanation': "The text does not mention that the '100 Greatest of All Time' first premiered on the Tennis Channel.", 'article': {'url': 'http://en.wikipedia.org/wiki/100_Greatest_of_All_Time?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/100_Greatest_of_All_Time?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/100_Greatest_of_All_Time?wprov=cna1#Top_100_ranking_per_Tennis_Channel_in_2012', 'title': '100 Greatest of All Time', 'section': 'Top 100 ranking per Tennis Channel in 2012', 'snippet': 'Top 100 ranking per Tennis Channel in 2012', 'references': 2, 'contributors': 429, 'last_updated': '2024-02-04 16:17'}}

Claim: Stanley Kubrick is not a director.
Label: REFUTES
Result: SUPPORTS
Result details: {'result': 'incorrect', 'explanation': 'There are matching sources to the claim', 'article': {'url': 'http://en.wikipedia.org/wiki/Stanley_Kubrick_filmography?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Stanley_Kubrick_filmography?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Stanley_Kubrick_filmography?wprov=cna1#Lead_Section', 'title': 'Stanley Kubrick filmography', 'section': 'Lead Section', 'snippet': 'Feature films\nthumb|upright|alt=Poster for Paths of Glory featuring Kirk Douglas as a soldier|Poster for Paths of Glory (1957)\nthumb|upright|alt=Film poster featuring young girl wearing sunglasses and sucking on a lollipop|Poster for Lolita (1962)\nthumb|upright|alt=Poster displaying youth aiming arrow and text: "Stanley Kubrick\'s A Clockwork Orange"|Poster for A Clockwork Orange (1971)\n+  Year  Title Director  Writer  Producer Notes  1952 Fear and Desire    Also editor and cinematographer 1955 Killer\'s Kiss    Also editor and cinematographer 1956     Co-written with Jim Thompson 1957 Paths of Glory    Co-written with Calder Willingham and Jim Thompson  1960 Spartacus     1962 Lolita     1964 Dr. Strangelove    Co-written with Terry Southern and Peter George 1968     Co-written with Arthur C. ClarkeAlso director and designer of special photographic effects   1971      1975 Barry Lyndon    1980     Co-written with Diane Johnson 1987 Full Metal Jacket    Co-written with Michael Herr and Gustav Hasford 1999 Eyes Wide Shut    Co-written with Frederic RaphaelReleased posthumously', 'references': 136, 'contributors': 155, 'last_updated': '2024-02-24 14:46'}}

Claim: Faith Evans is a person.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: None

Claim: There was a musician in the Grand Ole Pry.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching sections found.'}

Claim: Islam does not have any followers.
Label: REFUTES
Result: SUPPORTS
Result details: {'result': 'incorrect', 'explanation': "The term 'Muslim' refers to a follower of Islam, therefore Islam does have followers.", 'article': {'url': 'http://en.wikipedia.org/wiki/Islam?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Islam?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Islam?wprov=cna1#Lead_Section', 'title': 'Islam', 'section': 'Lead Section', 'snippet': 'A Muslim (), the word for a follower of Islam,"Muslim." Lexico. UK: Oxford University Press. 2020. is the active participle of the same verb form, and means "submitter (to God)" or "one who surrenders (to God)".', 'references': 564, 'contributors': 5852, 'last_updated': '2024-04-02 11:41'}}

Claim: The United Kingdom is the 21st-most populous city.
Label: REFUTES
Result: SUPPORTS
Result details: None

Claim: Michael Jordan never played basketball.
Label: REFUTES
Result: SUPPORTS
Result details: {'result': 'incorrect', 'explanation': 'The quote clearly states that Michael Jordan played basketball in the NBA.', 'article': {'url': 'http://en.wikipedia.org/wiki/Michael_Jordan?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Michael_Jordan?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Michael_Jordan?wprov=cna1#Professional_career', 'title': 'Michael Jordan', 'section': 'Professional career', 'snippet': 'Jordan played in his final NBA game on April 16, 2003, in Philadelphia.', 'references': 669, 'contributors': 4574, 'last_updated': '2024-04-01 16:16'}}

Claim: The capital city of Austria is Vienna.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'correct', 'explanation': 'The quote confirms that Vienna became the de facto capital of the Holy Roman Empire.', 'article': {'url': 'http://en.wikipedia.org/wiki/Vienna?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Vienna?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Vienna?wprov=cna1#History', 'title': 'Vienna', 'section': 'History', 'snippet': 'Vienna became the resident city of the Habsburg dynasty. It eventually grew to become the de facto capital of the Holy Roman Empire (800–1806) in 1437', 'references': 328, 'contributors': 3098, 'last_updated': '2024-03-30 23:39'}}

Claim: The Miami Marlins are a team that plays baseball.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'correct', 'explanation': 'The quote confirms that the Miami Marlins are a baseball team, managed by Rene Lachemann.', 'article': {'url': 'http://en.wikipedia.org/wiki/Miami_Marlins?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Miami_Marlins?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Miami_Marlins?wprov=cna1#Lead_Section', 'title': 'Miami Marlins', 'section': 'Lead Section', 'snippet': "The Marlins' first manager was Rene Lachemann, a former catcher who had previously managed the Seattle Mariners and Milwaukee Brewers, and who at the time of his hiring was a third base coach for the Oakland Athletics.", 'references': 72, 'contributors': 1993, 'last_updated': '2024-03-12 23:43'}}

Claim: Birth of the Dragon comes out in summer 2017.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Judi Dench is an actress.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'correct', 'explanation': 'The quote confirms that Judi Dench is an actress, as it describes her first professional stage appearance.', 'article': {'url': 'http://en.wikipedia.org/wiki/Judi_Dench?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Judi_Dench?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Judi_Dench?wprov=cna1#Career', 'title': 'Judi Dench', 'section': 'Career', 'snippet': 'Dench made her first professional stage appearance in September 1957 with the Old Vic Company at the Royal Court Theatre in Liverpool, as Ophelia in Hamlet.', 'references': 270, 'contributors': 1970, 'last_updated': '2024-03-30 06:07'}}

Claim: Lil Wayne has kept his albums entirely to himself.
Label: REFUTES
Result: SUPPORTS
Result details: None

Claim: Sleeping Beauty premiered in May 2011.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'incorrect', 'explanation': 'The filming started in April 2010, not the premiere.', 'article': {'url': 'http://en.wikipedia.org/wiki/Sleeping_Beauty_(2011_film)?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Sleeping_Beauty_(2011_film)?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Sleeping_Beauty_(2011_film)?wprov=cna1#Production', 'title': 'Sleeping Beauty (2011 film)', 'section': 'Production', 'snippet': 'Principal photography on the film began on 3 April 2010, at University of Sydney, Camperdown and downtown Sydney, New South Wales, Australia.', 'references': 36, 'contributors': 266, 'last_updated': '2024-03-11 18:10'}}

Claim: Philadelphia contains 67 National Historic Landmarks.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'no_result', 'explanation': 'No matching sections found.'}

Claim: Amelia Earhart was Canadian.
Label: NOT ENOUGH INFO
Result: SUPPORTS
Result details: {'result': 'incorrect', 'explanation': 'Amelia Earhart was born in Atchison, Kansas, USA, not in Canada.', 'article': {'url': 'http://en.wikipedia.org/wiki/Amelia_Earhart?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Amelia_Earhart?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Amelia_Earhart?wprov=cna1#Lead_Section', 'title': 'Amelia Earhart', 'section': 'Lead Section', 'snippet': 'One of the highlights of the day was a flying exhibition put on by a World War I ace. The pilot overhead spotted Earhart and her friend, who were watching from an isolated clearing, and dived at them. "I am sure he said to himself, \'Watch me make them scamper,\'" she said. Earhart stood her ground as the aircraft came close. "I did not understand it at the time," she said, "but I believe that little red airplane said something to me as it swished by."', 'references': 253, 'contributors': 2435, 'last_updated': '2024-04-02 02:34'}}

Claim: The Dodecanese are in the southeastern Tyrrhenian Sea.
Label: REFUTES
Result: SUPPORTS
Result details: None

Claim: Death in Paradise has enjoyed high ratings and is highly acclaimed.
Label: NOT ENOUGH INFO
Result: REFUTES
Result details: {'result': 'partially_correct', 'explanation': 'The claim is partially correct as it refers to high critical acclaim, but the source is about Hannibal season 3, not Death in Paradise.', 'article': {'url': 'http://en.wikipedia.org/wiki/Hannibal_season_3?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Hannibal_season_3?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Hannibal_season_3?wprov=cna1#Critical_reviews', 'title': 'Hannibal season 3', 'section': 'Critical reviews', 'snippet': 'The season received critical acclaim. On Rotten Tomatoes, season 3 has an approval rating of 98% with an average rating of 8.9/10 based on 47 reviews.', 'references': 112, 'contributors': 19, 'last_updated': '2024-03-31 18:41'}}

Claim: London is in Greater London.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'partially_correct', 'explanation': 'The quote mentions that London is divided into Inner London and Outer London, which is not the same as Greater London. However, the statement is partially correct as London is indeed located within Greater London.', 'article': {'url': 'http://en.wikipedia.org/wiki/Greater_London?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Greater_London?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Greater_London?wprov=cna1#Status', 'title': 'Greater London', 'section': 'Status', 'snippet': 'London is officially divided for some purposes, with varying definitions, into Inner London and Outer London.', 'references': 82, 'contributors': 950, 'last_updated': '2024-03-19 03:42'}}

Claim: Equidae includes some species known only from their bones.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'partially_correct', 'explanation': 'The statement is partially correct as it mentions the importance of bones in identifying and understanding the evolution of horses.', 'article': {'url': 'http://en.wikipedia.org/wiki/Evolution_of_the_horse?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Evolution_of_the_horse?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Evolution_of_the_horse?wprov=cna1#Details', 'title': 'Evolution of the horse', 'section': 'Details', 'snippet': 'The ancestors of the horse came to walk only on the end of the third toe and both side (second and fourth) "toes". Skeletal remnants show obvious wear on the back of both sides of metacarpal and metatarsal bones, commonly called the "splint bones".', 'references': 123, 'contributors': 695, 'last_updated': '2024-03-20 14:42'}}

Claim: The Eagles were active in the 1970s.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: The 2013 NBA draft was planned at Barclays Center in Brooklyn.
Label: NOT ENOUGH INFO
Result: SUPPORTS
Result details: {'result': 'incorrect', 'explanation': 'The quote does not provide information about the location of the 2013 NBA draft.', 'article': {'url': 'http://en.wikipedia.org/wiki/2013_NBA_draft?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/2013_NBA_draft?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/2013_NBA_draft?wprov=cna1#Eligibility_and_entrants', 'title': '2013 NBA draft', 'section': 'Eligibility and entrants', 'snippet': "The draft is conducted under the eligibility rules established in the league's new 2011 collective bargaining agreement (CBA) with its players union.", 'references': 87, 'contributors': 490, 'last_updated': '2024-04-01 08:25'}}

Claim: Alex Rodriguez was suspended for 500 games.
Label: REFUTES
Result: SUPPORTS
Result details: {'result': 'incorrect', 'explanation': 'The suspension was not for 500 games, but for the entirety of the 2014 regular season and postseason.', 'article': {'url': 'http://en.wikipedia.org/wiki/Alex_Rodriguez?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Alex_Rodriguez?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Alex_Rodriguez?wprov=cna1#2014:_Suspension_due_to_Biogenesis_scandal', 'title': 'Alex Rodriguez', 'section': '2014: Suspension due to Biogenesis scandal', 'snippet': "Rodriguez's suspension that was announced the previous season but delayed pending an appeal, was upheld, meaning that he would be suspended for the entirety of the 2014 regular season and postseason.", 'references': 404, 'contributors': 2962, 'last_updated': '2024-03-28 16:16'}}

Claim: Eliza Dushka is incapable of having any roles in films.
Label: REFUTES
Result: SUPPORTS
Result details: {'result': 'incorrect', 'explanation': 'There are clear examples of Eliza Dushku having roles in films.', 'article': {'url': 'http://en.wikipedia.org/wiki/Eliza_Dushku?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Eliza_Dushku?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Eliza_Dushku?wprov=cna1#Career', 'title': 'Eliza Dushku', 'section': 'Career', 'snippet': 'Dushku starred in the hit cheerleader comedy Bring It On. She followed that up with Soul Survivors, which reunited her with her Race the Sun co-star Casey Affleck. In 2001, she appeared in The New Guy with DJ Qualls and in City by the Sea with Robert De Niro and James Franco.', 'references': 174, 'contributors': 1649, 'last_updated': '2024-03-27 09:47'}}

Claim: The Weeknd only released an album in 2011.
Label: REFUTES
Result: NOT ENOUGH INFO
Result details: {'result': 'incorrect', 'explanation': 'The Weeknd released a series of mixtapes in 2011, not an album.', 'article': {'url': 'http://en.wikipedia.org/wiki/Trilogy_(The_Weeknd_album)?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Trilogy_(The_Weeknd_album)?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Trilogy_(The_Weeknd_album)?wprov=cna1#Lead_Section', 'title': 'Trilogy (The Weeknd album)', 'section': 'Lead Section', 'snippet': 'In 2011, the Weeknd released a series of mixtapes—House of Balloons, Thursday and Echoes of Silence—and garnered both critical acclaim and a growing fan base.', 'references': 108, 'contributors': 283, 'last_updated': '2024-03-17 02:14'}}

Claim: Michael J. Fox had a virus.
Label: NOT ENOUGH INFO
Result: SUPPORTS
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Heinrich Himmler oversaw all internal and external police of Nazi Germany.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'correct', 'explanation': "The quote confirms that Heinrich Himmler had operational control over Germany's entire detective force and authority over all of Germany's uniformed law enforcement agencies, making him responsible for overseeing all internal and external police of Nazi Germany.", 'article': {'url': 'http://en.wikipedia.org/wiki/Heinrich_Himmler?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Heinrich_Himmler?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Heinrich_Himmler?wprov=cna1#Rise_in_the_SS', 'title': 'Heinrich Himmler', 'section': 'Rise in the SS', 'snippet': 'In practice, however, the police were now effectively a division of the SS, and hence independent of Frick\'s control. This move gave Himmler operational control over Germany\'s entire detective force. He also gained authority over all of Germany\'s uniformed law enforcement agencies, which were amalgamated into the new Ordnungspolizei (Orpo: "order police"), which became a branch of the SS under Daluege.', 'references': 77, 'contributors': 2531, 'last_updated': '2024-03-19 19:50'}}

Claim: Hotel Hell is a TV series.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'correct', 'explanation': 'The quote confirms that Hotel Hell is a TV series that was scheduled to premiere on Fox.', 'article': {'url': 'http://en.wikipedia.org/wiki/Hotel_Hell?wprov=cna1', 'page_url': 'http://en.wikipedia.org/wiki/Hotel_Hell?wprov=cna1', 'section_url': 'http://en.wikipedia.org/wiki/Hotel_Hell?wprov=cna1#Lead_Section', 'title': 'Hotel Hell', 'section': 'Lead Section', 'snippet': "Originally scheduled to premiere on Fox on April 6, 2012, the series was first rescheduled to June 4, 2012 in order to accommodate the move of The Finder, then rescheduled to August 13, due to Ramsay's other two series, Hell's Kitchen and MasterChef, being scheduled for Monday nights during the summer.", 'references': 74, 'contributors': 230, 'last_updated': '2024-03-26 23:06'}}
