QA Run
======

Average Consistency Score: 0.8272727272727274

Selection: As the father of endoscopy, Chevalier Q. Jackson removed thousands of swallowed objects from his patients.
Consistency Score: 1.0
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: In the 19th century, an idea to move trains via air pumps was devised by Isambard Kingdom Brunel.
Consistency Score: 0.9
Prevelent Result: incorrect
Results: incorrect, incorrect, incorrect, incorrect, incorrect, correct, incorrect, incorrect, incorrect, incorrect
Errors: []

------

Selection: The Times once printed a politician's speech without realizing that an obscene remark -- "the speaker then said he felt inclined for a bit of fucking" -- was inserted into the middle of it.
Consistency Score: 0.5
Prevelent Result: no_result
Results: incorrect, incorrect, correct, no_result, no_result, no_result, incorrect, no_result, incorrect, no_result
Errors: []

------

Selection: In the 70s, you could buy a kit to assemble to a tiny toy version of a cannon that fired at noon, when the sun's rays were focused through a lens.
Consistency Score: 0.8
Prevelent Result: incorrect
Results: partially_correct, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, partially_correct
Errors: []

------

Selection: Children never change: as far back as Ancient Mesopotamia, a boy wrote letters to his mother trying to shame her into giving him better clothes to impress his peers.
Consistency Score: 0.9
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, no_result, correct, correct
Errors: []

------

Selection: Louis Armstrong's "What a Wonderful World" used to be banned from radio air time following Sept 11.
Consistency Score: 1.0
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: King Charles VIII's personal advisor once lived as a hermit in a cave on his father's estate.
Consistency Score: 1.0
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: Lauren Boebert reimbursed herself in 2020 for roughly 39,000 miles traveling in her car.
Consistency Score: 0.7
Prevelent Result: incorrect
Results: no_result, incorrect, incorrect, incorrect, no_result, incorrect, no_result, incorrect, incorrect, incorrect
Errors: []

------

Selection: In Lebanon, a common everyday hello is often in English, Arabic and French all in the same expression.
Consistency Score: 0.9
Prevelent Result: incorrect
Results: incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, no_result, incorrect, incorrect
Errors: []

------

Selection: Candy Raisins are made from jujube and are named after their appearance, rather than their flavor.
Consistency Score: 1.0
Prevelent Result: incorrect
Results: incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect
Errors: []

------

Selection: The inventor of the Segway, Dean Kamen, once bought an island called North Dumpling Island, before seceding from the United States and referring to himself as "Lord Dumpling II".
Consistency Score: 1.0
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: A bug in the original 1991 Civilization game would cause Mahatma Gandhi to become increasingly aggressive to the point of using nuclear weapons.
Consistency Score: 0.5
Prevelent Result: incorrect
Results: no_result, incorrect, no_result, no_result, incorrect, incorrect, no_result, no_result, incorrect, incorrect
Errors: []

------

Selection: Edgar Allan Poe died due to a ballot-stuffing scam known as cooping, where he was kidnapped and locked in a room and beaten in order to provide votes for a political party. 
Consistency Score: 0.7
Prevelent Result: no_result
Results: incorrect, no_result, incorrect, no_result, no_result, no_result, incorrect, no_result, no_result, no_result
Errors: []

------

Selection: The Bayside Canadian Railway is the world's shortest railway, measuring only 61m in total length.
Consistency Score: 0.9
Prevelent Result: no_result
Results: no_result, no_result, no_result, no_result, no_result, no_result, no_result, incorrect, no_result, no_result
Errors: []

------

Selection: A South African politician loved karate so much as a kid he got the nickname "Tokyo".
Consistency Score: 0.6
Prevelent Result: correct
Results: incorrect, correct, correct, correct, incorrect, incorrect, correct, correct, correct, no_result
Errors: []

------

Selection: If you're not careful when swimming at El Paredon beach, you could get caught in a rip current, which will drag you underwater and drown you.
Consistency Score: 1.0
Prevelent Result: incorrect
Results: incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect
Errors: []

------

Selection: The United States and Israel helped install Guatemalan leader, General Benedicto Lucas, who was responsible for the genocide of thousands of Mayans in the 1980s.
Consistency Score: 0.5
Prevelent Result: partially_correct
Results: partially_correct, correct, correct, partially_correct, no_result, partially_correct, correct, partially_correct, no_result, partially_correct
Errors: []

------

Selection: The Klobb was by far the most useless gun in Goldeneye, but I still kind of loved it for embracing chaos.
Consistency Score: 0.6
Prevelent Result: partially_correct
Results: correct, partially_correct, partially_correct, correct, correct, partially_correct, partially_correct, partially_correct, correct, partially_correct
Errors: []

------

Selection: The term dreamcatcher is a bit misleading, as they were originally used by the Anishinaabeg to ward off harm and bad spirits, rather than having anything to do with dreams.
Consistency Score: 1.0
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: Annette Eckis was infamously involved in a SeaWorld accident where she was bitten by the legs and dragged by Shamu after a stunt gone wrong, not having been informed that Shamu was triggered by bikinis.
Consistency Score: 0.6
Prevelent Result: no_articles
Results: no_articles, no_articles, partially_correct, no_articles, no_articles, partially_correct, no_articles, no_articles, no_result, partially_correct
Errors: []

------

Selection: Claire Van Ummersen had an academic career primarily focused on the sciences, but also found time to learn French and Latin.
Consistency Score: 1.0
Prevelent Result: incorrect
Results: incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect
Errors: []

------

Selection: Bruce Springsteen did not pay his taxes until he was put on the cover of Time Magazine in 1975. He then spent the next few years paying off his taxes, leaving him with only $20,000 on his 30th birthday despite multiple best-selling records and tours.
Consistency Score: 0.7
Prevelent Result: incorrect
Results: incorrect, incorrect, incorrect, no_result, no_result, incorrect, no_result, incorrect, incorrect, incorrect
Errors: []

------

Selection: a man under the pseudonym "Kirk Allen" who became deluded that a sci-fi book series was actually the story of his life. He filled in the blanks with elaborate details and hallucinated himself in those settings. He was treated by Robert Lindner, who himself became obsessed with the books.
Consistency Score: 0.6
Prevelent Result: error
Results: no_articles, no_articles, error, error, no_articles, no_articles, error, error, error, error
Errors: []

------

Selection: Tōxcatl, an annual Aztec festival which revolved around the sacrifice of a young man who had been impersonating their god Tezcatlipoca since the last Tōxcatl festival, and the selection of a new man to take that role in the year to come
Consistency Score: 1.0
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: the 'Eddie Murphy Rule' in the Dodd-Frank Wall Street Reform Act which disallows secretly giving out doctored market information so as to corner a market using false information.
Consistency Score: 1.0
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: Ruth Handler, creator of the Barbie doll, got her idea when she saw her daughter Barbara playing with paper dolls and giving them adult roles, but most of the dolls in the market were of infants.
Consistency Score: 1.0
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: "Dune" was rejected by at least 20 publishers before being published by Chilton, the auto-manual company.
Consistency Score: 1.0
Prevelent Result: incorrect
Results: incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect
Errors: []

------

Selection: Sonny Bono is the only member of Congress in American History to have scored a Number One Hit on the Billboard Hot 100, doing so with he and his ex-wife Chers 1965 Hit "I Got You Babe".
Consistency Score: 1.0
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: the mother of Lee Harvey Oswald would sometimes go to Dealey Plaza and sell her autograph for five dollars
Consistency Score: 0.6
Prevelent Result: incorrect
Results: incorrect, no_result, no_articles, incorrect, incorrect, no_articles, incorrect, no_articles, incorrect, incorrect
Errors: []

------

Selection: in 1556, during the execution of the Guernsey Martyrs by burning on the stake, Perotine Massey, one of the three women executed, gave birth to a son. The Bailiff Hellier Gosselin ordered this infant to be cast in the fire
Consistency Score: 0.6
Prevelent Result: incorrect
Results: correct, incorrect, incorrect, incorrect, no_result, no_result, no_result, incorrect, incorrect, incorrect
Errors: []

------

Selection: Dr Amy Bishop shot and killed three professors in a University of Alabama biology department meeting in 2010. It was found out later that she had shot and killed her brother in 1986 and was never charged.
Consistency Score: 1.0
Prevelent Result: correct
Results: correct, correct, correct, correct, correct, correct, correct, correct, correct, correct
Errors: []

------

Selection: the song "Hello! Ma Baby", popularized in Merrie Melodies cartoons and Spaceballs, dates to 1899 and was written about a girlfriend the singer has met only through the telephone.
Consistency Score: 1.0
Prevelent Result: incorrect
Results: incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect, incorrect
Errors: []

------

Selection: there are 17.3 million American digital nomads or people that travel freely while working remotely using technology and the internet.
Consistency Score: 0.7
Prevelent Result: incorrect
Results: no_result, incorrect, incorrect, no_result, incorrect, incorrect, incorrect, no_result, incorrect, incorrect
Errors: []

------

