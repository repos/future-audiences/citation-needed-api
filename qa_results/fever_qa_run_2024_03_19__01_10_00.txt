Accuracy: 0.6565656565656566

Incorrect Results:

Claim: Samuel L. Jackson was not in the M. Night Shyamalan film Unbreakable.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'Samuel L. Jackson did appear in the M. Night Shyamalan film Unbreakable as Elijah Price/Mr. Glass.', 'article': {'url': 'http://en.wikipedia.org/?curid=32252', 'page_url': 'http://en.wikipedia.org/?curid=32252', 'section_url': 'http://en.wikipedia.org/?curid=32252#Cast', 'title': 'Unbreakable (film)', 'section': 'Cast', 'snippet': 'Samuel L. Jackson as Elijah Price/Mr. Glass, a comic book theorist, and deranged domestic terrorist with brittle bone disease', 'references': 65, 'contributors': 1358, 'last_updated': '2024-03-16 07:17'}}

Claim: Planet of the Apes' filming took place between May 21 and August 10, 1967.
Label: NOT ENOUGH INFO
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The filming for the Planet of the Apes did not take place between May 21 and August 10, 1967, as the test scene was shot on March 8, 1966.', 'article': {'url': 'http://en.wikipedia.org/?curid=18618306', 'page_url': 'http://en.wikipedia.org/?curid=18618306', 'section_url': 'http://en.wikipedia.org/?curid=18618306#Origins', 'title': 'Planet of the Apes (1968 film)', 'section': 'Origins', 'snippet': 'To convince the Fox Studio that a Planet of the Apes film could be made, the producers shot a brief test scene from a Rod Serling draft of the script, using early versions of the ape makeup, on March 8, 1966.', 'references': 99, 'contributors': 1452, 'last_updated': '2024-03-10 18:43'}}

Claim: Red Hot Chili Peppers were originally named Tony Flow and the Miraculously Majestic Masters of Mayhem.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The selected quote does not support the claim about the original name of the Red Hot Chili Peppers.', 'article': {'url': 'http://en.wikipedia.org/?curid=26589', 'page_url': 'http://en.wikipedia.org/?curid=26589', 'section_url': 'http://en.wikipedia.org/?curid=26589#Lead_Section', 'title': 'Red Hot Chili Peppers', 'section': 'Lead Section', 'snippet': 'During the tour, continuing musical and lifestyle tension between Kiedis and Sherman complicated the transition between concert and daily band life. Sherman was fired in February 1985.', 'references': 356, 'contributors': 9142, 'last_updated': '2024-03-13 03:12'}}

Claim: Italy experienced economic growth between 1494 to 1559.
Label: NOT ENOUGH INFO
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The economic growth mentioned in the text is related to the period after World War I and the rise of the Fascist Party in the 1920s, not between 1494 to 1559.', 'article': {'url': 'http://en.wikipedia.org/?curid=26294015', 'page_url': 'http://en.wikipedia.org/?curid=26294015', 'section_url': 'http://en.wikipedia.org/?curid=26294015#Fascist_Italy', 'title': 'Economic history of Italy', 'section': 'Fascist Italy', 'snippet': 'Italy had emerged from World War I in a poor and weakened condition. The National Fascist Party of Benito Mussolini came to power in Italy in 1922, at the end of a period of social unrest. During the first four years of the new regime, from 1922 to 1925, the Fascist had a generally laissez-faire economic policy: they initially reduced taxes, regulations and trade restrictions on the whole.', 'references': 96, 'contributors': 134, 'last_updated': '2024-02-23 03:52'}}

Claim: A Nightmare on Elm Street stars Katie Cassidy and Rooney Mara.
Label: NOT ENOUGH INFO
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The claim is incorrect as the text mentions actors like Johnny Depp and Heather Langenkamp, not Katie Cassidy and Rooney Mara.', 'article': {'url': 'http://en.wikipedia.org/?curid=3028191', 'page_url': 'http://en.wikipedia.org/?curid=3028191', 'section_url': 'http://en.wikipedia.org/?curid=3028191#Cast', 'title': 'A Nightmare on Elm Street', 'section': 'Cast', 'snippet': 'The cast of A Nightmare on Elm Street included a crew of veteran actors such as Robert Englund and John Saxon and several aspiring young actors like Johnny Depp and Heather Langenkamp.', 'references': 149, 'contributors': 1840, 'last_updated': '2024-02-17 04:56'}}

Claim: Blackhat stars an actor.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'partially_correct', 'explanation': 'The statement is partially correct as it mentions the casting of an actor, but does not specify the name of the actor, Chris Hemsworth, who starred in the film.', 'article': {'url': 'http://en.wikipedia.org/?curid=39384134', 'page_url': 'http://en.wikipedia.org/?curid=39384134', 'section_url': 'http://en.wikipedia.org/?curid=39384134#Critical_response', 'title': 'Blackhat (film)', 'section': 'Critical response', 'snippet': 'For many critics, a significant issue of the film was the casting of Chris Hemsworth as a hacker.', 'references': 78, 'contributors': 432, 'last_updated': '2024-03-15 20:47'}}

Claim: The Houston Rockets have won four Western Conference titles.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The Houston Rockets have advanced to the NBA Finals four times, not won four Western Conference titles.', 'article': {'url': 'http://en.wikipedia.org/?curid=72885', 'page_url': 'http://en.wikipedia.org/?curid=72885', 'section_url': 'http://en.wikipedia.org/?curid=72885#Lead_Section', 'title': 'Houston Rockets', 'section': 'Lead Section', 'snippet': 'o the 1980–81 season, the arrival of the Dallas Mavericks led to an NBA realignment that sent the Rockets back to the Western Conference. Houston qualified for the playoffs only in the final game of the season with a 40–42 record. The postseason had the Rockets beat the Lakers, in-state rivals San Antonio Spurs, and the equally underdog Kansas City Kings to become only the second team in NBA history (after the 1959 Minneapolis Lakers) to have advanced to the Finals after achieving a losing record in the regular season.', 'references': 314, 'contributors': 2495, 'last_updated': '2024-03-08 00:06'}}

Claim: Syria includes Jewish people.
Label: NOT ENOUGH INFO
Result: SUPPORTS
Result details: {'result': 'correct', 'explanation': 'The text clearly states that there have been Jews in Syria since ancient times.', 'article': {'url': 'http://en.wikipedia.org/?curid=5531820', 'page_url': 'http://en.wikipedia.org/?curid=5531820', 'section_url': 'http://en.wikipedia.org/?curid=5531820#Lead_Section', 'title': 'Syrian Jews', 'section': 'Lead Section', 'snippet': "There have been Jews in Syria since ancient times: according to the community's tradition, since the time of King David, and certainly since early Roman times.", 'references': 42, 'contributors': 900, 'last_updated': '2024-03-15 20:51'}}

Claim: Laura Linney was in a movie.
Label: REFUTES
Result: SUPPORTS
Result details: {'result': 'correct', 'explanation': "The quote confirms that Laura Linney starred in the movie 'You Can Count on Me' in 2000.", 'article': {'url': 'http://en.wikipedia.org/?curid=375670', 'page_url': 'http://en.wikipedia.org/?curid=375670', 'section_url': 'http://en.wikipedia.org/?curid=375670#2000s', 'title': 'Laura Linney', 'section': '2000s', 'snippet': 'In 2000, she starred in Kenneth Lonergan\'s film You Can Count on Me alongside Mark Ruffalo and Matthew Broderick. The film was met with positive reviews from critics with an approval rating of 95% on Rotten Tomatoes, the consensus reading, "You Can Count on Me may look like it belongs on the small screen, but the movie surprises with its simple yet affecting story. Beautifully acted and crafted, the movie will simply draw you in." Linney was nominated for an Academy Award for Best Actress for her performance as the small town single mother Sammy Prescott.', 'references': 140, 'contributors': 787, 'last_updated': '2024-02-05 17:53'}}

Claim: Asia contains places.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'partially_correct', 'explanation': 'The quote acknowledges that Asia is made up of various regions with independent definitions, suggesting that Asia is more than just a collection of places.', 'article': {'url': 'http://en.wikipedia.org/?curid=1318227', 'page_url': 'http://en.wikipedia.org/?curid=1318227', 'section_url': 'http://en.wikipedia.org/?curid=1318227#Lead_Section', 'title': 'Geography of Asia', 'section': 'Lead Section', 'snippet': 'The area of Asia is not the sum of the areas of each of its regions, which have been defined independently of the whole.', 'references': 23, 'contributors': 500, 'last_updated': '2024-01-14 17:48'}}

Claim: Edge of Tomorrow was released in Portugal.
Label: NOT ENOUGH INFO
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The text does not provide information about the release of Edge of Tomorrow in Portugal.', 'article': {'url': 'http://en.wikipedia.org/?curid=37098299', 'page_url': 'http://en.wikipedia.org/?curid=37098299', 'section_url': 'http://en.wikipedia.org/?curid=37098299#Box_office_forecast', 'title': 'Edge of Tomorrow', 'section': 'Box office forecast', 'snippet': 'movie industry."\n\nIn the week prior to the release of Edge of Tomorrow in North America, its estimated opening-weekend gross increased from the  range to .', 'references': 291, 'contributors': 1231, 'last_updated': '2024-03-17 03:30'}}

Claim: Moana is a 2016 novel.
Label: REFUTES
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Maria Sharapova tested positive for a substance that has been banned by the president.
Label: NOT ENOUGH INFO
Result: REFUTES
Result details: {'result': 'partially_correct', 'explanation': 'The substance was banned by the International Tennis Federation, not the president.', 'article': {'url': 'http://en.wikipedia.org/?curid=58397322', 'page_url': 'http://en.wikipedia.org/?curid=58397322', 'section_url': 'http://en.wikipedia.org/?curid=58397322#Maria_Sharapova_(2016)', 'title': 'Doping in tennis', 'section': 'Maria Sharapova (2016)', 'snippet': "On 1 January 2016, the drug was placed on the International Tennis Federation's banned substance list as it was ruled to have performance-enhancing effects.", 'references': 57, 'contributors': 34, 'last_updated': '2024-03-03 16:29'}}

Claim: 100 Greatest of All Time first premiered on the Tennis Channel.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': "The text does not mention that the '100 Greatest of All Time' list first premiered on the Tennis Channel.", 'article': {'url': 'http://en.wikipedia.org/?curid=35191146', 'page_url': 'http://en.wikipedia.org/?curid=35191146', 'section_url': 'http://en.wikipedia.org/?curid=35191146#Lead_Section', 'title': '100 Greatest of All Time', 'section': 'Lead Section', 'snippet': 'An international panel of tennis experts determined this ranking of 62 men and 38 women.', 'references': 2, 'contributors': 429, 'last_updated': '2024-02-04 16:17'}}

Claim: Birth of the Dragon comes out in summer 2017.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The movie was released in August 2016, not 2017.', 'article': {'url': 'http://en.wikipedia.org/?curid=47830506', 'page_url': 'http://en.wikipedia.org/?curid=47830506', 'section_url': 'http://en.wikipedia.org/?curid=47830506#Box_office', 'title': 'Birth of the Dragon', 'section': 'Box office', 'snippet': 'In North America, Birth of the Dragon was released on August 25, 2017, alongside All Saints and Leap!, and was projected to gross around $3 million from 1,618 theaters in its opening weekend.', 'references': 26, 'contributors': 117, 'last_updated': '2024-02-28 19:02'}}

Claim: Emily Ratajkowski was in a commercial for an upscale car brand during a Super Bowl.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'There is no mention of Emily Ratajkowski or an upscale car brand in the provided text passages.', 'article': {'url': 'http://en.wikipedia.org/?curid=26114145', 'page_url': 'http://en.wikipedia.org/?curid=26114145', 'section_url': 'http://en.wikipedia.org/?curid=26114145#1970_(IV)', 'title': 'List of Super Bowl commercials', 'section': '1970 (IV)', 'snippet': 'her. Everything about this is describ', 'references': 871, 'contributors': 463, 'last_updated': '2024-03-11 00:46'}}

Claim: Sleeping Beauty premiered in May 2011.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Beauty Shop is an original work.
Label: REFUTES
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Philadelphia contains 67 National Historic Landmarks.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The quote does not provide information about the number of National Historic Landmarks in Philadelphia.', 'article': {'url': 'http://en.wikipedia.org/?curid=15023927', 'page_url': 'http://en.wikipedia.org/?curid=15023927', 'section_url': 'http://en.wikipedia.org/?curid=15023927#Lead_Section', 'title': 'List of National Historic Landmarks in Philadelphia', 'section': 'Lead Section', 'snippet': 'Current listings\n\n|}', 'references': 139, 'contributors': 60, 'last_updated': '2024-02-26 16:29'}}

Claim: Eddie Redmayne appeared in a film.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': "Eddie Redmayne did not appear in the film 'The Parting Glass'. The film features Ed Asner in the role of Tommy.", 'article': {'url': 'http://en.wikipedia.org/?curid=38045015', 'page_url': 'http://en.wikipedia.org/?curid=38045015', 'section_url': 'http://en.wikipedia.org/?curid=38045015#Lead_Section', 'title': 'Ed Asner filmography', 'section': 'Lead Section', 'snippet': 'The Parting GlassTommyW', 'references': 2, 'contributors': 250, 'last_updated': '2024-02-21 19:15'}}

Claim: Amelia Earhart was Canadian.
Label: NOT ENOUGH INFO
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The Great Canadian Theatre Company hosted a play about Amelia Earhart, but she was not Canadian. She was American.', 'article': {'url': 'http://en.wikipedia.org/?curid=85234', 'page_url': 'http://en.wikipedia.org/?curid=85234', 'section_url': 'http://en.wikipedia.org/?curid=85234#In_popular_culture', 'title': 'Amelia Earhart', 'section': 'In popular culture', 'snippet': 'In 2011, the Great Canadian Theatre Company hosted a musical play titled Amelia: The Girl Who Wants To Fly.. Great Canadian Theatre Company. Retrieved: October 6, 2011.', 'references': 403, 'contributors': 2433, 'last_updated': '2024-03-18 16:07'}}

Claim: Death in Paradise has enjoyed high ratings and is highly acclaimed.
Label: NOT ENOUGH INFO
Result: REFUTES
Result details: {'result': 'partially_correct', 'explanation': 'The show has been well-received, but the passage does not specifically mention high ratings', 'article': {'url': 'http://en.wikipedia.org/?curid=33529086', 'page_url': 'http://en.wikipedia.org/?curid=33529086', 'section_url': 'http://en.wikipedia.org/?curid=33529086#Lead_Section', 'title': 'Death in Paradise (TV series)', 'section': 'Lead Section', 'snippet': 'Detective Inspector Richard Poole (Ben Miller) is sent from the Metropolitan Police in London to investigate the murder of a British police officer on the fictional Caribbean island of Saint Marie,', 'references': 84, 'contributors': 834, 'last_updated': '2024-03-18 15:11'}}

Claim: London is in Greater London.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'partially_correct', 'explanation': 'London is part of Greater London, but the City of London has a unique government separate from the rest of Greater London.', 'article': {'url': 'http://en.wikipedia.org/?curid=45367389', 'page_url': 'http://en.wikipedia.org/?curid=45367389', 'section_url': 'http://en.wikipedia.org/?curid=45367389#Local_government', 'title': 'Greater London', 'section': 'Local government', 'snippet': 'Greater London is divided into 32 London Boroughs, each governed by a London Borough council. The City of London has a unique government dating back to the 12th century and is separate from the county of Greater London, although is still part of the region served by the Greater London Authority.', 'references': 82, 'contributors': 950, 'last_updated': '2024-03-10 10:31'}}

Claim: The Eagles were active in the 1970s.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The statement is incorrect as it refers to the period after the Eagles broke up, not during their active years in the 1970s.', 'article': {'url': 'http://en.wikipedia.org/?curid=90785', 'page_url': 'http://en.wikipedia.org/?curid=90785', 'section_url': 'http://en.wikipedia.org/?curid=90785#Lead_Section', 'title': 'Eagles (band)', 'section': 'Lead Section', 'snippet': 'After the Eagles broke up, the former members pursued solo careers.', 'references': 145, 'contributors': 2829, 'last_updated': '2024-02-20 05:03'}}

Claim: The 2013 NBA draft was planned at Barclays Center in Brooklyn.
Label: NOT ENOUGH INFO
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The quote does not provide information about the location of the 2013 NBA draft.', 'article': {'url': 'http://en.wikipedia.org/?curid=38000871', 'page_url': 'http://en.wikipedia.org/?curid=38000871', 'section_url': 'http://en.wikipedia.org/?curid=38000871#Draft_lottery', 'title': '2013 NBA draft', 'section': 'Draft lottery', 'snippet': 'The first 14 picks in the draft belong to teams that miss the playoffs; the order was determined through a lottery. The lottery determined the three teams that will obtain the first three picks on the draft.', 'references': 87, 'contributors': 487, 'last_updated': '2024-03-10 10:03'}}

Claim: Mickey Rooney was in a movie.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'partially_correct', 'explanation': "The quote mentions Mickey Rooney's nomination for an Academy Award, which is related to his involvement in movies, but it does not specifically mention a movie title.", 'article': {'url': 'http://en.wikipedia.org/?curid=44788090', 'page_url': 'http://en.wikipedia.org/?curid=44788090', 'section_url': 'http://en.wikipedia.org/?curid=44788090#Lead_Section', 'title': 'Mickey Rooney filmography', 'section': 'Lead Section', 'snippet': 'At age of only 19 years old, Mickey Rooney became the second-youngest Academy Award Best Actor nominee.', 'references': 10, 'contributors': 103, 'last_updated': '2023-12-20 14:37'}}

Claim: Believe is a work.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': "The statement is incorrect as the quote is about the album 'Make Believe' and not 'Believe'.", 'article': {'url': 'http://en.wikipedia.org/?curid=1595551', 'page_url': 'http://en.wikipedia.org/?curid=1595551', 'section_url': 'http://en.wikipedia.org/?curid=1595551#Lead_Section', 'title': 'Make Believe (Weezer album)', 'section': 'Lead Section', 'snippet': "Make Believe marks a return to Cuomo's more personal songwriting style after taking a more distant approach on the previous two albums.", 'references': 68, 'contributors': 367, 'last_updated': '2024-03-10 04:26'}}

Claim: The Catalyst is a single by Linkin Park.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'partially_correct', 'explanation': "The text mentions a contest related to the single 'The Catalyst' by Linkin Park, but does not explicitly state that 'The Catalyst' is a single.", 'article': {'url': 'http://en.wikipedia.org/?curid=28005401', 'page_url': 'http://en.wikipedia.org/?curid=28005401', 'section_url': 'http://en.wikipedia.org/?curid=28005401#Lead_Section', 'title': 'The Catalyst', 'section': 'Lead Section', 'snippet': 'The winner of this contest was Czesław "NoBraiN" Sakowski from Świdnica, Poland, whose remix was featured as an additional downloadable track on the Best Buy version of A Thousand Suns, and also available on Napster.', 'references': 75, 'contributors': 440, 'last_updated': '2023-12-19 01:57'}}

Claim: Wilt Chamberlain plays basketball.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'partially_correct', 'explanation': 'While Wilt Chamberlain was initially not interested in basketball, he eventually turned to the sport in seventh grade and went on to become one of the most dominant basketball players in NBA history.', 'article': {'url': 'http://en.wikipedia.org/?curid=255645', 'page_url': 'http://en.wikipedia.org/?curid=255645', 'section_url': 'http://en.wikipedia.org/?curid=255645#Lead_Section', 'title': 'Wilt Chamberlain', 'section': 'Lead Section', 'snippet': 'During early childhood, he was not interested in basketball, which he regarded as "a game for sissies".', 'references': 308, 'contributors': 2268, 'last_updated': '2024-03-12 04:39'}}

Claim: One Russian superhero is Black Widow.
Label: NOT ENOUGH INFO
Result: SUPPORTS
Result details: {'result': 'correct', 'explanation': 'The quote confirms that Black Widow is a Russian character who initially appeared as a Russian-spy antagonist.', 'article': {'url': 'http://en.wikipedia.org/?curid=709344', 'page_url': 'http://en.wikipedia.org/?curid=709344', 'section_url': 'http://en.wikipedia.org/?curid=709344#Lead_Section', 'title': 'Black Widow (Natasha Romanova)', 'section': 'Lead Section', 'snippet': 'The Black Widow\'s first appearances were as a recurring, non-costumed, Russian-spy antagonist in the feature "Iron Man", beginning in Tales of Suspense #52 (April 1964). Five issues later, she recruits the besotted costumed archer and later superhero Hawkeye to her cause.', 'references': 173, 'contributors': 1675, 'last_updated': '2024-03-15 01:19'}}

Claim: Captain America: The Winter Soldier is a work.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The statement is incorrect as it is incomplete and does not align with the content of the provided text passages.', 'article': {'url': 'http://en.wikipedia.org/?curid=36439749', 'page_url': 'http://en.wikipedia.org/?curid=36439749', 'section_url': 'http://en.wikipedia.org/?curid=36439749#Production', 'title': 'Captain America: The Winter Soldier', 'section': 'Production', 'snippet': 's Captain America: The Winter Soldier, and t', 'references': 438, 'contributors': 1370, 'last_updated': '2024-03-12 09:13'}}

Claim: Robert Redford was a guest star on a show.
Label: SUPPORTS
Result: REFUTES
Result details: None

Claim: The Matrix stars Keanu Reeves as John Matrix.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'incorrect', 'explanation': 'Keanu Reeves portrays the character Thomas Anderson, also known as Neo, not John Matrix, in The Matrix franchise.', 'article': {'url': 'http://en.wikipedia.org/?curid=16603', 'page_url': 'http://en.wikipedia.org/?curid=16603', 'section_url': "http://en.wikipedia.org/?curid=16603#1999–2004:_Stardom_with_''The_Matrix''_franchise_and_comedies", 'title': 'Keanu Reeves', 'section': "1999–2004: Stardom with ''The Matrix'' franchise and comedies", 'snippet': 'In 1999, Reeves starred in the critically acclaimed science fiction film The Matrix, the first installment in what would become The Matrix franchise. Reeves portrays computer programmer Thomas Anderson, a hacker using the alias "Neo", who discovers humanity is trapped inside a simulated reality created by intelligent machines.', 'references': 555, 'contributors': 2858, 'last_updated': '2024-03-16 07:30'}}

Claim: Roman Reigns is a three-time born again Christian.
Label: NOT ENOUGH INFO
Result: SUPPORTS
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}
