from app.services.keyword_extractor import KeywordExtractor
from langchain_groq import ChatGroq
import pytest

@pytest.mark.asyncio
async def test_extract_keyword():
    groq = ChatGroq(model="llama3-8b-8192", temperature=0)
    extractor = KeywordExtractor(groq)
    result = await extractor.extract("Claim: Rishi Sunak announces UK general election for Thursday 4 July 2024")
    assert "Rishi Sunak" in result.result.keywords
    assert "general election" in result.result.search_term
