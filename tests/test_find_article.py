import pytest

from app.services.find_articles import BasicArticleFinder
from app.services.keyword_extractor import KeywordExtractionResult
from app.utils.httpx_client import make_httpx_client

@pytest.mark.asyncio
async def test_find_article():
    client = make_httpx_client()
    finder = BasicArticleFinder(client)
    kw = KeywordExtractionResult(search_term="Rishi Sunak", keywords=["Rishi Sunak"])
    result = await finder.find_articles(kw)
    assert len(result) > 0
    assert "Rishi Sunak" in result[0].title
    assert result[0].pageid == 46622934

@pytest.mark.asyncio
async def test_find_article_no_result():
    client = make_httpx_client()
    finder = BasicArticleFinder(client)
    kw = KeywordExtractionResult(search_term="ASDkjsn askdfn akjnfASDF ", keywords=[])
    result = await finder.find_articles(kw)
    assert len(result) == 0
