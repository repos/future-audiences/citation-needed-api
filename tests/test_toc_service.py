import pytest
from app.services.wme_service import WmeService
from wme.on_demand import OnDemand
from wme.auth import login, revoke_token
from app.utils.httpx_client import make_httpx_client
import os

@pytest.mark.asyncio
async def test_toc_service():
    WME_USERNAME = os.environ.get("WME_USERNAME")
    WME_PASSWORD = os.environ.get("WME_PASSWORD")
    client = make_httpx_client()

    creds = await login(WME_USERNAME, WME_PASSWORD, client)
    try:
        on_demand = OnDemand(creds, client=client)
        toc_service = WmeService(on_demand)
        toc = await toc_service.fetch_articles(["Rishi Sunak"])
        assert len(toc) > 0
    finally:
        await revoke_token(creds.refresh_token, client)

@pytest.mark.asyncio
async def test_toc_service_batch():
    WME_USERNAME = os.environ.get("WME_USERNAME")
    WME_PASSWORD = os.environ.get("WME_PASSWORD")
    client = make_httpx_client()

    creds = await login(WME_USERNAME, WME_PASSWORD, client)
    try:
        on_demand = OnDemand(creds, client=client)
        toc_service = WmeService(on_demand)
        toc = await toc_service.fetch_articles(["Rishi Sunak", "Boris Johnson"])
        assert len(toc) == 2
    finally:
        await revoke_token(creds.refresh_token, client)


@pytest.mark.asyncio
async def test_wme_failings():
    WME_USERNAME = os.environ.get("WME_USERNAME")
    WME_PASSWORD = os.environ.get("WME_PASSWORD")
    client = make_httpx_client()

    creds = await login(WME_USERNAME, WME_PASSWORD, client)
    try:
        on_demand = OnDemand(creds, client=client)
        toc_service = WmeService(on_demand)
        # National Aeronautics and Space Administration is a redirect page so it fails
        toc = await toc_service.fetch_articles(["National Aeronautics and Space Administration", "Boris Johnson"])
        assert len(toc) == 1
    finally:
        await revoke_token(creds.refresh_token, client)