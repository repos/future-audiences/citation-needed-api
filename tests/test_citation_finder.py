# from typing import Tuple, List

# from unittest.mock import create_autospec
# from app.models.llm.interaction import KeywordExtractionResult, QuoteSelectionResult, SectionSelectionResult
# from app.services.chatgpt_llm_service import ChatGPTLLMService

# import pytest

# from app.models.search.search import GoogleSearchResult, WikipediaSearchResult
# from app.services.feature_flagging_service import UnleashFeatureFlaggingService
# from app.services.wikipedia_search_service import WikipediaSearchService
# from app.utils.settings_types import SearchProviderEnum


# class TestArticleFetcherService:
#     @pytest.fixture
#     def mock_chatgpt_llm_service(self):
#         return create_autospec(ChatGPTLLMService)

#     @pytest.fixture
#     def mock_wikipedia_search_service(self):
#         return create_autospec(WikipediaSearchService)

#     @pytest.fixture
#     def mock_feature_flagging_service(self):
#         return create_autospec(UnleashFeatureFlaggingService)

#     @pytest.fixture
#     def mock_chatgpt_keyword_result(self):
#         return create_autospec(KeywordExtractionResult)
    
#     @pytest.fixture
#     def mock_chatgpt_section_selection_result(self):
#         return create_autospec(SectionSelectionResult)
    
#     @pytest.fixture
#     def mock_chatgpt_quote_selection_result(self):
#         return create_autospec(QuoteSelectionResult)

#     @pytest.fixture
#     def mock_wikipedia_search_result(self):
#         return create_autospec(WikipediaSearchResult)

#     @pytest.fixture
#     def mock_wikipedia_tables_of_content_result(self):
#         return create_autospec(List[Tuple[str, List[str]]])
    
#     @pytest.fixture
#     def mock_wikipedia_fetch_sections_result(self):
#         return create_autospec(List[Tuple[str, str, str]])

#     @pytest.fixture
#     def article_fetcher_service(self,
#                                 mock_google_search_service,
#                                 mock_wikipedia_search_service,
#                                 mock_feature_flagging_service):
#         return ArticleFetcherService(
#             query='test query',
#             query_language='en',
#             preferred_search_provider=SearchProviderEnum.Wikipedia,
#             feature_flagging_service=mock_feature_flagging_service,
#             google_search_service=mock_google_search_service,
#             wikipedia_search_service=mock_wikipedia_search_service,
#         )

#     def test_get_search_results_with_disallowed_google(self,
#                                                        article_fetcher_service,
#                                                        mock_google_search_service,
#                                                        mock_wikipedia_search_service,
#                                                        mock_feature_flagging_service,
#                                                        mock_wikipedia_search_result):
#         # Set up mock return values
#         mock_feature_flagging_service.google_search_is_enabled.return_value = False
#         mock_wikipedia_search_service.perform_search.return_value = mock_wikipedia_search_result

#         # Run the method
#         results = article_fetcher_service._get_search_results()

#         # Check the results
#         assert results.google is None

#         # Verify that the mocked methods were called with the correct parameters
#         mock_google_search_service.perform_search.assert_not_called()

#         mock_wikipedia_search_service.perform_search.assert_called_once_with(
#             search_term=article_fetcher_service.query,
#             search_language=article_fetcher_service.query_language,
#         )

#     def test_get_search_results_with_allowed_google(self,
#                                                     article_fetcher_service,
#                                                     mock_google_search_service,
#                                                     mock_wikipedia_search_service,
#                                                     mock_feature_flagging_service,
#                                                     mock_google_search_result,
#                                                     mock_wikipedia_search_result):
#         # Set up mock return values
#         mock_feature_flagging_service.google_search_is_enabled.return_value = True

#         mock_google_search_service.perform_search.return_value = mock_google_search_result
#         mock_wikipedia_search_service.perform_search.return_value = mock_wikipedia_search_result

#         # Run the method
#         results = article_fetcher_service._get_search_results()
#         assert results.google is not None
#         assert results.wikipedia is not None

#         # Verify that the mocked methods were called with the correct parameters
#         mock_google_search_service.perform_search.assert_called_once_with(
#             search_term=article_fetcher_service.query,
#             search_language=article_fetcher_service.query_language,
#         )

#         mock_wikipedia_search_service.perform_search.assert_called_once_with(
#             search_term=article_fetcher_service.query,
#             search_language=article_fetcher_service.query_language,
#         )
