from locust import HttpUser, task, between
import urllib.parse


# class TestSettings(BaseSettings):
#     bearer_token: str | None

#     class Config():
#         """
#         Config for the Settings class
#         """
#         env_file = ".env"


# settings = TestSettings()  # pyright: ignore # Our vars are set via env vars

TEXT_SELECTION = "Taylor Swift is shaking it off the charts. One of the pop superstar's recent The Eras Tour performances at Lumen Field in Seattle, Washington, triggered seismic activity equivalent to a 2.3 magnitude earthquake, according to local seismologists."
PARSED_SELECTION = urllib.parse.quote(TEXT_SELECTION)
class PerformanceTests(HttpUser):
    network_timeout = 120.0
    connection_timeout = 120.0
    wait_time = between(1, 3)

    # @task(3)
    # def test_health_check(self):
    #     res = self.client.get("")
    #     print("res", res.json())

    @task(1)
    def test_find_citation(self):
        res = self.client.get("perf_mock_llm_mock_xtool_no_feature_flagging", params={"selection": PARSED_SELECTION})
        # res is a StreamingResponse
        print("res", res.content)

        # print("res", res)