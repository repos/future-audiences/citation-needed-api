from app.utils.settings import get_settings
from fastapi import APIRouter

router = APIRouter()

@router.get("/", include_in_schema=False)
async def hello_world() -> str:
    """ Hello World endpoint for sanity checking """
    settings = get_settings()
    return "You've reached the health check endpoint. The environment is: " + settings.environment
