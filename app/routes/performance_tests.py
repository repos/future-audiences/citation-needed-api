from app.prompt_constants import FLUENT_KW_PROMPT, FLUENT_QUOTE_PROMPT
from app.services.feature_flagging_service import get_feature_flagging_service
from app.services.mock_article_details_service import MockArticleDetailsService
from app.services.mock_feature_flagging_service import MockFeatureFlaggingService
from app.services.mock_llm_service import MockLLMService
from fastapi.responses import StreamingResponse
from typing import Any
from fastapi import APIRouter, HTTPException, status, Request
from structlog import getLogger
from fastapi.responses import JSONResponse

from app.models.parameters import FactCheckerRequestInfo
import json
import time
from app.utils.settings_types import SearchStrategy, SystemEnum
from app.services.citation_finder import CitationFinderService
from app.services.wikipedia_search_service import WikipediaSearchService
from app.services.xtools_article_details_service import XtoolsArticleDetailsService

router = APIRouter()

logger = getLogger(__name__)


def run_perf_stream() -> Any:
    for i in range(10):
        yield json.dumps({"info": f"step {i}"})
        time.sleep(0.5)

    
@router.get('/perf_stream')
async def perf_stream(selection: str) -> StreamingResponse:
    FactCheckerRequestInfo(selection=selection)
    
    return StreamingResponse(run_perf_stream(), media_type='application/x-ndjson')

    
@router.get('/perf')
async def perf(selection: str) -> StreamingResponse:
    FactCheckerRequestInfo(selection=selection)
    
    result = []
    for i in range(10):
        result.append({"info": f"step {i}"})
        time.sleep(0.5)

    return JSONResponse(content=result, media_type='application/json')


@router.get('/perf_mock_llm')
async def perf_stream_mock_llm(selection: str, request: Request) -> StreamingResponse:
    feature_flagging_service = get_feature_flagging_service()
    
    if not feature_flagging_service.api_is_enabled():
        raise HTTPException(status_code=status.HTTP_503_SERVICE_UNAVAILABLE, detail="API_DISABLED")
    
    full_request_info = FactCheckerRequestInfo(selection=selection, 
                                                client_id=f"perf-from: '{request.client.host}'", 
                                                site_category="Others")

    logger.info("Initial request received",
                source=SystemEnum.User,
                destination=SystemEnum.CitationNeededAPI,
                info=full_request_info.dict())
    
    if feature_flagging_service.user_is_on_blocklist(user_ip=request.client.host):
        raise HTTPException(status_code=status.HTTP_429_TOO_MANY_REQUESTS, detail="USER_BLOCKED")
    
    # Override the user agent param to not have these requests be counted witht he citation-needed
    # real usage
    search_service = WikipediaSearchService()
    search_service.set_user_agent("citation-needed-load-testing")
    
    fact_checker_service = CitationFinderService(search_strategy=SearchStrategy.TocAndSections,
                                                llm_service=MockLLMService(do_text_lookup=True),
                                                search_service=search_service,
                                                article_details_service=XtoolsArticleDetailsService(),
                                                feature_flagging_service=feature_flagging_service,
                                                instruction_keywords=FLUENT_KW_PROMPT,
                                                instruction_quote=FLUENT_QUOTE_PROMPT)
    
    return StreamingResponse(fact_checker_service.stream_citation_finding_no_section_selection(full_request_info), media_type='application/x-ndjson')

@router.get('/perf_mock_llm_no_lookup')
async def perf_stream_mock_llm_no_lookup(selection: str, request: Request) -> StreamingResponse:
    feature_flagging_service = get_feature_flagging_service()
    
    if not feature_flagging_service.api_is_enabled():
        raise HTTPException(status_code=status.HTTP_503_SERVICE_UNAVAILABLE, detail="API_DISABLED")
    
    full_request_info = FactCheckerRequestInfo(selection=selection, 
                                                client_id=f"perf-from: '{request.client.host}'", 
                                                site_category="Others")

    logger.info("Initial request received",
                source=SystemEnum.User,
                destination=SystemEnum.CitationNeededAPI,
                info=full_request_info.dict())


    if feature_flagging_service.user_is_on_blocklist(user_ip=request.client.host):
        raise HTTPException(status_code=status.HTTP_429_TOO_MANY_REQUESTS, detail="USER_BLOCKED")
    
    # Override the user agent param to not have these requests be counted witht he citation-needed
    # real usage
    search_service = WikipediaSearchService()
    search_service.set_user_agent("citation-needed-load-testing")
    
    fact_checker_service = CitationFinderService(search_strategy=SearchStrategy.TocAndSections,
                                                llm_service=MockLLMService(do_text_lookup=False),
                                                search_service=search_service,
                                                article_details_service=XtoolsArticleDetailsService(),
                                                feature_flagging_service=feature_flagging_service,
                                                instruction_keywords=FLUENT_KW_PROMPT,
                                                instruction_quote=FLUENT_QUOTE_PROMPT)
    
    return StreamingResponse(fact_checker_service.stream_citation_finding_no_section_selection(full_request_info), media_type='application/x-ndjson')

@router.get('/perf_mock_llm_mock_xtool')
async def perf_stream_mock_llm_mock_xtool(selection: str, request: Request) -> StreamingResponse:
    feature_flagging_service = get_feature_flagging_service()
    
    if not feature_flagging_service.api_is_enabled():
        raise HTTPException(status_code=status.HTTP_503_SERVICE_UNAVAILABLE, detail="API_DISABLED")
    
    full_request_info = FactCheckerRequestInfo(selection=selection, 
                                                client_id=f"perf-from: '{request.client.host}'", 
                                                site_category="Others")

    logger.info("Initial request received",
                source=SystemEnum.User,
                destination=SystemEnum.CitationNeededAPI,
                info=full_request_info.dict())
    
    if feature_flagging_service.user_is_on_blocklist(user_ip=request.client.host):
        raise HTTPException(status_code=status.HTTP_429_TOO_MANY_REQUESTS, detail="USER_BLOCKED")
        
    # Override the user agent param to not have these requests be counted witht he citation-needed
    # real usage
    search_service = WikipediaSearchService()
    search_service.set_user_agent("citation-needed-load-testing")
    
    fact_checker_service = CitationFinderService(search_strategy=SearchStrategy.TocAndSections,
                                                llm_service=MockLLMService(do_text_lookup=False),
                                                search_service=search_service,
                                                article_details_service=MockArticleDetailsService(),
                                                feature_flagging_service=feature_flagging_service,
                                                instruction_keywords=FLUENT_KW_PROMPT,
                                                instruction_quote=FLUENT_QUOTE_PROMPT)
    
    return StreamingResponse(fact_checker_service.stream_citation_finding_no_section_selection(full_request_info), media_type='application/x-ndjson')

@router.get('/perf_mock_llm_mock_xtool_no_feature_flagging')
async def perf_stream_mock_llm_mock_xtool_no_ff(selection: str, request: Request) -> StreamingResponse:
    
    full_request_info = FactCheckerRequestInfo(selection=selection, 
                                                client_id=f"perf-from: '{request.client.host}'", 
                                                site_category="Others")
   
    # Override the user agent param to not have these requests be counted witht he citation-needed
    # real usage
    search_service = WikipediaSearchService()
    search_service.set_user_agent("citation-needed-load-testing")

    fact_checker_service = CitationFinderService(search_strategy=SearchStrategy.TocAndSections,
                                                llm_service=MockLLMService(do_text_lookup=False),
                                                search_service=search_service,
                                                article_details_service=MockArticleDetailsService(),
                                                feature_flagging_service=MockFeatureFlaggingService(),
                                                instruction_keywords=FLUENT_KW_PROMPT,
                                                instruction_quote=FLUENT_QUOTE_PROMPT)
    
    return StreamingResponse(fact_checker_service.stream_citation_finding_no_section_selection(full_request_info), media_type='application/x-ndjson')
