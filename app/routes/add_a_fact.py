from typing import Annotated

import httpx
from app.config.server_config import get_wme_auth
from app.services.add_a_fact import AddAFactService
from app.services.article_picker_service import ArticlePicker
from app.services.find_articles import BasicArticleFinder, MultiArticleFinder
from app.services.find_articles_brave import BraveArticleFinder
from app.services.keyword_extractor import KeywordExtractor
from app.services.quote_extractor import QuoteExtractor
from app.services.summary_service import FactSummarizer
from app.services.wme_service import WmeService
from app.utils.langchain_util import get_model
from app.utils.settings import get_settings
from fastapi.responses import StreamingResponse
from fastapi import APIRouter, HTTPException, Request, status, Header
from structlog import getLogger

from app import constants
from app.models.parameters import (FactCheckerRequestInfo)
from app.services.feature_flagging_service import get_feature_flagging_service
from app.utils.settings_types import SystemEnum
from langchain_core.language_models.chat_models import BaseChatModel

from wme.on_demand import OnDemand

router = APIRouter()

logger = getLogger(__name__)
    
@router.get('/add_a_fact')
async def add_a_fact(selection: str, 
                        request: Request,
                        context: str | None = None,
                        x_user_id: Annotated[str | None, Header()] = None,
                        x_site_category: Annotated[str | None, Header()] = None,
                        x_site_reliability: Annotated[str | None, Header()] = None,
                        ) -> StreamingResponse:
    feature_flagging_service = get_feature_flagging_service()
    if not feature_flagging_service.api_is_enabled():
        raise HTTPException(status_code=status.HTTP_503_SERVICE_UNAVAILABLE, detail="API_DISABLED")
    

    settings = get_settings()
    client: httpx.AsyncClient = request.app.requests_client

    access_token = await get_wme_auth()
    on_demand = OnDemand(access_token,  client)
    wme_service = WmeService(on_demand)

    llm: BaseChatModel = get_model(feature_flagging_service, client)

    article_picker_service = ArticlePicker(llm)
    search_service = BasicArticleFinder(client, 6)
    brave_search_service = BraveArticleFinder(client, settings.brave_api_key, 6)
    search_service = MultiArticleFinder([search_service, brave_search_service])
    summarizer_service = FactSummarizer(llm)
    quote_service = QuoteExtractor(llm)
    kw_extractor = KeywordExtractor(llm)

    # enforce maxmimum input length
    selection = selection.strip()[0:constants.MAX_INPUT_LENGTH]
    context = context.strip()[0:constants.MAX_INPUT_LENGTH] if context else None

    # make sure that the provided x_site_category is in the allowed list, otherwise use "Others"
    if x_site_category not in constants.ALLOWED_SITE_CATEGORY_VALUES:
        x_site_category = "Others"

    if not x_site_reliability:
        x_site_reliability = "UNKNOWN_SOURCE"

    user_id = x_user_id if x_user_id is not None else "anonymous"
    full_request_info = FactCheckerRequestInfo(selection=selection,
                                               context=context,
                                               client_id=user_id, 
                                               site_category=x_site_category,
                                               site_reliability=x_site_reliability)

    
    if feature_flagging_service.user_is_on_blocklist(user_id=user_id):
        logger.info("User is on blocklist",
                    source=SystemEnum.User,
                    destination=SystemEnum.AddAFactAPI,
                    info=full_request_info.model_dump())
        raise HTTPException(status_code=status.HTTP_429_TOO_MANY_REQUESTS, detail="USER_BLOCKED")
    
    # make sure that the provided claim is not empty. Consider white spaces as empty
    if not selection or not selection.strip() or selection.isspace():
        logger.info("Empty claim provided",
                    source=SystemEnum.User,
                    destination=SystemEnum.AddAFactAPI,
                    info=full_request_info.model_dump())
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="EMPTY_CLAIM")

    logger.info("Initial request received",
                source=SystemEnum.User,
                destination=SystemEnum.AddAFactAPI,
                info=full_request_info.model_dump())
    
    add_a_fact_service = AddAFactService(wme_service=wme_service,
                                                summarizer_service=summarizer_service,
                                                article_picker=article_picker_service,
                                                kw_extractor=kw_extractor,
                                                search_service=search_service,
                                                quote_service=quote_service,
                                                feature_flagging_service=get_feature_flagging_service())
    
    return StreamingResponse(add_a_fact_service.stream_add_a_fact(full_request_info),
                             media_type='application/x-ndjson')
