import asyncio
from typing import Annotated

import httpx
from app.config.server_config import get_wme_auth
from app.services.add_a_fact import AddAFactResponse
from app.services.find_articles import BasicArticleFinder, MultiArticleFinder
from app.services.find_articles_brave import BraveArticleFinder
from app.services.heuristic_section_ranker import HeuristicSectionRanker, heuristic_score
from app.services.keyword_extractor import KeywordExtractor
from app.services.quote_extractor import QuoteExtractor
from app.services.quote_matcher import quote_matcher
from app.services.summary_service import FactSummarizer, FactSummarizerResult
from app.services.wme_service import WmeService
from app.utils.langchain_helper import ResultWithTokens
from app.utils.langchain_util import get_model
from app.utils.settings import get_settings
from fastapi import APIRouter, HTTPException, Request, status, Header
from structlog import getLogger

from app import constants
from app.models.parameters import (FactCheckerRequestInfo)
from app.services.feature_flagging_service import get_feature_flagging_service
from app.utils.settings_types import SystemEnum
from langchain_core.language_models.chat_models import BaseChatModel

from wme.on_demand import OnDemand


router = APIRouter()

logger = getLogger(__name__)
    
@router.get('/add_a_fact_per_article')
async def add_a_fact_per_article(selection: str, 
                        manual_article: str,
                        request: Request,
                        context: str | None = None,
                        x_user_id: Annotated[str | None, Header()] = None,
                        x_site_category: Annotated[str | None, Header()] = None,
                        x_site_reliability: Annotated[str | None, Header()] = None,
                        ) -> AddAFactResponse:

    feature_flagging_service = get_feature_flagging_service()
    if not feature_flagging_service.api_is_enabled():
        raise HTTPException(status_code=status.HTTP_503_SERVICE_UNAVAILABLE, detail="API_DISABLED")
    

    settings = get_settings()
    client: httpx.AsyncClient = request.app.requests_client

    access_token = await get_wme_auth()
    on_demand = OnDemand(access_token,  client)
    wme_service = WmeService(on_demand)

    llm: BaseChatModel = get_model(feature_flagging_service, client)

    search_service = BasicArticleFinder(client, 6)
    brave_search_service = BraveArticleFinder(client, settings.brave_api_key, 6)
    search_service = MultiArticleFinder([search_service, brave_search_service])
    summarizer_service = FactSummarizer(llm)
    quote_service = QuoteExtractor(llm)
    kw_extractor = KeywordExtractor(llm)

    # enforce maxmimum input length
    selection = selection.strip()[0:constants.MAX_INPUT_LENGTH]
    context = context.strip()[0:constants.MAX_INPUT_LENGTH] if context else None

    # make sure that the provided x_site_category is in the allowed list, otherwise use "Others"
    if x_site_category not in constants.ALLOWED_SITE_CATEGORY_VALUES:
        x_site_category = "Others"

    if not x_site_reliability:
        x_site_reliability = "UNKNOWN_SOURCE"

    user_id = x_user_id if x_user_id is not None else "anonymous"
    full_request_info = FactCheckerRequestInfo(selection=selection,
                                               context=context,
                                               manual_article=manual_article,
                                               client_id=user_id, 
                                               site_category=x_site_category,
                                               site_reliability=x_site_reliability)


    if feature_flagging_service.user_is_on_blocklist(user_id=user_id):
        logger.info("User is on blocklist",
                    source=SystemEnum.User,
                    destination=SystemEnum.AddAFactAPIManual,
                    info=full_request_info.model_dump())
        raise HTTPException(status_code=status.HTTP_429_TOO_MANY_REQUESTS, detail="USER_BLOCKED")

    # make sure that the provided claim is not empty. Consider white spaces as empty
    if not selection or not selection.strip() or selection.isspace():
        logger.info("Empty claim provided",
                    source=SystemEnum.User,
                    destination=SystemEnum.AddAFactAPIManual,
                    info=full_request_info.model_dump())
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="EMPTY_CLAIM")


    logger.info("Initial per article request received",
                source=SystemEnum.User,
                destination=SystemEnum.AddAFactAPIManual,
                info=full_request_info.model_dump())
    

    claim_summary = asyncio.ensure_future(summarizer_service.extract(selection, context, SystemEnum.AddAFactAPIManual))

    kw_result, article_contents = await asyncio.gather(kw_extractor.extract(selection, context=context, src=SystemEnum.AddAFactAPIManual), wme_service.fetch_articles([manual_article], search_language="en"))

    section_ranker = HeuristicSectionRanker(heuristic_score)
    ranked_sections = section_ranker.rank(kw_result.result, {manual_article: article_contents.get(manual_article)}, limit=6)

    quote_result = await quote_service.extract(selection, context, ranked_sections, src = SystemEnum.AddAFactAPIManual)

    summary: ResultWithTokens[FactSummarizerResult] = await claim_summary

    if quote_result.result:
        quote_match = None
        if quote_result.result.selected_quote:
            quote_match = quote_matcher(quote_result.result.selected_quote, ranked_sections)
        return AddAFactResponse(result=quote_result.result.result or "not supported", msg=quote_result.result.explanation or 'Fact not found in Wikipedia article', match=quote_match)
        
    return AddAFactResponse(result="Fact not found", msg="No matching information found in Wikipedia article.", match=None, fact_summary=summary.result.summary)