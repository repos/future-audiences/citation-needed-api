import httpx
from app.constants import USER_AGENT, HTTP_TIMEOUT

def make_httpx_client() -> httpx.AsyncClient:
    headers = {
        "User-Agent": USER_AGENT
    }
    return httpx.AsyncClient(headers=headers,
                             timeout=HTTP_TIMEOUT)