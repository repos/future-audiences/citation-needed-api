"""Module providing Settings for the application - picks up from .env file"""

from typing import Optional
from functools import lru_cache

from pydantic import SecretStr
from pydantic_settings import BaseSettings

from app.utils.settings_types import EnvironmentEnum


class EnvironmentSettings(BaseSettings):
    """
    Settings for the application

    Reads from .env file in the root directory, automatically
    changes settings names to uppercase.

    Some settings are obtained from a feature flag service.
    In the event that calls to this service fail, the fallback
    function is used to find a default value.
    """

    # The full address the app is running on, including port
    address: str

    # The port that uvicorn is running on
    uvicorn_port: int = 8000

    # Whether we should log to JSON - typically used in production
    log_json_format: bool = True
    log_level: str = "INFO"

    model_override: str | None = None

    # The environment we're running in
    environment: str = EnvironmentEnum.prod

    # Google API credentials
    openai_api_key: SecretStr
    openai_assistant_id: str

    # Wikimedia Enterprise API credentials
    wme_username: str
    wme_password: SecretStr

    # Groq API credentials
    groq_api_key: Optional[SecretStr]

    brave_api_key: Optional[SecretStr]

    openai_api_key: Optional[SecretStr]

    # Gitlab feature-flag related settings
    gitlab_feature_flag_api_url: str
    gitlab_feature_flag_instance_id: str

    tool_data_dir: str = "./"

    extended_call_secret: str

    log_gdrive_folder_id: str

    # class Config():
    #     """
    #     Config for the Settings class
    #     """
    #     env_file = ".env"


@lru_cache()
def get_settings() -> EnvironmentSettings:
    return EnvironmentSettings()  # pyright: ignore # Our vars are set via env vars
