import httpx
from langchain_core.language_models.chat_models import BaseChatModel
from langchain_groq import ChatGroq
from langchain_openai import ChatOpenAI
from structlog import getLogger

from app import constants
from app.services.feature_flagging_service import UnleashFeatureFlaggingService
from app.utils.settings import get_settings

logger = getLogger(__name__)


def get_model(feature_flagging_service: UnleashFeatureFlaggingService, http_async_client: httpx.AsyncClient) -> BaseChatModel:
    """ Returns the model to use based on the feature flagging service """
    settings = get_settings()


    llm_groq: BaseChatModel = ChatGroq(model=constants.GROQ_MODEL, api_key=settings.groq_api_key.get_secret_value(), http_async_client=http_async_client)
    llm_gpt4o: BaseChatModel = ChatOpenAI(model=constants.OPENAI_MODEL, api_key=settings.openai_api_key.get_secret_value(), http_async_client=http_async_client)

    if feature_flagging_service.use_groq():
        return llm_groq.with_fallbacks([llm_gpt4o])
    elif feature_flagging_service.use_openai():
        return llm_gpt4o.with_fallbacks([llm_groq])
    else:
        raise Exception("No model found")
