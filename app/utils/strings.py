

def trim_to_space(txt: str, max_len: int):
    end_pos = txt.find(' ', max_len)
    if end_pos == -1:
        end_pos = max_len
    return txt[:end_pos] + "..."

