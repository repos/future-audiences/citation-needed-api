from enum import StrEnum


class EnvironmentEnum(StrEnum):
    dev = 'dv'
    uat = 'uat'
    prod = 'pr'
    local = 'local'


class SystemEnum(StrEnum):
    """ Describes any system we might be interacting with,
    used for logging purposes """
    ChatGPT = 'ChatGPT'
    CitationNeededAPI = 'CitationNeededAPI'
    AddAFactAPI = 'AddAFactAPI'
    AddAFactAPIManual = 'AddAFactAPIManual'
    User = 'User'
    Wikipedia = 'Wikipedia'
    Brave = 'Brave'
    XTools = 'XTools'


class SearchStrategy(StrEnum):
    """ The strategy for searching Wikipedia"""
    Snippets = 'Snippets'
    TocAndSections = 'TocAndSections'

class LLMProviderEnum(StrEnum):
    """ The provider of the llm interaction """
    MockLLM = 'MockLLM'
    ChatGPT = 'ChatGPT'

class SearchProviderEnum(StrEnum):
    """ The provider of the search results """
    Wikipedia = 'Wikipedia'

class LLMInteractionStrategy(StrEnum):
    """ The strategy for searching Wikipedia"""
    FreeForm = 'FreeForm'
    JSONMode = 'JSONMode'
    ToolCalls = 'ToolCalls'

