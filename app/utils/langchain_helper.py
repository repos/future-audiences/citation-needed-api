
from typing import Generic, TypeVar
from langchain_core.pydantic_v1 import BaseModel as LangChainModel
from langchain_core.messages import BaseMessage, AIMessage

ResultT = TypeVar("ResultT")

class RawStructuredOutput(LangChainModel, Generic[ResultT]):
    raw: BaseMessage
    parsed: ResultT | None
    parsing_error: BaseException | None

    class Config:
        arbitrary_types_allowed = True

class ResultWithTokens(LangChainModel, Generic[ResultT]):
    """
    Object that contains the result of a structured output call along with metadata about the call e.g. token counts and time taken.
    """
    result: ResultT
    in_token_count: int | None
    out_token_count: int | None
    total_time: float | None

    @staticmethod
    def from_result(result: RawStructuredOutput[ResultT]) -> 'ResultWithTokens[ResultT]':
        in_token_count = None
        out_token_count = None
        total_time = None
        match result.raw:
            case AIMessage():
                metadata = result.raw.response_metadata
                token_usage = metadata.get("token_usage", {})
                in_token_count = token_usage.get('prompt_tokens')
                out_token_count = token_usage.get('completion_tokens')
                total_time = token_usage.get('total_time')

        if result.parsing_error:
            raise result.parsing_error
        return ResultWithTokens(result=result.parsed, in_token_count=in_token_count, out_token_count=out_token_count, total_time=total_time)
