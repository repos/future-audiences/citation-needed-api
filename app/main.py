""" Main entrypoint for the application. """

# grequests must be imported before any modules that might import requests
import grequests # noqa: F401
from prometheus_fastapi_instrumentator import Instrumentator
import uvicorn
from fastapi import FastAPI

from app.config.server_config import ServerConfigurer

# not needed for now
# init_nltk.init_nltk()

# Logs must be configured before initializing the app
configurer = ServerConfigurer()
configurer.configure_logging()

app = FastAPI(lifespan=configurer.lifespan)
configurer.configure_app(app)
Instrumentator().instrument(app).expose(app)

def main():
    """Launched with `poetry run api` only"""
    uvicorn.run("app.main:app", host="0.0.0.0", port=8000, reload=False, workers=1)


if __name__ == "__main__":
    main()
