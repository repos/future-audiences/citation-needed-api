""" Constants for the app """

MIN_INPUT_LENGTH = 2
MAX_INPUT_LENGTH = 500
GOOGLE_SEARCH_API_BASE_URL = "www.googleapis.com/customsearch/v1/siterestrict"

GROQ_MODEL = "llama3-70b-8192"
OPENAI_MODEL = "gpt-4o-2024-08-06"

MAX_SECTION_LENGTH_CHARS = 10000


# From the "lr" parameter of https://developers.google.com/custom-search/v1/reference/rest/v1/cse/list
SUPPORTED_GOOGLE_LANGUAGES = ["ar", "bg", "ca", "cs", "da", "de", "el", "en", "es", "et", "fi", "fr", "hr", "hu", "id", "is", "it", "iw", "ja", "ko", "lt", "lv", "nl", "no", "pl", "pt", "ro", "ru", "sk", "sl", "sr", "sv", "tr", "zh-CN", "zh-TW"]

# Based on: https://www.mediawiki.org/w/api.php?action=sitematrix&smsiteprop=code|lang&smlangprop=code|site&format=json
# Extracted via: https://public-paws.wmcloud.org/User:Isaac_(WMF)/plugin/constants.ipynb
WIKIPEDIA_LANGUAGES = ['ab', 'ace', 'ady', 'af', 'als', 'alt', 'am', 'ami', 'an', 'ang', 'anp', 'ar', 'arc', 'ary', 'arz', 'as', 'ast', 'atj', 'av', 'avk', 'awa', 'ay', 'az', 'azb', 'ba', 'ban', 'bar', 'bat-smg', 'bcl', 'be', 'be-x-old', 'bg', 'bh', 'bi', 'bjn', 'blk', 'bm', 'bn', 'bo', 'bpy', 'br', 'bs', 'bug', 'bxr', 'ca', 'cbk-zam', 'cdo', 'ce', 'ceb', 'ch', 'chr', 'chy', 'ckb', 'co', 'cr', 'crh', 'cs', 'csb', 'cu', 'cv', 'cy', 'da', 'dag', 'de', 'din', 'diq', 'dsb', 'dty', 'dv', 'dz', 'ee', 'el', 'eml', 'en', 'eo', 'es', 'et', 'eu', 'ext', 'fa', 'fat', 'ff', 'fi', 'fiu-vro', 'fj', 'fo', 'fr', 'frp', 'frr', 'fur', 'fy', 'ga', 'gag', 'gan', 'gcr', 'gd', 'gl', 'glk', 'gn', 'gom', 'gor', 'got', 'gu', 'guc', 'gur', 'guw', 'gv', 'ha', 'hak', 'haw', 'he', 'hi', 'hif', 'hr', 'hsb', 'ht', 'hu', 'hy', 'hyw', 'ia', 'id', 'ie', 'ig', 'ik', 'ilo', 'inh', 'io', 'is', 'it', 'iu', 'ja', 'jam', 'jbo', 'jv', 'ka', 'kaa', 'kab', 'kbd', 'kbp', 'kcg', 'kg', 'ki', 'kk', 'kl', 'km', 'kn', 'ko', 'koi', 'krc', 'ks', 'ksh', 'ku', 'kv', 'kw', 'ky', 'la', 'lad', 'lb', 'lbe', 'lez', 'lfn', 'lg', 'li', 'lij', 'lld', 'lmo', 'ln', 'lo', 'lt', 'ltg', 'lv', 'mad', 'mai', 'map-bms', 'mdf', 'mg', 'mhr', 'mi', 'min', 'mk', 'ml', 'mn', 'mni', 'mnw', 'mr', 'mrj', 'ms', 'mt', 'mwl', 'my', 'myv', 'mzn', 'nah', 'nap', 'nds', 'nds-nl', 'ne', 'new', 'nia', 'nl', 'nn', 'no', 'nov', 'nqo', 'nrm', 'nso', 'nv', 'ny', 'oc', 'olo', 'om', 'or', 'os', 'pa', 'pag', 'pam', 'pap', 'pcd', 'pcm', 'pdc', 'pfl', 'pi', 'pih', 'pl', 'pms', 'pnb', 'pnt', 'ps', 'pt', 'pwn', 'qu', 'rm', 'rmy', 'rn', 'ro', 'roa-rup', 'roa-tara', 'ru', 'rue', 'rw', 'sa', 'sah', 'sat', 'sc', 'scn', 'sco', 'sd', 'se', 'sg', 'sh', 'shi', 'shn', 'si', 'simple', 'sk', 'skr', 'sl', 'sm', 'smn', 'sn', 'so', 'sq', 'sr', 'srn', 'ss', 'st', 'stq', 'su', 'sv', 'sw', 'szl', 'szy', 'ta', 'tay', 'tcy', 'te', 'tet', 'tg', 'th', 'ti', 'tk', 'tl', 'tn', 'to', 'tpi', 'tr', 'trv', 'ts', 'tt', 'tum', 'tw', 'ty', 'tyv', 'udm', 'ug', 'uk', 'ur', 'uz', 've', 'vec', 'vep', 'vi', 'vls', 'vo', 'wa', 'war', 'wo', 'wuu', 'xal', 'xh', 'xmf', 'yi', 'yo', 'za', 'zea', 'zh', 'zh-classical', 'zh-min-nan', 'zh-yue', 'zu']

# The number of articles to return from the search service
NUM_ARTICLES = 3

# The number of paragraphs to return per article
NUM_PARAGRAPHS_PER_ARTICLE = 12

# User agent to use for requests
USER_AGENT = 'citation-needed-api'
HTTP_TIMEOUT = 10
WPRV_ARGUMENT = 'cna1'

LEAD_SECTION_PLACEHOLDER = "Lead Section"

# Feature Flag values
IS_ENABLED = "is_enabled"
SHOW_FACT_CHECK_RESULT = "show_fact_check_result"
SHOW_FACT_CHECK_EXPLANATION = "show_fact_check_explanation"
ON_BLOCKLIST = "on_blocklist"
DISCLAIMER_VARIANT_1_FLAG = "disclaimer_variant_1"
DISCLAIMER_VARIANT_2_FLAG = "disclaimer_variant_2"
DISCLAIMER_VARIANT_3_FLAG = "disclaimer_variant_3"
USE_GROQ = "use_groq"
USE_OPENAI = "use_openai"

# Allowed site category values, otherwise replace with "Others". To be kept in sync with the frontend.
ALLOWED_SITE_CATEGORY_VALUES = ["Youtube", "Facebook", "Twitter", "Reddit", "Tiktok", "Quora", "Instagram", "Ycombinator", "LinkedIn", "Github", "Pinterest", "Coursera", "Medium", "Stackoverflow", "news-site"]