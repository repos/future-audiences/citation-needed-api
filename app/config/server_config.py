""" Configures the server """
from contextlib import asynccontextmanager
from datetime import datetime, timedelta
import json
import logging
import sys
import time
from typing import TypedDict

from pydantic import BaseModel
import structlog
from asgi_correlation_id import CorrelationIdMiddleware, correlation_id
from fastapi import FastAPI, Request, Response, status
from fastapi.middleware.cors import CORSMiddleware
from structlog.types import Processor
from uvicorn.protocols.utils import get_path_with_query_string

from app.utils.httpx_client import make_httpx_client
from app.utils.settings import get_settings
from prometheus_client import make_asgi_app
from wme.auth import refresh_token, revoke_token, login

class AuthState(TypedDict):
    refreshing_token: str | None
    access_token: str | None

WME_TOKEN_REFRESH_BUFFER_SEC = 60 * 60 # 1 hour buffer for token refresh


class HTTPInfo(BaseModel):
    url: str
    status_code: int
    method: str
    request_id: str
    version: str

auth_state: AuthState = { "refreshing_token": None, "access_token": None}

async def get_wme_auth() -> str:
    expires_at = auth_state['token_expired_at']
    if expires_at < datetime.now():
        token_response = await refresh_token(get_settings().wme_username, auth_state["refreshing_token"])
        auth_state['access_token'] = token_response.access_token
        auth_state['token_expired_at'] = datetime.now() + timedelta(token_response.expires_in - WME_TOKEN_REFRESH_BUFFER_SEC)
        return token_response.access_token
    else:
        return auth_state['access_token']

class ServerConfigurer:
    """ Stores configurations for the FastAPI server """
    _open_api_schema = None

    def __init__(self):
        self.settings = get_settings()


    @staticmethod
    @asynccontextmanager
    async def lifespan(app: FastAPI):
        settings = get_settings()
        app.requests_client = make_httpx_client()
        creds = await login(settings.wme_username, settings.wme_password.get_secret_value(), app.requests_client)
        expires_in_seconds = creds.expires_in - WME_TOKEN_REFRESH_BUFFER_SEC
        expired_at = datetime.now() + timedelta(seconds=expires_in_seconds - WME_TOKEN_REFRESH_BUFFER_SEC)
        auth_state["refreshing_token"] =  creds.refresh_token
        auth_state["access_token"] = creds.access_token
        auth_state["token_expired_at"] = expired_at
        yield
        await revoke_token(creds.refresh_token)
        await app.requests_client.aclose()

    """ Configures the server """
    def configure_app(self, app: FastAPI) -> None:
        """ Configures the server """
        self.configure_routes(app)
        self.configure_middleware(app)
        self.configure_prometheus(app)

    def configure_prometheus(self, app: FastAPI) -> None:
        app.mount("/metrics", make_asgi_app())

    def configure_logging(self) -> None:
        """ Configures the logging for the server
        Adapted from https://gist.github.com/nymous/f138c7f06062b7c43c060bf03759c29e
        """

        shared_processors: list[Processor] = [
            # Allows adding contextvars to requests - see HTTP middleware for example
            structlog.contextvars.merge_contextvars,

            # Add logger name and log level to the event dict
            structlog.stdlib.add_logger_name,
            structlog.stdlib.add_log_level,

            # Enable %-style formatting.
            structlog.stdlib.PositionalArgumentsFormatter(),

            # Add extra keys to log message
            structlog.stdlib.ExtraAdder(),

            # Add timestamp to log message in ISO 8601 format
            structlog.processors.TimeStamper(fmt="iso"),

            # Add callsite parameters.
            structlog.processors.CallsiteParameterAdder(
                {
                    structlog.processors.CallsiteParameter.FILENAME,
                    structlog.processors.CallsiteParameter.FUNC_NAME,
                    structlog.processors.CallsiteParameter.LINENO,
                }
            ),

            # If the "stack_info" key in the event dict is true,
            # add stack info to log message under 'stack'
            structlog.processors.StackInfoRenderer(),

            # Log bytes to unicode strings
            structlog.processors.UnicodeDecoder(),
        ]

        if self.settings.log_json_format:
            # Format the exception only for JSON logs, as we want to pretty-print them when
            # using the ConsoleRenderer
            shared_processors.append(structlog.processors.format_exc_info)

        structlog.configure(
            processors=shared_processors
            + [
                # Prepare event dict for `ProcessorFormatter`.
                # A bridge between structlog and Python's logging module
                structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
            ],
            logger_factory=structlog.stdlib.LoggerFactory(),
            cache_logger_on_first_use=True,
        )

        def remove_color_message_and_stack_serializer(dic,**kw):
            ignored_keys = ["color_message", "stack", 'filename', 'func_name', 'lineno', 'logger']
            mod = {}
            for k in dic:
                if k not in ignored_keys:
                    mod[k] = dic[k]
            return json.dumps(mod,**kw)

        file_log_renderer = structlog.processors.JSONRenderer(serializer=remove_color_message_and_stack_serializer)
        console_log_renderer = structlog.dev.ConsoleRenderer()

        console_log_formatter = structlog.stdlib.ProcessorFormatter(
            # These run ONLY on `logging` entries that do NOT originate within structlog.
            foreign_pre_chain=shared_processors,
            # These run on ALL entries after the pre_chain is done.
            processors=[
                    # Remove _record & _from_structlog.
                    structlog.stdlib.ProcessorFormatter.remove_processors_meta,
                    console_log_renderer,
                ],
        )
        file_log_formatter = structlog.stdlib.ProcessorFormatter(
            # These run ONLY on `logging` entries that do NOT originate within structlog.
            foreign_pre_chain=shared_processors,
            # These run on ALL entries after the pre_chain is done.
            processors=[
                    # Remove _record & _from_structlog.
                    structlog.stdlib.ProcessorFormatter.remove_processors_meta,
                    file_log_renderer,
                ],
        )

        console_handler = logging.StreamHandler()
        # Use OUR `ProcessorFormatter` to format all `logging` entries.
        console_handler.setFormatter(console_log_formatter)
        root_logger = logging.getLogger()
        root_logger.addHandler(console_handler)

        # Create the log_path to the logs folder in the tool_data_dir
        log_path = self.settings.tool_data_dir + "/logs"


        file_handler = logging.FileHandler("{0}/{1}.log".format(log_path, 'default'))
        file_handler.setFormatter(file_log_formatter)
        root_logger.addHandler(file_handler)

        root_logger.setLevel(self.settings.log_level.upper())

        for _log in ["uvicorn", "uvicorn.error"]:
            # Clear the log handlers for uvicorn loggers, and enable propagation
            # so the messages are caught by our root logger and formatted correctly
            # by structlog
            logging.getLogger(_log).handlers.clear()
            logging.getLogger(_log).propagate = True
        # Since we re-create the access logs ourselves, to add all information
        # in the structured log (see the `logging_middleware` in main.py), we clear
        # the handlers and prevent the logs to propagate to a logger higher up in the
        # hierarchy (effectively rendering them silent).
        logging.getLogger("uvicorn.access").handlers.clear()
        logging.getLogger("uvicorn.access").propagate = False

        def handle_exception(exc_type, exc_value, exc_traceback):
            """
            Log any uncaught exception instead of letting it be printed by Python
            (but leave KeyboardInterrupt untouched to allow users to Ctrl+C to stop)
            See https://stackoverflow.com/a/16993115/3641865
            """
            if issubclass(exc_type, KeyboardInterrupt):
                sys.__excepthook__(exc_type, exc_value, exc_traceback)
                return

            root_logger.error(
                "Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback)
            )
        sys.excepthook = handle_exception

    async def logging_middleware(self, request: Request, call_next) -> Response:
        structlog.contextvars.clear_contextvars()
        # These context vars will be added to all log entries emitted during the request
        request_id = correlation_id.get()
        structlog.contextvars.bind_contextvars(request_id=request_id)

        start_time = time.perf_counter_ns()
        # If the call_next raises an error, we still want to return our own 500 response,
        # so we can add headers to it (process time, request ID...)
        response = Response(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            response = await call_next(request)
        except Exception:
            structlog.stdlib.get_logger("api.error").exception("Uncaught exception")
            raise
        finally:
            process_time = time.perf_counter_ns() - start_time
            status_code = response.status_code
            url = get_path_with_query_string(request.scope)
            http_method = request.method
            http_version = request.scope["http_version"]

            # Disable logging simple OKs to our healthcheck to avoid polluting the logs
            if not (http_method == "GET" and url == "/" and status_code == status.HTTP_200_OK):
                # Recreate the Uvicorn access log format, but add all parameters as structured information
                access_logger = structlog.stdlib.get_logger("api.access")
                http_info = HTTPInfo(url=str(request.url),
                                     method=http_method,
                                     status_code=status_code,
                                     request_id=request_id or "",
                                     version=http_version)
                access_logger.info(
                    event=f""""{http_method} {url} HTTP/{http_version}" {status_code}""",
                    http=http_info.dict(),
                    duration=process_time,
                )
                response.headers["X-Process-Time"] = str(process_time / 10 ** 9)

            return response

    def configure_middleware(self, app: FastAPI) -> None:
        """ Configures the middleware for the server """
        app.add_middleware(
            CORSMiddleware,
            # allow_origins=["https://chat.openai.com"],
            # allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
            expose_headers=['X-Request-ID']
        )
        app.middleware("http")(self.logging_middleware)

        # Notably this needs to be last in order to log the request ID correctly
        app.add_middleware(CorrelationIdMiddleware)

    def configure_routes(self, app: FastAPI) -> None:
        from app.routes import find_citation, health_check, performance_tests, add_a_fact, add_a_fact_per_article

        app.include_router(health_check.router)
        app.include_router(find_citation.router)
        app.include_router(performance_tests.router)
        app.include_router(add_a_fact.router)
        app.include_router(add_a_fact_per_article.router)

        # app.mount("/static", StaticFiles(directory=get_package_path("static")), name="static")
