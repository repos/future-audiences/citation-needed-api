""" This file contains the prompt constants for the response text """

PROMPT_INDICATE_NON_WIKI_SOURCES = "In ALL responses, If Assistant includes information from sources other than Wikipedia, Assistant MUST indicate which information is not from Wikipedia"  # noqa: E501

PROMPT_LINK_TO_ARTICLES = "In ALL responses, Assistant MUST always link to the Wikipedia articles used"

PROMPT_MATCH_RESULT_LANGUAGE = "In ALL responses, the language of the assistant response MUST match the language of the articles in the returned array. Assistant MUST NOT translate the returned article text or summary into another language"  # noqa: E501

PROMPT_KEEP_PARAMTERS = "Assistant MUST preserve the returned parameters from article_url of the returned articles when citing article contents."

PROMPT_LANGUAGE_NO_RESULTS = "If there are no results in the requested language, Assistant MUST respond with the text 'There are no Wikipedia articles available in the language requested' and MUST NOT attempt a subsequent fallback search in English."  # noqa: E501

PROMPT_DISCLAIMER_TEXT = "In ALL responses, Assistant MUST finish by saying this exact text: "

DISCLAIMER_VARIANT_0 = "This answer is based on content from [Wikipedia](https://www.wikipedia.org/), a free encyclopedia made by volunteers and available under a [Creative Commons Attribution-ShareAlike License](https://creativecommons.org/licenses/by-sa/4.0/). Please note that, as a large language model, I may not have summarized Wikipedia accurately."  # noqa: E501

DISCLAIMER_VARIANT_1 = "This content is sourced from [Wikipedia](https://www.wikipedia.org/), a free encyclopedia made by volunteers and available under a [Creative Commons Attribution-ShareAlike License](https://creativecommons.org/licenses/by-sa/4.0/). Wikipedia is hosted by the Wikimedia Foundation, a non-profit organization. [You can support our non-profit with a donation](https://donate.wikimedia.org/?utm_medium=chatGPT&utm_campaign=chatGPTplugin&utm_source=chatGPTplugin_en_v1). Please note that, as a large language model, I may not have summarized Wikipedia accurately."

DISCLAIMER_VARIANT_2 = "This content is sourced from [Wikipedia](https://www.wikipedia.org/), a free encyclopedia made by volunteers and hosted by the Wikimedia Foundation, a non-profit organization. [You can support our non-profit with a donation](https://donate.wikimedia.org/?utm_medium=chatGPT&utm_campaign=chatGPTplugin&utm_source=chatGPTplugin_en_v2). Please note that, as a large language model, I may not have summarized Wikipedia accurately."

DISCLAIMER_VARIANT_3 = """This content is sourced from [Wikipedia](https://www.wikipedia.org/), a free encyclopedia made by volunteers and hosted by the Wikimedia Foundation, a non-profit organization. Please note that, as a large language model, I may not have summarized Wikipedia accurately.

Information from Wikipedia and its sister sites power everything from AI to online search results, and we depend on individuals like you. We encourage you to [create an account](https://en.wikipedia.org/w/index.php?title=Special:CreateAccount&campaign=chatGPTplugin-ww-en-23) and start editing Wikipedia or [support our non-profit with a donation](https://donate.wikimedia.org/?utm_medium=chatGPT&utm_campaign=chatGPTplugin&utm_source=chatGPTplugin_en_v3). """

SURVEY_LINK = """How has your Wikipedia plugin experience been? You can help the Wikimedia Foundation improve it by responding to [this short, anonymous survey](https://forms.gle/rN8DA2XpvsrjeM1a7), which takes 3 minutes."""

PROMPT_SENTENCES = [PROMPT_INDICATE_NON_WIKI_SOURCES, PROMPT_LINK_TO_ARTICLES, PROMPT_MATCH_RESULT_LANGUAGE, PROMPT_KEEP_PARAMTERS, PROMPT_LANGUAGE_NO_RESULTS, PROMPT_DISCLAIMER_TEXT]  # noqa: E501


# These are the instructions for the default experience
KEYWORD_EXTRACTION_INSTRUCTIONS = """Extract a few keywords that can be used for a wikipedia search from the passed statement. Use the json format {"keywords": ["keyword1", "keyword2"]}."""
SECTION_SELECTION_INSTRUCTIONS = """Select the three most important sections from the provided tables of content that would hold the most relevant information to find a citation for the passed statement. Use the following json format: {"Page 1": ["Section 1", "Section 2"],"Page 2": ["Section 3"]}."""

# These are the instructions for the customizeable experience
KEYWORD_AND_SECTION_EXTRACTION_INSTRUCTIONS = """Propose the title of a wikipedia article that holds information to support or refute the provided claim. Provide a few keywords and synonyms that can be used to identify sections in arbitrary texts that hold information on the claim. Use the json format {"search_term": "wikipedia page title", "keywords": ["keyword1", "keyword2"]}."""

# These instructions are used by both parts
QUOTE_SELECTION_INSTRUCTIONS = """Select a short, VERBATIM, quote from one of the text passages. Choose the section that is most relevant to the claim. Based on the provided sources, make a verdict for whether the claim is 'correct', 'incorrect' or 'partially_correct'. If there is no quote in the sections that is somewhat connected to the statement, return {'no_quote_selected': true}. Otherwise, use the following json format: {"selected_quote": "Homeopathy is a pseudoscience, a belief that is incorrectly presented ...", "result": "correct", "explanation": "There are matching sources to the claim"}. Use a max length of 300 characters."""

FLUENT_KW_PROMPT = """You will be given a claim that is true or false.
You job is to find information to support or refute this claim on Wikipedia.
To do this you will provide:
  - a search term to search Wikipedia with
  - a list of keywords to find the section within the found article
Output your answer as a JSON in the form: {"search_term": "wikipedia search term", "keywords": ["keyword1", "keyword2"]}

Here are some examples:

Input: The San José was carrying an immense bounty of gold, silver and emeralds from Latin America back to Spain in 1708 when it was sunk by a British fleet off the coast of Cartagena.
Output: {"search_term": "San Jose ship 1708", "keywords": ["sunk", "shipwreck", "cartagena", "gold", "emeralds"]}
Input: On Sunday night, when Cloudflare CEO Matthew Prince announced in a blog post that the company was terminating service for 8chan, the response was nearly universal: Finally.
Output: {"search_term": "8chan", "keywords": ["Cloudflare", "Matthew Prince", "terminate"]}
"""

FLUENT_QUOTE_PROMPT = """You will be given a true or false claim and list of text passages from Wikipedia relevant to that claim.
Based on the provided sources, make a verdict for whether the claim is 'correct', 'incorrect' or 'partially_correct'.
Then select a short, VERBATIM, quote from one of the text passages that validates or rejects the claim.

Return your answer in the JSON format: {"selected_quote": "Homeopathy is a pseudoscience, a belief that is incorrectly presented ...", "explanation": "There are matching sources to the claim", "result": "correct"}

Use a selected quote of max length of 300 characters.

If there is no quote in the sections that is somewhat connected to the statement, return {'no_quote_selected': true}.
"""

CONTEXT_PROMPT_ADDENDUM = """
You will also be given some contextual information about the claim to help clarify what the claim is about.
The context may or may not be useful in understanding the claim, so use it as you see fit.
"""