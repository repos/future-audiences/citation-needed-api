import math
from pydantic import BaseModel, Field
from wme.on_demand import EnterpriseAPIResponse

from app.constants import MAX_SECTION_LENGTH_CHARS
from app.services.keyword_extractor import KeywordExtractionResult


class RankedSection(BaseModel):
    title: str
    response: EnterpriseAPIResponse = Field(exclude=True)
    section_title: str
    section_text: str = Field(exclude=True)
    score: float

def heuristic_score(kw: KeywordExtractionResult, section_text: str, article_title: str):
    keywords = kw.keywords + (kw.search_term or "").split()
    keywords = [k.lower() for k in keywords]
    section_text = section_text.lower()
    score = 0
    for kw in keywords:
        if kw in article_title.lower():
            score += 4
        elif kw in section_text:

            score += 2
        else:
            partials = kw.split()
            for partial_kw in partials:
                if partial_kw in section_text or partial_kw in article_title:
                    score += 1 / len(partials)
    return score / math.log(max(len(section_text), 2))

def take_until_characters(sections: list[RankedSection], limit: int, max_chars: int) -> list[RankedSection]:
    selected_sections = []
    char_count = 0
    for section in sections:
        if not section.section_text:
            continue
        char_count += len(section.section_text)
        selected_sections.append(section)
        if len(selected_sections) >= limit:
            break
        if char_count > max_chars:
            break
    return selected_sections

class HeuristicSectionRanker:
    def __init__(self, scoring_function):
        self._scorer = scoring_function

    def rank(self, kw: KeywordExtractionResult, page_sections: dict[str, EnterpriseAPIResponse], limit: int = 3) -> list[RankedSection]:
        sections: list[RankedSection] = []
        for title, page in page_sections.items():
            for section_name, section_text in page.iter_sections():
                score = self._scorer(kw, section_text, section_name)
                rs = RankedSection(title=title, response=page, section_title=section_name, section_text=section_text, score=score)
                sections.append(rs)
        sections.sort(key=lambda x: x.score, reverse=True)

        return take_until_characters(sections, limit, MAX_SECTION_LENGTH_CHARS)
