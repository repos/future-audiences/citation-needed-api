import httpx
from structlog import getLogger
from app.services.keyword_extractor import KeywordExtractionResult
from pydantic import BaseModel

from app.utils.settings_types import SystemEnum
from abc import ABC, abstractmethod
import asyncio

logger = getLogger(__name__)

class WikiResult(BaseModel):
    ns: int | None = None
    title: str
    pageid: int | None = None
    size: int | None = None
    wordcount: int | None = None
    snippet: str
    timestamp: str | None = None

    def clean_title(self):
        return self.title.replace('_', ' ').replace("%20", " ")

class ArticleFinder(ABC):
    @abstractmethod
    async def find_articles(self, keyword_result: KeywordExtractionResult, dst: SystemEnum = SystemEnum.CitationNeededAPI) -> list[WikiResult]:
        pass


async def search_wiki(client: httpx.AsyncClient, query: str, limit: int = 3, lang: str = "en") -> list[WikiResult]:
    if not query:
        return []
    url = f'https://{lang}.wikipedia.org/w/api.php'
    params = {
        'action': 'query',
        'format': 'json',
        'list': 'search',
        'srsearch': query,
        'srlimit': limit,
        'srnamespace': 0
    }
    response = await client.get(url, params=params)
    response.raise_for_status()
    r = response.json()
    if 'error' in r:
        raise ValueError(f"Error while search for '{query}'", r['error'])
    return [WikiResult(**result) for result in r['query']['search']]


class BasicArticleFinder(ArticleFinder):
    def __init__(self, client: httpx.AsyncClient, limit: int = 3):
        self._client = client
        self._limit = limit

    async def find_articles(self, keyword_result: KeywordExtractionResult, dst: SystemEnum = SystemEnum.CitationNeededAPI) -> list[WikiResult]:
        try:
            res = await search_wiki(self._client, keyword_result.search_term, limit=self._limit)
        except Exception as e:
            logger.error("Error while searching Wiki",
                source=SystemEnum.Wikipedia,
                destination=dst,
                error=e)
            return []

        logger.info("Results received from Wiki search",
            source=SystemEnum.Wikipedia,
            destination=dst,
            info=[r.model_dump() for r in res])
        return res
    

class MultiArticleFinder(ArticleFinder):
    def __init__(self, finders: list[ArticleFinder]):
        self._finders = finders

    async def find_articles(self, keyword_result: KeywordExtractionResult, dst: SystemEnum = SystemEnum.CitationNeededAPI) -> list[WikiResult]:
        all_results_futures = []
        for finder in self._finders:
            all_results_futures.append(finder.find_articles(keyword_result, dst=dst))
        all_results = await asyncio.gather(*all_results_futures)
        # flatten the list
        all_results = [res for sublist in all_results for res in sublist]
        # remove duplicates (should preserve order of first appearance)
        distinct_results = list({res.clean_title(): res for res in all_results}.values())
        return distinct_results