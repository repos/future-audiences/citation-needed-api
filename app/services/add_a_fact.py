
import asyncio
from pydantic import BaseModel
from app.services.article_picker_service import ArticlePicker
from app.services.find_articles import ArticleFinder
from app.services.heuristic_section_ranker import HeuristicSectionRanker, RankedSection, heuristic_score
from app.services.interfaces import FeatureFlaggingService
from structlog import getLogger
from typing import Any, Self
from app.services.keyword_extractor import KeywordExtractor
from app.services.quote_extractor import QuoteExtractionResult, QuoteExtractor
from app.services.quote_matcher import QuoteMatch
from app.services.summary_service import FactSummarizer, FactSummarizerResult
from app.services.wme_service import WmeService
from app.utils.langchain_helper import ResultWithTokens
from app.utils.settings_types import  SystemEnum
from app.models.parameters import FactCheckerRequestInfo
from wme.on_demand import EnterpriseAPIResponse
import json
import sys
import traceback

logger = getLogger(__name__)

class ArticleDetailsResult(BaseModel):
    title: str
    image_url: str | None
    short_description: str
    fact_contained: bool

    @classmethod
    def from_wme_response(cls, resp: EnterpriseAPIResponse) -> Self:
        if resp.image:
            image_url = resp.image.content_url
        else:
            image_url = None
        return cls(title=resp.name, image_url=image_url, short_description=resp.description, fact_contained=False)

class AddAFactResponse(BaseModel):
    result: str
    msg: str | None = None
    match: QuoteMatch | None = None
    fact_summary: str | None = None

class AddAFactMultiResponse(BaseModel):
    matches: dict[str, AddAFactResponse]



class AddAFactService:
    """Fetches articles from Wikipedia and interacts with ChatGPT to find citations for a given query."""
    def __init__(self,
                 kw_extractor: KeywordExtractor,
                 summarizer_service: FactSummarizer,
                 article_picker: ArticlePicker,
                 wme_service: WmeService,
                 search_service: ArticleFinder,
                 feature_flagging_service: FeatureFlaggingService,
                 quote_service: QuoteExtractor,
                 top_n_sections: int = 3):
        self.kw_extractor = kw_extractor
        self.article_picker = article_picker
        self.quote_service = quote_service
        self.wme_service = wme_service
        self.search_service = search_service
        self.feature_flagging_service = feature_flagging_service
        self.summarizer = summarizer_service
        self.top_n_sections = top_n_sections

    async def stream_add_a_fact(self, info: FactCheckerRequestInfo) -> Any:
        try:
           async for msg in self._stream_add_a_fact(info):
               yield msg
        except Exception:
            exc_info = sys.exc_info()
            ex = traceback.format_exception(*exc_info)
            yield json.dumps({"error": ex})
            logger.error("Error in add_a_fact", exc_info=ex)
        yield json.dumps({"current_step": "Done"}) + '\n'
        logger.info("User request completed")
    async def _stream_add_a_fact(self, info: FactCheckerRequestInfo) -> Any:
        logger.info("User request received",
                    source=SystemEnum.User,
                    destination=SystemEnum.AddAFactAPI,
                    info=info.model_dump())
        
        yield json.dumps({"current_step": "Checking out the statement", "statement": info.selection}) + '\n'
        
        keyword_result = await self.kw_extractor.extract(info.selection, context=info.context, src=SystemEnum.AddAFactAPI)

        yield json.dumps({"current_step": "Searching Wikipedia", "search_term": keyword_result.result.json()}) + '\n'

        articles = await self.search_service.find_articles(keyword_result.result, dst=SystemEnum.AddAFactAPI)

        if not articles:
            result_json = AddAFactResponse(result="no_articles", msg="No matching Wikipedia pages found.", article_titles=[])
            logger.info("No articles found response",
                    source=SystemEnum.AddAFactAPI,
                    destination=SystemEnum.User,
                    info=result_json)
            yield json.dumps(result_json.model_dump()) + "\n"
            return

        yield json.dumps({"current_step": "Looking up on Wikipedia"}) + '\n'

        claim_summary = asyncio.ensure_future(self.summarizer.extract(info.selection, info.context, SystemEnum.AddAFactAPI))

        article_contents = await self.wme_service.fetch_articles([a.title for a in articles], search_language="en")

        yield json.dumps({"current_step": "Picking the most relevant articles", "candidates": list(article_contents.keys())}) + '\n'

        picked_articles_res = await self.article_picker.extract(info.selection, list(article_contents.values()), context=info.context, src=SystemEnum.AddAFactAPI)
        picked_articles = picked_articles_res.result.articles[0:3]
        articles = [article_contents.get(art) for art in picked_articles if art in article_contents]
        results = [ArticleDetailsResult.from_wme_response(art) for art in articles]
        if not results:
            resp = AddAFactResponse(result="Found nothing", articles=[]).model_dump()
            logger.info("No articles found response",
                        source=SystemEnum.AddAFactAPI,
                        destination=SystemEnum.User,
                        info=resp)
            yield json.dumps(resp) + "\n"
            return

        yield json.dumps(dict(current_step="Found some relevant articles", articles=[r.model_dump() for r in results])) + "\n"

        # now check if the articles contain the fact
        section_ranker = HeuristicSectionRanker(heuristic_score)
        ranked_sections = section_ranker.rank(keyword_result.result, {title: article for title, article in article_contents.items() if title in picked_articles}, limit=10)
        yield json.dumps({"current_step": "Checking these sections", "sections" : [(rs.title, rs.section_title) for rs in ranked_sections]}) + "\n"

        per_article_quotes_fut = []

        async def extract_and_match_quotes(selection: str, context: str, article: str, sections: list[RankedSection], src: SystemEnum) -> tuple[str, QuoteExtractionResult, QuoteMatch | None]:
            quote_result = await self.quote_service.extract(selection, context, sections, src=src)
            
            quote_match = None
            return (article, quote_result.result, quote_match)
            

        for article, contents in article_contents.items():
            if article not in picked_articles:
                continue
            per_article_sections = section_ranker.rank(keyword_result.result, {article: contents}, limit=50)
            per_article_quote = asyncio.ensure_future(extract_and_match_quotes(info.selection, info.context, article, per_article_sections, src=SystemEnum.AddAFactAPI))
            per_article_quotes_fut.append(per_article_quote)
        
        per_article_quotes: list[tuple[str, QuoteExtractionResult, QuoteMatch | None]] = await asyncio.gather(*per_article_quotes_fut)

        summary: ResultWithTokens[FactSummarizerResult] = await claim_summary
        yield json.dumps(dict(current_step="Summarized claim", claim_summary=summary.result.summary)) + "\n"

        output: dict[str, AddAFactResponse] = dict()
        for page, res, quote_match in per_article_quotes:
            output[page] = AddAFactResponse(result=res.result or "not supported", msg=res.explanation or 'Fact not found in Wikipedia article', match=quote_match)

        yield json.dumps(AddAFactMultiResponse(matches=output).model_dump()) + "\n"
