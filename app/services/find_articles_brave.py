

import httpx
from pydantic import SecretStr
from structlog import getLogger
from app.services.find_articles import ArticleFinder, WikiResult
from app.services.keyword_extractor import KeywordExtractionResult
from app.utils.settings_types import SystemEnum
from urllib.parse import urlparse

BRAVE_API_URL = "https://api.search.brave.com/res/v1/web/search"
logger = getLogger(__name__)

BANNED_PREFIXES = ["Talk:", "Portal:", "Category:", "File:", "Template:", 'Wikipedia:']

def get_title_from_url(url: str) -> str | None:
    url_parse = urlparse(url)
    if url_parse.netloc == "en.wikipedia.org":
        split_url = url_parse.path.split("/", 2)
        if len(split_url) == 3:
            title = split_url[2]
            for prefix in BANNED_PREFIXES:
                if title.startswith(prefix):
                    return None
            return title.replace("_", " ").replace("%20", " ")

class BraveArticleFinder(ArticleFinder):
    def __init__(self, client: httpx.AsyncClient, brave_api_key: SecretStr, limit: int = 3):
        self._client = client
        self._api_key = brave_api_key
        self._limit = limit

    async def find_articles(self, keyword_result: KeywordExtractionResult, dst: SystemEnum = SystemEnum.AddAFactAPI) -> list[WikiResult]:
        headers = {
            "X-Subscription-Token": self._api_key.get_secret_value(),
            "Accept": "application/json",
            "Accept-Encoding": "gzip"
        }

        if not keyword_result.search_term:
            return []

        args = {
            "q": keyword_result.search_term + " site:en.wikipedia.org",
            "count": self._limit,
            "safesearch": "off"
        }

        try:
            response = await self._client.get(BRAVE_API_URL, headers=headers, params=args)
            response.raise_for_status()
            r = response.json()
            results = r.get('web', {}).get('results', [])
            wiki_results = [result for result in results if "en.wikipedia.org" in result.get('url')]

            wiki_titles = [(get_title_from_url(res['url']), res['description']) for res in wiki_results]

            brave_results = [WikiResult(title=title, snippet=desc) for (title, desc) in wiki_titles if title]

            logger.info("Results received from Brave search",
                source=SystemEnum.Brave,
                destination=dst,
                info=[r.model_dump() for r in brave_results])
            
            return brave_results
        except Exception as e:
            logger.error("Error while searching Brave",
                source=SystemEnum.Brave,
                destination=dst,
                error=e)
            return []