from typing import List

from langchain_core.pydantic_v1 import BaseModel as LangChainModel
from langchain_core.language_models.chat_models import BaseChatModel
from langchain_core.messages import SystemMessage, HumanMessage
from structlog import getLogger

from app.utils.langchain_helper import RawStructuredOutput, ResultWithTokens
from app.utils.settings_types import SystemEnum

logger = getLogger(__name__)

SYSTEM = """
You will be given a claim that is true or false.
You may also be provided some contextual information to help you understand but you are only making keywords for the given claim.
You job is to find information to support or refute this claim on Wikipedia.
To do this you will provide:
  - a search term to search Wikipedia with
  - a list of keywords to find the section within the found article
Output your answer as a JSON in the form: {"search_term": "wikipedia search term", "keywords": ["keyword1", "keyword2"]}

Examples:
  Claim: Rishi Sunak announces UK general election for Thursday 4 July 2024
  Output: {"search_term": "UK general election July", "keywords": ["Rishi Sunak", "Thursday 4 July"]}

  Claim: Former Republican presidential candidate Nikki Haley has said she plans to vote for Donald Trump, her former opponent and boss, in the 2024 US presidential election.
  Output: {"search_term": "Nikki Haley Donald Trump 2024", "keywords": ["Republican presidential candidate", "vote", "2024 US presidential election"]}

  Claim: In 1985, the number of grizzly bears in the Yellowstone region was as low as 200. By 2010, this number had risen to 600.
  Output: {"search_term": "Grizzly Bear Yellowstone", "keywords": ["1985", "2010", "number of bears"]}

  Claim: The San José was carrying an immense bounty of gold, silver and emeralds from Latin America back to Spain in 1708 when it was sunk by a British fleet off the coast of Cartagena.
  Output: {"search_term": "San Jose ship 1708", "keywords": ["sunk", "shipwreck", "cartagena", "gold", "emeralds"]}

  Claim: On Sunday night, when Cloudflare CEO Matthew Prince announced in a blog post that the company was terminating service for 8chan, the response was nearly universal: Finally.
  Output: {"search_term": "8chan", "keywords": ["Cloudflare", "Matthew Prince", "terminate"]}

  Claim: They can carry diseases and parasites that can leave behind in your home through their feces.
  Context: Interesting Cricket Facts
  Output: {"search_term": "cricket disease", "keywords": ["parasite", "disease", "feces"]}

"""

class KeywordExtractionResult(LangChainModel):
    """ This class represents a keyword extraction result """
    keywords: List[str] | None
    search_term: str | None


class KeywordExtractor:
    def __init__(self, model: BaseChatModel):
        self._base_model = model
        self._model = model.with_structured_output(KeywordExtractionResult, method="json_mode", include_raw=True)

    async def extract(self, query: str, context: str | None = None, src: SystemEnum = SystemEnum.CitationNeededAPI) -> ResultWithTokens[KeywordExtractionResult]:
        messages = [
            SystemMessage(content=SYSTEM),
            HumanMessage(content=f"Claim: {query}"),
        ]

        if context:
            messages.append(HumanMessage(content=f"Page context: {context}"))

        model_type = self._base_model._llm_type

        logger.info("Starting keyword extraction",
            source=src,
            destination=SystemEnum.ChatGPT,
            model_type=model_type,
            query=query, context=context)
        res = await self._model.ainvoke(messages)
        res = ResultWithTokens.from_result(RawStructuredOutput.parse_obj(res))
        logger.info("Finished keyword extraction",
            source=SystemEnum.ChatGPT,
            destination=src,
            model_type=model_type,
            info=res.dict())
        return res