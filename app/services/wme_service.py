import asyncio
import httpx
from structlog import getLogger
from wme.on_demand import OnDemand, Filter, EnterpriseAPIResponse

from app.utils.settings_types import SystemEnum

logger = getLogger(__name__)


class WmeService:
    def __init__(self, on_demand: OnDemand):
        self.on_demand = on_demand

    async def _fetch(self, title: str, search_language: str = "en", src: SystemEnum = SystemEnum.CitationNeededAPI) -> EnterpriseAPIResponse:
        filters = [Filter.for_site(f"{search_language}wiki")]
        try:
            response = await self.on_demand.lookup_structured(title, limit = 1, filters=filters)
            return response[0]
        except httpx.HTTPStatusError as e:
            logger.warn("HTTP Error when talking to WME",
                        source=src,
                        destination=SystemEnum.Wikipedia,
                        info={"title": title, "status_code": e.response.status_code})
            return None
            
    
    async def fetch_articles(self, titles: list[str], search_language: str = "en", src: SystemEnum = SystemEnum.CitationNeededAPI) -> dict[str, EnterpriseAPIResponse]:
        responses = [self._fetch(title, search_language, src) for title in titles]
        response_values = await asyncio.gather(*responses, return_exceptions=True)
        for resp in response_values:
            if isinstance(resp, Exception):
                logger.warn("Error when talking to WME",
                            source=src,
                            destination=SystemEnum.Wikipedia,
                            info={"error": str(resp)})

        return {title: resp for (title, resp) in zip(titles, response_values) if resp is not None and not isinstance(resp, Exception)}