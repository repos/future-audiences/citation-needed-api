
from langchain_core.pydantic_v1 import BaseModel as LangChainModel
from langchain_core.language_models.chat_models import BaseChatModel
from langchain_core.messages import SystemMessage, HumanMessage
from structlog import getLogger

from app.services.heuristic_section_ranker import RankedSection
from app.utils.langchain_helper import RawStructuredOutput, ResultWithTokens
from app.utils.settings_types import SystemEnum
from datetime import date

logger = getLogger(__name__)

SYSTEM = """
Today is %s.

You will be given a possibly true claim and list of text passages from Wikipedia relevant to that claim.
Based on the provided sources, make a verdict for whether the claim is 'supported', 'partially supported' or 'unsupported'.
Then select a short, VERBATIM, quote from one of the text passages that validates or rejects the claim. Include all original text including parentheses, brackets, etc.

Return your answer in the JSON format: {"selected_quote": "Homeopathy is a pseudoscience, a belief that is incorrectly presented ...", "explanation": "There are matching sources to the claim", "result": "supported"}

Use a selected quote of max length of 300 characters.

Do not use words such as "true", "false", "accurate" or "inaccurate" in your response. Always refer to the passages given as "this Wikipedia article" or just "Wikipedia".

If there is no quote in the sections that either directly proves or refutes the statement then set "selected_quote" to null.

Example 1:

Input:

Wikipedia Passages: 

Ultrafine particle # Exposure, risk, and health effects: The main exposure to UFPs is through inhalation. Owing to their size, UFPs are considered to be respirable particles. Contrary to the behaviour of inhaled PM10 and PM2.5, ultrafine particles are deposited in the lungs, where they have the ability to penetrate tissue and undergo interstitialization, or to be absorbed directly into the bloodstream—and therefore are not easily removed from the body and may have immediate effect.[2] Exposure to UFPs, even if components are not very toxic, may cause oxidative stress,[14] inflammatory mediator release, and could induce heart disease, lung disease, and other systemic effects. The exact mechanism through which UFP exposure leads to health effects remains to be elucidated

Ultrafine particle # Regulation and legislation: As the nanotechnology industry has grown, nanoparticles have brought UFPs more public and regulatory attention.[27] UFP risk assessment research is still in the very early stages. There are continuing debates about whether to regulate UFPs and how to research and manage the health risks they may pose

Claim: Ultrafine particles, UFPs, the smallest contributors to air pollution, hinder the function of mitochondria in human olfactory mucosa cells, a new study shows


Output:

{"selected_quote": null, "explanation": "Wikipedia notes the general health effects of UFPs but does not mention mitochondria", "result": "unsupported"}

Example 2:

Input:

Wikipedia Passages: 

Tmesipteris truncata # Abstract: Tmesipteris truncata (aka Tmesipteris oblanceolata) is a fern ally endemic to eastern Australia. It is commonly called a Fork Fern. The habitat of this primitive plant is under waterfalls, or in sandstone gullies or rainforests. It is often found growing on the base of the King Fern. Usually seen as an epiphyte or lithophyte, but it may also appear as a terrestrial plant. It is found as far south as Mount Dromedary.

Tmesipteris truncata # Genetics: On 31 May 2024, Tmesipteris oblanceolata was reported to have been found to contain the largest known eukaryotic genome, with 160 billion base pairs, by comparison more than 50 times larger than the human genome.

Claim: Tmesipteris truncata's full set of genetic instructions is over 50 times the size of the human genome. And it’s about 7 percent larger than the genome of the previous record holder, a Japanese flower dubbed Paris japonica.

Output:

{"selected_quote": "On 31 May 2024, Tmesipteris oblanceolata was reported to have been found to contain the largest known eukaryotic genome, with 160 billion base pairs, by comparison more than 50 times larger than the human genome.", "explanation": "This Wikipedia article explicitly states that Tmesipteris oblanceolata's genome is more than 50 times larger than the human genome.", "result": "supported"}

Example 3:

Input:

Wikipedia Passages:

Strangles # Abstract: Strangles (equine distemper) is a contagious upper respiratory tract infection of horses and other equines caused by a Gram-positive bacterium, Streptococcus equi. As a result, the lymph nodes swell, compressing the pharynx, larynx, and trachea, and can cause airway obstruction leading to death, hence the name strangles. Strangles is enzootic in domesticated horses worldwide. The contagious nature of the infection has at times led to limitations on sporting events

Strangles # Cause: The disease is spread by an infected horse when nasal discharge or pus from the draining lymph nodes contaminate pastures, feed troughs, brushes, bedding, tack, etc.

Claim: Strangles has been vexing horse owners since at least 1256. Caused by the bacterium Streptococcus equi, the common name “strangles” refers to the upper respiratory noise that is one of the disease’s more remarkable clinical signs

Output:

{"selected_quote": "Strangles (equine distemper) is a contagious upper respiratory tract infection of horses and other equines caused by a Gram-positive bacterium, Streptococcus equi", "explanation": "This Wikipedia article confirms the bacterial cause but does not mention 1256", "result": "partially supported"}

"""



class QuoteExtractionResult(LangChainModel):
    """ This class represents a keyword extraction result """
    selected_quote: str | None
    explanation: str | None
    result: str | None
    no_quote_selected: str | None


class QuoteExtractor:
    def __init__(self, model: BaseChatModel):
        self._base_model = model
        self._model = model.with_structured_output(QuoteExtractionResult, method="json_mode", include_raw=True)

    async def extract(self, claim: str, context: str, sections: list[RankedSection], src: SystemEnum = SystemEnum.CitationNeededAPI) -> ResultWithTokens[QuoteExtractionResult]:
        passages = "\n\n".join([f"{section.title} # {section.section_title}: {section.section_text}" for section in sections])
        model_type = self._base_model._llm_type

        if len(passages) > 12000:
            passages = passages[:12000]

        logger.info("Starting quote selection",
            source=src,
            destination=SystemEnum.ChatGPT,
            passage_length=len(passages),
            model_type=model_type,
            claim=claim)

        messages = [
            SystemMessage(content=SYSTEM % (date.today(),)),
            HumanMessage(content=f"Wikipedia Passages: {passages}"),
            HumanMessage(content=f"Claim: {claim}"),
        ]

        res = await self._model.ainvoke(messages)
        res = ResultWithTokens.from_result(RawStructuredOutput.parse_obj(res))

        logger.info("Received selected quote",
            source=SystemEnum.ChatGPT,
            destination=src,
            model_type=model_type,
            info=res.dict())

        return res
