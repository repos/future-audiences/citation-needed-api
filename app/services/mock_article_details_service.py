""" XTools service implementation for detail lookups on Wikipedia articles"""

from app.models.parameters import ArticleDetailsResult
import structlog

from app.services.interfaces import ArticleDetailsService

logger = structlog.getLogger(__name__)


class MockArticleDetailsService(ArticleDetailsService):

    async def get_batch_details(self, page_titles: list[str], search_language: str = "en") -> list[ArticleDetailsResult]:
        """ This function retrieves details for a batch of articles in parallel. """
        return [self.get_details(page_title, search_language) for page_title in page_titles]


    def get_details(self, page_title: str, search_language: str = "en") -> ArticleDetailsResult:
        """ This function retrieves details for a given article. """
        return ArticleDetailsResult(editors=42, 
                                    last_update="23.02.2024", 
                                    outgoing_links=42)