
from langchain_core.pydantic_v1 import BaseModel as LangChainModel
from langchain_core.language_models.chat_models import BaseChatModel
from langchain_core.messages import SystemMessage, HumanMessage
from structlog import getLogger
from wme.on_demand import EnterpriseAPIResponse

from app.utils.langchain_helper import RawStructuredOutput, ResultWithTokens
from app.utils.settings_types import SystemEnum
import json

logger = getLogger(__name__)

SYSTEM = """
You will be given a true or false claim along with list of Wikipedia article titles and their descriptions.
You may also be given some contextual information to help you understand the claim (e.g. the title of the website the claim came from).
Your job is to identify the articles that the claim would make sense to be mentioned in.
Disambiguation articles or set-index articles are not relevant.

Return a JSON object with a single key "articles" containing a list of article titles from most relevant to least. Exclude irrelevant articles. Return an empty list if nothing is relevant. Only return a single output.

For example:

Claim: MIT researchers have developed a glassy, amber-like polymer that can be used for long-term storage of DNA, whether entire human genomes or digital files such as photos
Articles: {"DNA digital data storage": "Process of encoding and decoding binary data to and from synthesized strands of DNA", "Massachusetts Institute of Technology": "Land-grant university in Cambridge, Massachusetts", "DNA": "Molecule that carries genetic information", "Polymer": "Substance composed of macromolecules with repeating structural units"}
Output: {"articles":["DNA digital data storage", "DNA"]}

Claim: The effects of internet addiction were seen throughout multiple neural networks in the brains of adolescents
Articles: {"Internet": "Global system of connected computer networks", "Brain": "Organ central to the nervous system", "Internet addiction disorder": "Excessive internet use that causes psychological disorders", "Psychological effects of Internet use": "Psychological effects of Internet use", "Digital media use and mental health": "Effect of digital media on the mental health of its users"}
Output: {"articles":["Psychological effects of Internet use", "Internet addiction disorder", "Digital media use and mental health"]}

Claim: Justin Timberlake was arraigned on one count of driving while intoxicated and released from police custody in New York, according to his attorney.
Articles: {"Justin Timberlake discography": "Justin Timberlake discography", "Justin Timberlake videography":"Justin Timberlake videography", "Cry Me a River (Justin Timberlake song)":"2002 single sung by Justin Timberlake"}
Output: {"articles":[]}

Claim: They can carry diseases and parasites that can leave behind in your home through their feces.
Context: Top 15 Facts About Crickets
Articles: {"Cricket": "Insect of the Gryllidae family", "House cricket": "Species of cricket", "Cricket (disambiguation)": "Disambiguation page for the term cricket", "Cricket (sport)": "Sport of cricket", "Welfare of farmed insects": "Form of animal welfare"}
Output: {"articles":["Cricket", "House cricket"]}
"""

class ArticlePickerResult(LangChainModel):
    """ This class represents a keyword extraction result """
    articles: list[str]


class ArticlePicker:
    def __init__(self, model: BaseChatModel):
        self._base_model = model
        self._model = model.with_structured_output(ArticlePickerResult, method="json_mode", include_raw=True)

    async def extract(self, claim: str, articles: list[EnterpriseAPIResponse], context: str | None = None, src: SystemEnum = SystemEnum.AddAFactAPI) -> ResultWithTokens[ArticlePickerResult]:

        articles = dict({article.name : article.description or article.name for article in articles})
        messages = [
            SystemMessage(content=SYSTEM),
            HumanMessage(content=f"Claim: {claim}\nArticles: {json.dumps(articles)}"),
        ]

        if context:
            messages.append(HumanMessage(content=f"Context: {context}"))

        model_type = self._base_model._llm_type

        logger.info("Starting article picking",
            source=src,
            destination=SystemEnum.ChatGPT,
            model_type=model_type,
            query=claim,
            context=context,
            articles=[a for a in articles.keys()])
        try:
            res = await self._model.ainvoke(messages)
            res = ResultWithTokens.from_result(RawStructuredOutput.parse_obj(res))
            logger.info("Finished article extraction",
                source=SystemEnum.ChatGPT,
                destination=src,
                model_type=model_type,
                info=res.dict())
            
            return res
        except Exception as e:
            logger.error("Error while invoking article picker", error=e, claim=claim, context=context)
            raise e