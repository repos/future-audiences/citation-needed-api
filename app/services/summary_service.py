
from langchain_core.pydantic_v1 import BaseModel as LangChainModel
from langchain_core.language_models.chat_models import BaseChatModel
from langchain_core.messages import SystemMessage, HumanMessage
from structlog import getLogger

from app.utils.langchain_helper import RawStructuredOutput, ResultWithTokens
from app.utils.settings_types import SystemEnum

logger = getLogger(__name__)

SYSTEM = """
You will be given a true or false claim taken from an article and optionally some contextual information from the same article.
Your task is to summarize the claim into just a few words suitable for a headline. Keep the summary concise and to the point. Do not include any details. It does not need to be a complete sentence.
Return a json object with the key "summary" and the value being the summary.

For example:

Claim: MIT researchers have developed a glassy, amber-like polymer that can be used for long-term storage of DNA, whether entire human genomes or digital files such as photos
Context: A new polymer developed by MIT researchers can store digital data in DNA
Output: {"summary": "Amber-like polymer for DNA storage"}

Claim: Enhanced rock weathering could put a substantial dent in that quantity, a team of researchers in the United States reported last year in Earth’s Future. If applied to all arable lands on the planet, the method could remove up to 215 billion tons over the next 75 years, according to the team’s computer simulations. That’s a fifth of the IPCC’s maximum estimate for needed removals.
Context: How powdered rock could help slow climate change
Output: {"summary": "Slowing climate change with rock weathering"}

Claim: The judge overseeing Donald Trump's New York criminal trial on Tuesday approved a delay of the former president's sentencing after his lawyers asked for more time to argue that the Supreme Court's immunity decision calls for a new trial.
Output: {"summary": "Delay in Trump's sentencing"}

Claim: A US federal judge has refused prosecutors’ request to prohibit the maker of the 1990s rap classic Bling Bling “from promoting and glorifying future gun violence/murder” in songs and at concerts while on supervised release from prison, saying such a restriction could violate his constitutional right to free speech. But the artist known as BG must provide the government with copies of any songs he writes moving forward
Context: Rapper BG ordered to have all future songs approved by US government
Output: {"summary": "BG to provide government with future songs"}

Claim: “About 75% of cannabis consumed in the state comes from illegal sources,” the Times reported in 2022, citing industry figures. “They blame taxes, too much regulation and a failure to tackle illegal competition, which is free from red tape and able to offer cannabis at much lower prices.”
Context: Calif. pot industry owes $732m in taxes - Most of it is owed by businesses that have already failed.
Output: {"summary": "Illegal sources dominate California cannabis market"}


"""

class FactSummarizerResult(LangChainModel):
    summary: str


class FactSummarizer:
    def __init__(self, model: BaseChatModel):
        self._base_model = model
        self._model = model.with_structured_output(FactSummarizerResult, method="json_mode", include_raw=True)

    async def extract(self, claim: str, context: str | None = None, src: SystemEnum = SystemEnum.AddAFactAPI) -> ResultWithTokens[FactSummarizerResult]:

        messages = [
            SystemMessage(content=SYSTEM),
            HumanMessage(content=f"Claim: {claim}"),
        ]

        if context:
            messages.append(HumanMessage(content=f"Context: {context}"))

        model_type = self._base_model._llm_type

        logger.info("Starting summarization",
            source=src,
            destination=SystemEnum.ChatGPT,
            model_type=model_type,
            query=claim,
            context=context)

        res = await self._model.ainvoke(messages)
        res = ResultWithTokens.from_result(RawStructuredOutput.parse_obj(res))
        logger.info("Finished summarization",
            source=SystemEnum.ChatGPT,
            destination=src,
            model_type=model_type,
            info=res.dict())
        
        return res
