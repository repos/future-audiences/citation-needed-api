""" Feature flagging service for Unleash """

from functools import lru_cache
from app.services.interfaces import FeatureFlaggingService

from structlog import getLogger
from UnleashClient import UnleashClient

from app import constants
from app.utils.settings import get_settings


logger = getLogger(__name__)


class UnleashFeatureFlaggingService(FeatureFlaggingService):
    """ Feature flagging service for Unleash """
    def __init__(self, api_url: str, instance_id: str):
        self.unleash_client = UnleashClient(
            url=api_url,
            app_name="production",
            instance_id=instance_id,
            disable_metrics=True,
            disable_registration=True,
            refresh_interval=900,
        )
        self.unleash_client.initialize_client()

    def _is_enabled(self, feature_name: str, context: dict, fallback_function=None) -> bool:
        """ Checks if a feature is enabled """
        if fallback_function:
            return self.unleash_client.is_enabled(feature_name, context, fallback_function)
        else:
            return self.unleash_client.is_enabled(feature_name, context)

    def api_is_enabled(self) -> bool:
        """ Checks if we are accepting requests to the API """
        return self._is_enabled(constants.IS_ENABLED,
                               {},
                               lambda feature_name, context: False)

    def show_fact_check_result(self) -> bool:
        """ Checks if we want to show the fact check result"""
        return self._is_enabled(constants.SHOW_FACT_CHECK_RESULT,
                               {},
                               lambda feature_name, context: False)
    
    def show_fact_check_explanation(self) -> bool:
        """ Checks if we want to show the fact check explanation"""
        return self._is_enabled(constants.SHOW_FACT_CHECK_EXPLANATION,
                               {},
                               lambda feature_name, context: False)

    def user_is_on_blocklist(self, user_id: str) -> bool:
        """ Checks if the user is on the blocklist """
        return self._is_enabled(constants.ON_BLOCKLIST,
                               {"userId": user_id},
                               lambda feature_name, context: False)
    
    def use_groq(self) -> bool:
        """ Checks if we want to use GROQ """
        return self._is_enabled(constants.USE_GROQ,
                               {},
                               lambda feature_name, context: False)
    
    def use_openai(self) -> bool:
        """ Checks if we want to use OpenAI """
        return self._is_enabled(constants.USE_OPENAI,
                               {},
                               lambda feature_name, context: False)

@lru_cache()
def get_feature_flagging_service() -> UnleashFeatureFlaggingService:
    """ Gets the cached feature flagging service """
    settings = get_settings()
    return UnleashFeatureFlaggingService(api_url=settings.gitlab_feature_flag_api_url,
                                         instance_id=settings.gitlab_feature_flag_instance_id)
