import asyncio
from app.services.find_articles import ArticleFinder
from app.services.heuristic_section_ranker import HeuristicSectionRanker, RankedSection
from app.services.interfaces import ArticleDetailsService, FeatureFlaggingService
from structlog import getLogger
from typing import Any, Self
from app.services.keyword_extractor import KeywordExtractor
from app.services.quote_extractor import QuoteExtractionResult, QuoteExtractor
from app.services.quote_matcher import QuoteMatch, quote_matcher
from app.services.wme_service import WmeService
from app.utils.settings_types import  SystemEnum
from app.models.parameters import ArticleDetailsResult, FactCheckerArticleResponse, FactCheckerRequestInfo, FactCheckerResponse
from wme.on_demand import EnterpriseAPIResponse
import traceback

import json

from app.utils.strings import trim_to_space

logger = getLogger(__name__)

class CitationFinderService:
    """Fetches articles from Wikipedia and interacts with ChatGPT to find citations for a given query."""
    def __init__(self,
                 kw_extractor: KeywordExtractor,
                 quote_extractor: QuoteExtractor,
                 wme_service: WmeService,
                 search_service: ArticleFinder,
                 article_details_service: ArticleDetailsService,
                 section_ranker: HeuristicSectionRanker,
                 feature_flagging_service: FeatureFlaggingService,
                 top_n_sections: int = 3):
        self.kw_extractor = kw_extractor
        self.wme_service = wme_service
        self.search_service = search_service
        self.article_details_service = article_details_service
        self.feature_flagging_service = feature_flagging_service
        self.section_ranker = section_ranker
        self.quote_extractor = quote_extractor
        self.top_n_sections = top_n_sections


    @staticmethod
    def make_no_result_response() -> FactCheckerResponse:
        return FactCheckerResponse(result="no_articles", explanation="No matching Wikipedia pages found.", article=None)
    
    @staticmethod
    def make_article_response(resp: EnterpriseAPIResponse, section: RankedSection | None, quote_match: QuoteMatch | None, details: ArticleDetailsResult | None) -> FactCheckerArticleResponse:

        page_url = resp.url
        url = page_url
        section_url = url
        title = resp.name
        section_title = section.section_title if section else ""
        if section_title == "Abstract":
            section_title = ""
        snippet = quote_match.quote if quote_match else trim_to_space(resp.abstract, 200)
        references = details.outgoing_links if details else "0"
        contributors = details.editors if details else "0"
        last_updated = details.last_update if details else "0"
        return FactCheckerArticleResponse(url=url, page_url=page_url, section_url=section_url, title=title, section=section_title, snippet=snippet, references=references, contributors=contributors, last_updated=last_updated)

    @classmethod
    def make_no_citations_response(cls: Self, resp: EnterpriseAPIResponse, details: ArticleDetailsResult | None) -> FactCheckerResponse:
        article_response = cls.make_article_response(resp, None, None, details)
        return FactCheckerResponse(result="no_result", explanation="No matching citations found.", article=article_response)

    @classmethod
    def make_citations_response(cls: Self, resp: EnterpriseAPIResponse, quote_result: QuoteExtractionResult, section: RankedSection, quote: QuoteMatch, details: ArticleDetailsResult | None) -> FactCheckerResponse:
        article_response = cls.make_article_response(resp, section, quote, details)
        return FactCheckerResponse(result=quote_result.result, explanation=quote_result.explanation, article=article_response)


    async def stream_citation_finding(self, info: FactCheckerRequestInfo) -> Any:
        logger.info("User request received",
                    source=SystemEnum.User,
                    destination=SystemEnum.CitationNeededAPI,
                    info=info.model_dump())
        
        yield json.dumps({"current_step": "Checking out the statement"}) + '\n'

        try:
            # Step 1: extract keywords from the selection
            kw_result_res = await self.kw_extractor.extract(info.selection)
            kw_result = kw_result_res.result
            yield json.dumps({"current_step": "Picking out key words"}) + '\n'

            # Step 2: find articles that match the keywords
            articles = await self.search_service.find_articles(kw_result)

            if not articles:
                result_json = self.make_no_result_response().model_dump()
                logger.info("No articles found response",
                        source=SystemEnum.CitationNeededAPI,
                        destination=SystemEnum.User,
                        info=result_json)
                yield json.dumps(result_json)
                return

            # in the background start fetching article details
            titles = [a.title for a in articles]
            page_details = asyncio.ensure_future(self.article_details_service.get_batch_details(titles))
                        
            yield json.dumps({"current_step": "Looking up on Wikipedia"}) + '\n'

            # Step 3: get the articles
            article_contents = await self.wme_service.fetch_articles([a.title for a in articles], search_language="en")

            # Step 4: select the top sections from the articles
            top_sections = self.section_ranker.rank(kw_result, article_contents, limit=self.top_n_sections)

            yield json.dumps({"current_step": "Finding something for you"}) + '\n'

            extracted_quote_res = await self.quote_extractor.extract(info.selection, top_sections)
            extracted_quote = extracted_quote_res.result

            selected_quote: QuoteMatch | None = None
            if extracted_quote.result and extracted_quote.selected_quote:
                selected_quote = quote_matcher(extracted_quote.selected_quote, top_sections)

            if (selected_quote is None):
                # no matching quote found. respond with a generic on result on the first hit
                top_result_title = top_sections[0].title
                matching_article = article_contents[top_result_title]
                page_details_dict = dict(zip(titles, await page_details))

                result_json = self.make_no_citations_response(matching_article, page_details_dict.get(top_result_title)).model_dump()
                logger.info("No quote found response",
                        source=SystemEnum.CitationNeededAPI,
                        destination=SystemEnum.User,
                        info=result_json)
                yield json.dumps(result_json)
                return


            matched_section = selected_quote.section

            matched_title = matched_section.title
            page_details_dict = dict(zip(titles, await page_details))
            page_details = page_details_dict.get(matched_title)
            matched_article = article_contents[matched_title]
            resp = self.make_citations_response(matched_article, extracted_quote, matched_section, selected_quote, page_details)
            result_json = resp.model_dump()
            logger.info("Successfull response",
                        source=SystemEnum.CitationNeededAPI,
                        destination=SystemEnum.User,
                        extracted_quote=extracted_quote,
                        info=result_json)
            yield json.dumps(result_json)

            return
        # Handle all other errors - return a generic error message but log in detail
        except Exception as e:
            result_json = {"result": "error", "explanation": "Something unexpected happened."}
            
            logger.info("Error response",
                        source=SystemEnum.CitationNeededAPI,
                        destination=SystemEnum.User,
                        info={"result": "error", 
                              "explanation": "Something unexpected happened.",
                              "traceback": traceback.format_exc(),
                              "error": str(e)})
            
            yield json.dumps(result_json)
            return