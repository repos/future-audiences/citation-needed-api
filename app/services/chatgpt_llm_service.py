""" This module contains the ChatGPTLLMService class for interacting with the ChatGPT API."""
import json
from typing import Any
from typing import List, Tuple

from app.models.llm.interaction import KeywordExtactionMetadata, KeywordExtractionResult, QuoteSelectionMetadata, QuoteSelectionResult, SectionSelectionMetadata, SectionSelectionResult
from app.prompt_constants import CONTEXT_PROMPT_ADDENDUM, KEYWORD_EXTRACTION_INSTRUCTIONS, QUOTE_SELECTION_INSTRUCTIONS, SECTION_SELECTION_INSTRUCTIONS
from app.services.errors import LLMInteractionError
from app.utils.settings_types import LLMInteractionStrategy, LLMProviderEnum, SystemEnum
from openai import OpenAI
from groq import Groq

from structlog import getLogger
from time import sleep

from app.services.interfaces import LLMService
from app.utils.string_matching import get_best_match
from app.utils.settings import get_settings

logger = getLogger(__name__)

class ChatGPTLLMService(LLMService):
    """ Wraps the ChatGPT API """

    def __init__(self, 
                 interaction_strategy: LLMInteractionStrategy=LLMInteractionStrategy.FreeForm,
                 model_version: str="gpt-3.5-turbo-0125",
                 seed: int | None = None):
        """ Initializes the ChatGPTLLmService class """
        settings = get_settings()
        self.interaction_strategy = interaction_strategy
        if model_version.startswith("gpt"):
            self.api_key = settings.openai_api_key
            self.client = OpenAI(api_key=self.api_key)
        elif model_version.startswith("llama"):
            self.api_key = settings.groq_api_key
            if self.api_key is None:
                raise ValueError("No Groq API key provided")
            self.client = Groq(api_key=self.api_key)
        else:
            raise ValueError(f"Unknown model version: {model_version}")
        self.use_assistant_api = interaction_strategy == LLMInteractionStrategy.ToolCalls
        self.model_version = model_version
        self.seed = seed
        if self.use_assistant_api:
            self.assistant_id = settings.openai_assistant_id
            self.assistant = self.client.beta.assistants.retrieve(self.assistant_id)
            self.thread = self.client.beta.threads.create()

    def _run_chat_gpt_thread(self, run_id: str) -> Any:
        while True:
            run = self.client.beta.threads.runs.retrieve(
                thread_id=self.thread.id,
                run_id=run_id
            )
            # sleep one second
            if run.status != "in_progress":
                break
            sleep(1)

        return run
    
    def _get_filtered_free_form_mesages(self) -> List[str]:
        messages = self.client.beta.threads.messages.list(
            thread_id=self.thread.id
        )
        # message = messages.data[0].content[0].text.value
        messages = messages.data
        messages = [message for message in messages if message.role == "assistant"]
        messages = [message.content[0] for message in messages]
        messages = [message for message in messages if message.type == "text"]
        messages = [message for message in messages if message.text is not None]
        messages = [message for message in messages if message.text.value != ""]
        messages = [message.text.value for message in messages]
        messages = [message.replace("\n", "") for message in messages]
        return messages
    
    def _normalize_response_from_section_selection(self, tocs: List[Tuple[str, List[str]]], entries: List) -> List[Tuple[str, str]]:
        # create a mapping of page titles to section titles
        page_map = {}
        for toc in tocs:
            page_map[toc[0]] = toc[1]
        
        selected_sections = []

        # Sometimes, ChatGPT does not exactly adhere to the instructions. There are multiple
        # typical errors that can occur. We will try to handle them here.
        # 1. The selection does not include the page title
        # In this case, we add the page title to all sections that are unique
        entries_without_page = [entry for entry in entries if len(entry) == 1]
        entries_without_page = [(entry[0]) for entry in entries_without_page]

        # 2. The page title is not in the list of known page
        entries_with_unknown_page = [entry for entry in entries if len(entry) == 2]
        entries_with_unknown_page = [entry for entry in entries_with_unknown_page if entry[0] not in [toc[0] for toc in tocs]]
        # Remove the page title from the entries
        entries_with_unknown_page = [(entry[1]) for entry in entries_with_unknown_page]

        # 3. The section is placed in the wrong page
        # In this case, we check if we can find the section in some other page
        entries_to_verify = [entry for entry in entries if len(entry) == 2]
        entries_to_verify = [entry for entry in entries_to_verify if entry[0] in [toc[0] for toc in tocs]]
        entries_to_verify = [entry for entry in entries_to_verify if entry[1] not in page_map[entry[0]]]
        # Remove the page title from the entries
        entries_to_verify = [(entry[1]) for entry in entries_to_verify]

        # For both 1. and 2., we will add the page title to the section title
        # and check if the combination is in the list of known sections
        entries_to_lookup = entries_without_page + entries_with_unknown_page + entries_to_verify

        for entry in entries_to_lookup:
            page_id = None
            multiple_page_ids = False
            for toc in tocs:
                if entry in toc[1]:
                    if page_id is not None:
                        multiple_page_ids = True
                    page_id = toc[0]
            if page_id is not None and not multiple_page_ids:
                selected_sections.append((page_id, entry))

        # All entries with known page titles that have a valid section title are added
        
        valid_entries = [entry for entry in entries if len(entry) == 2]
        valid_entries = [entry for entry in valid_entries if entry[0] in [toc[0] for toc in tocs]]
        valid_entries = [entry for entry in valid_entries if entry[1] in page_map[entry[0]]]
        
        selected_sections += valid_entries

        # Limit the number of sections to 3
        selected_sections = selected_sections[:3]

        return selected_sections

    def _extract_response_from_section_selection(self,
                                                 tocs: List[Tuple[str, List[str]]] ) -> List[Tuple[str, str]]:
        messages = self._get_filtered_free_form_mesages()

        if len(messages) == 0:
            raise LLMInteractionError("ChatGPT failed to return any messages")
        
        messages = [message for message in messages if message.startswith("[[")]
        messages = [message for message in messages if message.endswith("]]")]
        messages = [message[2:-2] for message in messages]
        messages = [message.replace("], [", "],[") for message in messages]
        if len(messages) == 0:
            raise LLMInteractionError("ChatGPT failed to return any sections")
        if len(messages) > 1:
            raise LLMInteractionError("ChatGPT returned too many messages")
        
        entries = messages[0].split("],[")
        entries = [entry.replace("','", '","') for entry in entries]
        entries = [entry.replace("', '", '","') for entry in entries]
        entries = [entry.replace('", "', '","') for entry in entries]
        entries = [entry[1:-1] for entry in entries]
        entries = [tuple(entry.split('","')) for entry in entries]

        return self._normalize_response_from_section_selection(tocs=tocs, entries=entries)
    
    def _extract_response_from_quote_selection(self, 
                                               sections: List[Tuple[str, str, str]],
                                               message: str) -> Tuple[str, str, str]:


        best_matching_section_index = -1
        best_match = None
        for i in range(len(sections)):
            section = sections[i]
            match = get_best_match(message, section[2], step=5, case_sensitive=False)
            if best_match is None or match[1] > best_match[1]:
                best_match = match
                best_matching_section_index = i
        
        if best_matching_section_index == -1:
            raise LLMInteractionError("ChatGPT failed to return a quote")
        
        # Find letter-by-letter best match
        best_match = get_best_match(message, sections[best_matching_section_index][2], step=1, case_sensitive=True)

        if best_match[1] < 0.5:
            raise LLMInteractionError("ChatGPT returned an unexpected or bad quote")
        
        matching_section = sections[best_matching_section_index]

        return [matching_section[0], matching_section[1], best_match[0]]

    def extract_keyword(self,
                       selection: str,
                       context: str | None = None,
                       *,
                       instructions: str | None = None,
                       overwrite_model: str | None = None) -> KeywordExtractionResult:
        """ This function extracts keywords from the passed selection. """

        used_model = overwrite_model if overwrite_model is not None else self.model_version

        metadata = metadata=KeywordExtactionMetadata(term=selection, 
                                                     language=self.search_language, 
                                                     provider=LLMProviderEnum.ChatGPT,
                                                     model=used_model)
        
        logger.info("Starting keyword extraction",
                    source=SystemEnum.CitationNeededAPI,
                    destination=SystemEnum.ChatGPT,
                    info=metadata.dict())

        used_instructions = instructions if instructions is not None else KEYWORD_EXTRACTION_INSTRUCTIONS
        # add the context addendum if it is provided
        used_instructions = used_instructions + (CONTEXT_PROMPT_ADDENDUM if context else "")


        unparsed_return_value = None
        synonyms = None
        search_terms = None
        tokens_in = None
        tokens_out = None

        try:
            if self.use_assistant_api:
                self.run = self.client.beta.threads.runs.create(
                    thread_id=self.thread.id,
                    assistant_id=self.assistant.id,
                    instructions=used_instructions
                )
                                
                user_content="Verifiy: " + selection 
                self.client.beta.threads.messages.create(
                    thread_id=self.thread.id,
                    role="user",
                    content=user_content
                )

                self.run = self._run_chat_gpt_thread(run_id=self.run.id)
                tokens_in = round(len(used_instructions) / 4) + round(len(user_content) / 4)

            if self.interaction_strategy == LLMInteractionStrategy.JSONMode \
                or self.interaction_strategy == LLMInteractionStrategy.FreeForm:

                messages = [
                    {"role": "system", "content": used_instructions},
                ]
                if context:
                    messages.append({"role": "user", "content": "Context: " + context})
                messages.append({"role": "user", "content": "Claim: " + selection})

                if self.interaction_strategy == LLMInteractionStrategy.JSONMode:
                    response = self.client.chat.completions.create(
                        model=used_model,
                        response_format={ "type": "json_object" },
                        messages=messages,
                        seed=self.seed,
                        temperature=0.0,
                        max_tokens=128
                    )
                else:
                    response = self.client.chat.completions.create(
                        model=used_model,
                        messages=messages,
                        seed=self.seed,
                        temperature=0.0
                    )
                keyword_text = response.choices[0].message.content
                unparsed_return_value = keyword_text

                tokens_in = response.usage.prompt_tokens
                tokens_out = response.usage.completion_tokens

                try:
                    keywords_json = json.loads(keyword_text)
                except json.JSONDecodeError as e:
                    raise Exception("ChatGPT failed to return a valid JSON response. Message was " + str(response.choices[0].message)) from e
               
                if "keywords" not in keywords_json:
                    raise LLMInteractionError("ChatGPT failed to return a search term")
                keywords = keywords_json["keywords"]
                if "synonyms" in keywords_json:
                    synonyms = keywords_json["synonyms"]
                if "search_term" in keywords_json:
                    search_terms = [keywords_json["search_term"]]
            
            if keywords is None:
                raise LLMInteractionError("ChatGPT failed to return a search term")
        except LLMInteractionError as e:
            logger.error("ChatGPT failed to extract a keyword",
                         source=SystemEnum.ChatGPT,
                         destination=SystemEnum.CitationNeededAPI,
                         error=e)
            return KeywordExtractionResult(keywords=None,
                                           unparsed_return_value=unparsed_return_value,
                                           metadata=metadata,
                                           error=str(e),
                                           tokens_in=tokens_in,
                                           tokens_out=tokens_out)

        result = KeywordExtractionResult(keywords=keywords,
                                         synonyms=synonyms,
                                         search_terms=search_terms,
                                         unparsed_return_value=unparsed_return_value,
                                         metadata=metadata,
                                         error=None,
                                         tokens_in=tokens_in,
                                         tokens_out=tokens_out)
        
        logger.info("Finished keyword extraction",
                    source=SystemEnum.ChatGPT,
                    destination=SystemEnum.CitationNeededAPI,
                    info=result.dict())
        
        return result

    def select_sections(self,
                       tocs: List[Tuple[str, List[Tuple[str, float]]]],
                       instructions: str | None = None,
                       selection: str | None = None,
                       overwrite_model: str | None = None) -> SectionSelectionResult:
        """ This function requests sections from a list of table of contents. """

        used_model = overwrite_model if overwrite_model is not None else self.model_version

        tocs_without_weighting = []
        for entry in tocs:
            key = entry[0]
            sections = entry[1]
            sections_str_list = [section[0] for section in sections]
            tocs_without_weighting.append((key, sections_str_list))

        output_str = json.dumps(tocs_without_weighting)

        metadata = SectionSelectionMetadata(tocs=tocs_without_weighting, 
                                            parsed_tocs=output_str,
                                            language=self.search_language, 
                                            provider=LLMProviderEnum.ChatGPT,
                                            model=used_model)
        
        logger.info("Starting section selection",
                    source=SystemEnum.CitationNeededAPI,
                    destination=SystemEnum.ChatGPT,
                    info=metadata.dict())

        used_instructions = instructions if instructions is not None else SECTION_SELECTION_INSTRUCTIONS
        unparsed_return_value = None
        tokens_in = None
        tokens_out = None
        try:
            if sum(len(sections) for (page, sections) in tocs_without_weighting) <= 3:
                # If the total number of sections is less than or equal to 3 we can just return all sections
                sections = sum([[(page, section) for section in sections] for (page, sections) in tocs_without_weighting], [])
                unparsed_return_value = None
            elif self.interaction_strategy == LLMInteractionStrategy.ToolCalls:
            # Send the article list to ChatGPT
                self.client.beta.threads.runs.submit_tool_outputs(
                    thread_id=self.thread.id,
                    run_id=self.run.id,
                    tool_outputs=[{
                        'tool_call_id': self.tool_call.id,
                        'output': output_str 
                    }]
                )
                self.run = self._run_chat_gpt_thread(run_id=self.run.id)
                unparsed_return_value = self._get_filtered_free_form_mesages()[0]
                tokens_in = round(len(output_str) / 4)
                tokens_out = round(len(unparsed_return_value) / 4)
                sections = self._extract_response_from_section_selection(tocs=tocs_without_weighting)

            elif self.interaction_strategy == LLMInteractionStrategy.JSONMode \
                or self.interaction_strategy == LLMInteractionStrategy.FreeForm:
                if selection is None:
                    raise LLMInteractionError("No selection was provided for JSON Mode.")
                
                user_content = "Claim: " + selection
                if self.interaction_strategy == LLMInteractionStrategy.JSONMode:
                    response = self.client.chat.completions.create(
                        model=used_model,
                        response_format={ "type": "json_object" },
                        messages=[
                            {"role": "system", "content": used_instructions},
                            {"role": "system", "content": output_str},
                            {"role": "user", "content": user_content}
                        ],
                        seed=self.seed,
                        temperature=0.0
                    )
                else:
                    response = self.client.chat.completions.create(
                        model=used_model,
                        messages=[
                            {"role": "system", "content": used_instructions},
                            {"role": "system", "content": output_str},
                            {"role": "user", "content": user_content}
                        ],
                        seed=self.seed,
                        temperature=0.0
                    )
                sections_text = response.choices[0].message.content
                unparsed_return_value = sections_text
                tokens_in = response.usage.prompt_tokens
                tokens_out = response.usage.completion_tokens

                # The expected response looks like this:
                # {"Impact of the Eras Tour": ["Seismic activity"], "Lumen Field": ["Facility contracts and naming rights","Seismic experiments"]}
                # We will parse the response and return the sections as Tuple[str, str]
                sections = []

                try:
                    sections_json = json.loads(sections_text)
                except json.JSONDecodeError as e:
                    raise Exception("ChatGPT failed to return a valid JSON response. Message was " + str(response.choices[0].message)) from e

                for key in sections_json:
                    for section in sections_json[key]:
                        sections.append((key, section))

                # Normalize the result to handle inconsitencies in the response
                sections = self._normalize_response_from_section_selection(tocs=tocs_without_weighting, entries=sections)
            
            # Handle errors around the section selection
            if len(sections) == 0:
                raise LLMInteractionError("ChatGPT failed to return any sections: " + unparsed_return_value)
        
        except LLMInteractionError as e:
            logger.error("ChatGPT failed to select sections",
                         source=SystemEnum.ChatGPT,
                         destination=SystemEnum.CitationNeededAPI,
                         error=e)
            return SectionSelectionResult(sections=None,
                                           unparsed_return_value=unparsed_return_value,
                                           metadata=metadata,
                                           error=str(e),
                                           tokens_in=tokens_in,
                                           tokens_out=tokens_out)
        
        result = SectionSelectionResult(sections=sections,
                                        metadata=metadata,
                                        unparsed_return_value=unparsed_return_value,
                                        error=None,
                                        tokens_in=tokens_in,
                                        tokens_out=tokens_out)

        logger.info("Finished section selection",
                    source=SystemEnum.ChatGPT,
                    destination=SystemEnum.CitationNeededAPI,
                    info=result.dict())
        
        return result


    def select_quote(self,
                     sections: List[Tuple[str, str, str]],
                     instructions: str | None = None,
                     selection: str | None = None,
                     context: str | None = None,
                     overwrite_model: str | None = None) -> QuoteSelectionResult:
        
        """ This function selects a quote from the passed in text passages 
        that best verifies or rejects the original statement. """

        used_model = overwrite_model if overwrite_model is not None else self.model_version

        metadata = QuoteSelectionMetadata(sections=sections, 
                                            language=self.search_language, 
                                            provider=LLMProviderEnum.ChatGPT,
                                            model=used_model)
        
        logger.info("Starting quote selection",
                    source=SystemEnum.CitationNeededAPI,
                    destination=SystemEnum.ChatGPT,
                    info=metadata.dict())
        
        used_instructions = instructions if instructions is not None else QUOTE_SELECTION_INSTRUCTIONS
        unparsed_return_value = None
        result = None
        explanation = None
        tokens_in = None
        tokens_out = None

        try:
            if self.interaction_strategy == LLMInteractionStrategy.ToolCalls:
                # Send the article list to ChatGPT
                tool_output = json.dumps(sections)
                self.client.beta.threads.runs.submit_tool_outputs(
                    thread_id=self.thread.id,
                    run_id=self.run.id,
                    tool_outputs=[{
                        'tool_call_id': self.tool_call.id,
                        'output': tool_output
                    }]
                )

                self.run = self._run_chat_gpt_thread(run_id=self.run.id)
                messages = self._get_filtered_free_form_mesages()
                likely_message = messages[0]
                unparsed_return_value=likely_message
                tokens_in = round(len(tool_output) / 4) 
                tokens_out = round(len(unparsed_return_value) / 4)

                # Handle the valid return case where no quote was selected
                if ("no_quote_selected" in unparsed_return_value):
                    return QuoteSelectionResult(quote=None,
                                                no_quote_selected=True,
                                                metadata=metadata,
                                                unparsed_return_value=unparsed_return_value)
                
                logger.info("Received selected quote",
                            source=SystemEnum.ChatGPT,
                            destination=SystemEnum.CitationNeededAPI,
                            info={"selected_quote": likely_message})
                quote = self._extract_response_from_quote_selection(sections=sections, 
                                                                    message=likely_message)
            
            elif self.interaction_strategy == LLMInteractionStrategy.JSONMode \
                or self.interaction_strategy == LLMInteractionStrategy.FreeForm:
                if selection is None:
                    raise LLMInteractionError("No selection was provided for JSON Mode.")
                section_text = json.dumps(sections)

                messages = [
                    {"role": "system", "content": used_instructions},
                    {"role": "system", "content": "Wikipedia Passages: " + section_text},
                ]
                if context:
                    messages.append({"role": "user", "content": "Context: " + context})
                messages.append({"role": "user", "content": "Claim: " + selection})

                if self.interaction_strategy == LLMInteractionStrategy.JSONMode:
                    response = self.client.chat.completions.create(
                        model=used_model,
                        response_format={ "type": "json_object" },
                        messages=messages,
                        seed=self.seed,
                        temperature=0.0,
                        max_tokens=512
                    )
                else:
                    response = self.client.chat.completions.create(
                        model=used_model,
                        messages=messages,
                        seed=self.seed,
                        temperature=0.0
                    )
                quote_text = response.choices[0].message.content
                unparsed_return_value=quote_text

                tokens_in = response.usage.prompt_tokens
                tokens_out = response.usage.completion_tokens

                # The expected response looks like this:
                # {"selected_quote": "Homeopathy is a pseudoscience – a belief that is incorrectly ..."}
                # We will parse the response and return the quote
                
                try:
                    quote_json = json.loads(quote_text)
                except json.JSONDecodeError as e:
                    raise Exception("ChatGPT failed to return a valid JSON response. Message was " + str(response.choices[0].message)) from e

                quote_json = json.loads(quote_text)
                # Handle the valid return case where no quote was selected
                if ("no_quote_selected" in quote_json and quote_json["no_quote_selected"]):
                    return QuoteSelectionResult(quote=None,
                                                no_quote_selected=True,
                                                metadata=metadata,
                                                unparsed_return_value=unparsed_return_value,
                                                tokens_in=tokens_in,
                                                tokens_out=tokens_out)
                if "selected_quote" not in quote_json:                    
                    raise LLMInteractionError("ChatGPT failed to return a quote: " + quote_text)
                
                if "result" in quote_json:
                    # Test that result is one of "correct", "incorrect" or "partially_correct"
                    if quote_json["result"] not in ["correct", "incorrect", "partially_correct"]:
                        raise LLMInteractionError("ChatGPT returned an unexpected result")
                    result = quote_json["result"]
                if "explanation" in quote_json:
                    explanation = quote_json["explanation"]
                
                likely_message = quote_json["selected_quote"]
                logger.info("Received selected quote",
                            source=SystemEnum.ChatGPT,
                            destination=SystemEnum.CitationNeededAPI,
                            info={"selected_quote": likely_message})
                quote = self._extract_response_from_quote_selection(sections=sections, message=likely_message)

        except LLMInteractionError as e:
            logger.error("ChatGPT failed to select a quote",
                         source=SystemEnum.ChatGPT,
                         destination=SystemEnum.CitationNeededAPI,
                         error=e)
            return QuoteSelectionResult(quote=None,
                                        no_quote_selected=False,
                                        unparsed_return_value=unparsed_return_value,
                                        metadata=metadata,
                                        error=str(e),
                                        result=result,
                                        explanation=explanation,
                                        tokens_in=tokens_in,
                                        tokens_out=tokens_out)
        
        result = QuoteSelectionResult(quote=quote,
                                      no_quote_selected=False,
                                      metadata=metadata,
                                      unparsed_return_value=unparsed_return_value,
                                      result=result,
                                      explanation=explanation,
                                      tokens_in=tokens_in,
                                      tokens_out=tokens_out)

        logger.info("Finished quote selection", info=result.dict())
        
        return result
