""" Cirrus search service implementation for Wikipedia Search """

import grequests
from typing import Tuple, List
# import requests
from app.models.parameters import ArticleDetailsResult, FactCheckerArticleResponse
import urllib.parse
import structlog
import re

from app import constants
from app.constants import LEAD_SECTION_PLACEHOLDER, USER_AGENT, WPRV_ARGUMENT
from app.models.search.search import SearchMetadata, WikipediaSearchResult
from app.models.search.wiki_search import (Normalization, Redirect,
                                           WikiAPIResponse, WikiPage)
from app.models.wikipedia_article import WikipediaArticle
from app.services.interfaces import SearchService
from app.utils.clean_wikitext import clean_wikitext
from app.utils.settings_types import SearchProviderEnum, SystemEnum

logger = structlog.getLogger(__name__)


class WikipediaAPI:
    BASE_URL_TEMPLATE = "https://{}.wikipedia.org/w/api.php"
    HEADERS = {'User-Agent': USER_AGENT}

    @staticmethod
    def resolve_redirects(language: str, article_titles: list[str]) -> list[WikipediaArticle]:
        base_url = WikipediaAPI.BASE_URL_TEMPLATE.format(language)
        params = {
            "action": "query",
            "titles": "|".join(article_titles),
            "redirects": True,
            "prop": "pageprops|info",
            "ppprop": "disambiguation",
            "inprop": "url",
            "format": "json",
            "formatversion": 2
        }
        response = grequests.get(base_url, params=params, timeout=100, headers=WikipediaAPI.HEADERS)
        response.raise_for_status()
        api_result = WikiAPIResponse(**response.json())

        parsed_articles = []
        for title in article_titles:
            normalized_title = WikipediaAPI._get_normalized_title(title,
                                                                  api_result.query.normalized or [],
                                                                  api_result.query.redirects or [])
            for page in api_result.query.pages:
                if page.title == normalized_title:
                    if (WikipediaAPI._is_valid_search_result(page)):
                        parsed_articles.append(WikipediaArticle.from_url(page.fullurl))  # type: ignore (checked below)
        return parsed_articles

    @staticmethod
    def _get_normalized_title(title: str, normalizations: list[Normalization], redirects: list[Redirect]) -> str:
        # Search for the title in the normalizations, and set it to the normalized title if found
        for n in normalizations:
            if n.from_ == title:
                title = n.to
                break

        # Search for the normalized title in the redirects
        for r in redirects:
            if r.from_ == title:
                title = r.to
                break

        return title

    @staticmethod
    def _is_valid_search_result(page: WikiPage) -> bool:
        return (page.ns == 0
                and page.missing is None
                and page.pageprops is None
                and page.fullurl is not None)
    
    @staticmethod
    def set_user_agent(user_agent: str):
        WikipediaAPI.HEADERS['User-Agent'] = user_agent


class WikipediaSearchService(SearchService):
    def __init__(self):
        self.user_agent = constants.USER_AGENT

    def set_user_agent(self, user_agent: str) -> None:
        self.user_agent = user_agent

    """ Wikipedia search service implementation"""
    def search_articles(self,
                       search_term: str,
                       search_language: str = "en") -> WikipediaSearchResult:
        """ Perform a search using the Cirrus search service (default search for Wikipedia )"""
        if search_language not in constants.WIKIPEDIA_LANGUAGES:
            logger.error("Language not supported, defaulting to English", language=search_language)
            search_language = "en"
        base_url = f"https://{search_language}.wikipedia.org/w/api.php"
        params = {"action": "query",
                  "generator": "search",
                  "gsrsearch": search_term,
                  "prop": "pageprops|info",
                  "gsrlimit": 3,
                  "gsrnamespace": 0,
                  "ppprop": "disambiguation",
                  "inprop": "url",
                  "format": "json",
                  "formatversion": 2}
        req_list = [grequests.get(base_url, params=params, timeout=100, headers={'User-Agent': self.user_agent})]
        resp_list = grequests.map(req_list)
        resp = resp_list[0]
        resp.raise_for_status()
        search_response = WikiAPIResponse(**resp.json())
        parsed_articles: list[WikipediaArticle] = []
        search_results = []
        if (search_response.query is not None):
            search_results = sorted(search_response.query.pages, key=lambda x: x.index or 0)
            for page in search_results:
                if page.pageprops is None:  # only included if disambiguation property exists
                    parsed_articles.append(WikipediaArticle.from_url(page.fullurl, pageid=page.pageid))  # type: ignore (checked below)
        metadata = SearchMetadata(term=search_term,
                                  language=search_language,
                                  provider=SearchProviderEnum.Wikipedia)
        search_result = WikipediaSearchResult(original_results=search_results,
                                              parsed_results=parsed_articles,
                                              metadata=metadata)
        # Note at this point we have not hydrated article text because it is expensive
        # to compute and we don't need it here.
        logger.info("Results received from Wiki search",
                    source=SystemEnum.Wikipedia,
                    destination=SystemEnum.CitationNeededAPI,
                    info=search_result.dict())
        return search_result

    def fetch_tables_of_content(self, articles: list[WikipediaArticle], keywords: List[str], search_language: str = "en") -> List[Tuple[str, List[Tuple[str, float]]]]:
        """ Extract the table of contents from a list of Wikipedia articles """


        self.pageid_by_title = {}
        self.sectionid_by_pageid_and_title = {}
        self.section_text_by_pageid_and_title = {}
        
        # Perform the requests in parallel
        base_url = f"https://{search_language}.wikipedia.org/w/api.php"
        param_list = []
        for article in articles:
            params = {"action": "parse",
                        "pageid": article.pageid,
                        "prop": "wikitext",
                        "format": "json",
                        "formatversion": 2,
                        "disabletoc": 1}
            param_list.append(params)

        req_list = (grequests.get(base_url, params=params, timeout=100, headers={'User-Agent': self.user_agent}) for params in param_list)
        resp_list = grequests.map(req_list)

        return_value = []
        for i in range(len(articles)):
            return_value.append(self._handle_toc_response(articles[i], keywords=keywords, resp=resp_list[i]))

        return return_value
    
    def _fetch_section(self, section: Tuple[str, str], search_language: str = "en") -> Tuple[str, str, str]:

        page_id = self.pageid_by_title[section[0]]

        if (page_id is None):
            raise Exception("Page '" + section[0] + "' not found")
        
        # Sometimes, ChatGPT places the sections into the wrong page. This is a workaround to fix that.
        if (section[1] not in self.sectionid_by_pageid_and_title[page_id]):
            for key in self.sectionid_by_pageid_and_title:
                if (section[1] in self.sectionid_by_pageid_and_title[key]):
                    page_id = key
                    break
        
        # Check if the section is still not found
        if (section[1] not in self.sectionid_by_pageid_and_title[page_id]):
            raise Exception("Section '" + section[1] + "' not found")
        
        section_text = self.section_text_by_pageid_and_title[page_id][section[1]]
        return (section[0], section[1], section_text)
    
    def _handle_toc_response(self, article: WikipediaArticle, keywords: List[str], resp: any) -> Tuple[str, List[Tuple[str, float]]]:

        resp.raise_for_status()
        page_query_json = resp.json()
        page_text = page_query_json['parse']['wikitext']

        # Find all occurences of "==.*==" and split the text into sections
        section_split_indexes = [m.start() for m in re.finditer(r'==.*==', page_text)]

        self.pageid_by_title[article.title] = article.pageid

        section_texts = []
        for i in range(len(section_split_indexes)):
            # handle the first section
            if (i == 0):
                section_texts.append(page_text[:section_split_indexes[i]])
            # handle the last section
            elif (i == len(section_split_indexes) - 1):
                section_texts.append(page_text[section_split_indexes[i]:])
            else:
                section_texts.append(page_text[section_split_indexes[i-1]:section_split_indexes[i]])

        self.sectionid_by_pageid_and_title[article.pageid] = {}
        self.section_text_by_pageid_and_title[article.pageid] = {}

        article_title_lower = article.title.lower()

        sections = []
        # go over all sections, extract the title and find all matching keywords
        for section_index in range(len(section_texts)):
            section_text = section_texts[section_index]
            section_title = re.search(r'===*(.*)==', section_text)
            if (section_title is not None):
                section_title = section_title.group(1)
                # replace all remaining = at the end of the section title
                section_title = re.sub(r'=*$', '', section_title)
                section_title = section_title.strip()
                section_text = re.sub(r'====.*====', '', section_text)
                section_text = re.sub(r'===.*===', '', section_text)
                section_text = re.sub(r'==.*==', '', section_text)
            else:
                section_title = LEAD_SECTION_PLACEHOLDER

            section_text = clean_wikitext(section_text)
            section_text_lower = section_text.lower()

            self.sectionid_by_pageid_and_title[article.pageid][section_title] = section_index
            self.section_text_by_pageid_and_title[article.pageid][section_title] = section_text

            # Exclude unhelpful sections from rankings
            if section_title.lower() in ["references", "external links", "further reading", "see also", "notes", "additional sources", "sources", "bibliography"]:
                sections.append((section_title, 0))
                continue
            
            section_keyword_match_value = 0
            # For all keywords, check if they are in the section
            for keyword in keywords:
                # keywords can have weights added to them. If a keyword ends with :x, it is a keyword with a weight of x
                keyword_text = keyword.lower()
                keyword_weight = 1
                if (keyword_text[-2] == ":"):
                    keyword_text = keyword_text[:-2]
                    keyword_weight = int(keyword[-1])

                if (keyword_text in section_text_lower or keyword_text in article_title_lower):
                    section_keyword_match_value += 2 * keyword_weight
                else:
                    partial_keyword = keyword_text.split(" ")
                    for partial in partial_keyword:
                        if (partial in section_text_lower or partial in article_title_lower):
                            section_keyword_match_value += (1/len(partial_keyword)) * keyword_weight
                
            sections.append((section_title, section_keyword_match_value))
        return (article.title, sections)

    def fetch_sections(self, sections: list[Tuple[str, str]], search_language: str = "en") -> List[Tuple[str, str, str]]:
        """ Retrieves individual sections from articles """
        
        return [self._fetch_section(section, search_language=search_language) for section in sections]
    
    def create_result_article(self, 
                              selected_quote: Tuple[str, str, str], 
                              article_details: ArticleDetailsResult | None) -> FactCheckerArticleResponse:
        """ Creates a FactCheckerArticleResponse from a section """
        
        if (selected_quote[0] not in self.pageid_by_title):
            raise Exception("LLM selected a page that was not found in the search results")
        
        pageid = self.pageid_by_title[selected_quote[0]]
        
        
        if (selected_quote[1] not in self.sectionid_by_pageid_and_title[pageid]):
            raise Exception("LLM selected a section that was not found in the search results")
        
        title_escaped = selected_quote[0].replace(' ', '_')

        base_url = f"http://en.wikipedia.org/wiki/{title_escaped}?wprov=" + WPRV_ARGUMENT

        references = '?'
        contributors = '?'
        last_updated = '?'
        if (article_details is not None):
            references = article_details.outgoing_links
            contributors = article_details.editors
            last_updated = article_details.last_update

        escaped_quote = urllib.parse.quote(selected_quote[2]).replace('-','%2D').replace('&','%26').replace(',', '%2C')

        # Don't return a specific section URL if the section is the lead section
        if (selected_quote[1] == LEAD_SECTION_PLACEHOLDER):
            # Try to highlight the text directly - this might not work if there is a quote or
            # other special characters in the text
            section_url = base_url
            if (escaped_quote != "" and escaped_quote is not None):
                section_url = str(base_url + "#:~:text=" + escaped_quote)

            return FactCheckerArticleResponse(
                                    url=base_url,
                                    page_url=base_url,
                                    section_url=section_url,
                                    title=selected_quote[0],
                                    snippet=selected_quote[2],
                                    references=references,
                                    contributors=contributors,
                                    last_updated=last_updated)
    

        # Try to highlight the text directly - this might not work if there is a quote or
        # other special characters in the text. In such cases, the fallback of just jumping to the
        # section will still work.
        section_url = base_url + "#" + (selected_quote[1].replace(' ', '_'))
        if (escaped_quote != "" and escaped_quote is not None):
            section_url += str(":~:text=" + escaped_quote)

        return FactCheckerArticleResponse(
                                    url=base_url,
                                    page_url=base_url,
                                    section_url=section_url,
                                    title=selected_quote[0],
                                    section=selected_quote[1],
                                    snippet=selected_quote[2],
                                    references=references,
                                    contributors=contributors,
                                    last_updated=last_updated)
    
    def _get_pageid_and_sections(self, article_title: str, search_language: str = "en") -> str:
        base_url = f"https://{search_language}.wikipedia.org/w/api.php"
        params = {"action": "parse",
                  "page": article_title,
                  "prop": "sections",
                  "format": "json",
                  "formatversion": 2}
        response = grequests.get(base_url, params=params, timeout=100, headers={'User-Agent': self.user_agent})
        response.raise_for_status()
        response_json = response.json()
        
        self.pageid_by_title = {}
        self.sectionid_by_pageid_and_title = {}
        self.pageid_by_title[response_json['parse']['title']] = response_json['parse']['pageid']
        self.sectionid_by_pageid_and_title[response_json['parse']['pageid']] = {}
        for section in response_json['parse']['sections']:
            self.sectionid_by_pageid_and_title[response_json['parse']['pageid']][section['line']] = section['index']

        # Always add Abstract as the first section. Add to both toc and our internal map
        self.sectionid_by_pageid_and_title[response_json['parse']['pageid']][LEAD_SECTION_PLACEHOLDER] = 0
    

    def _set_internal_metadata_dicts(self, 
                                     pageid_by_title: dict[str, str], 
                                     sectionid_by_pageid_and_title: dict[str, dict[str, str]]):
        self.pageid_by_title = pageid_by_title
        self.sectionid_by_pageid_and_title = sectionid_by_pageid_and_title
