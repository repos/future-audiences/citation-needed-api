""" This module contains the interface for a generic search service. """

from typing import Dict, List, Tuple
from abc import ABC, abstractmethod
from app.models.llm.interaction import KeywordExtractionResult, QuoteSelectionResult, SectionSelectionResult
from app.models.parameters import ArticleDetailsResult, FactCheckerArticleResponse

from app.models.search.search import SearchResult
from app.models.wikipedia_article import WikipediaArticle


class SearchService(ABC):
    """ This abstract base class is an interface for a generic search service. """
    @abstractmethod
    def set_user_agent(self, user_agent: str) -> None:
        """ This function sets the user agent that is used to make requests """

    @abstractmethod
    def search_articles(self,
                       search_term: str,
                       search_language: str = "en") -> SearchResult:
        """ This function performs a search and returns a list of results
        or None if there was an error. """

    @abstractmethod
    def fetch_tables_of_content(self, 
                                articles: list[WikipediaArticle], 
                                keywords: List[str], 
                                search_language: str = "en") -> List[Tuple[str, List[Tuple[str, float]]]]:
        """ Extract the table of contents from a list of Wikipedia articles """

    @abstractmethod
    def fetch_sections(self, 
                       sections: list[Tuple[str, str]], 
                       search_language: str = "en") -> List[Tuple[str, str, str]]:
        """ This function fetches the sections from the passed in list of sections. """
    
    @abstractmethod
    def create_result_article(self, 
                              selected_quote: Tuple[str, str, str], 
                              article_details: ArticleDetailsResult | None) -> FactCheckerArticleResponse:
        """ Creates a FactCheckerArticleResponse from a section """

class LLMService(ABC):
    """ This abstract base class is an interface for a generic LLM service. """
    def set_search_language(self, search_language: str) -> None:
        """ This function sets the search language """
        self.search_language = search_language

    @abstractmethod
    def extract_keyword(self,
                       selection: str,
                       context: str | None) -> KeywordExtractionResult:
        """ This function extracts keywords from the passed selection. """

    @abstractmethod
    def select_sections(self,
                       tocs: Dict[str, List[Tuple[str, str]]],
                       selection: str) -> SectionSelectionResult:
        """ This function requests sections from a list of table of contents. """

    @abstractmethod
    def select_quote(self,
                     sections: List[Tuple[str, str, str]],
                     selection: str,
                     context: str | None) -> QuoteSelectionResult:
        """ This function selects a quote from the passed in text passages 
        that best verifies or rejects the original statement. """

        
class ArticleDetailsService(ABC):
    """ This abstract base class is an interface for a generic article details retrieval service. """

    @abstractmethod
    async def get_batch_details(self, page_titles: list[str], search_language: str = "en") -> list[ArticleDetailsResult]:
        """ This function retrieves details for a batch of articles in parallel. """

        
class FeatureFlaggingService(ABC):
    """ This abstract base class is an interface for a generic feature flagging service. """

    @abstractmethod
    def api_is_enabled(self) -> bool:
       """ Checks if we are accepting requests to the API """

    @abstractmethod
    def show_fact_check_result(self) -> bool:
        """ Checks if we want to show the fact check result"""

    @abstractmethod
    def show_fact_check_explanation(self) -> bool:
        """ Checks if we want to show the fact check explanation"""

    @abstractmethod
    def user_is_on_blocklist(self, user_id: str) -> bool:
        """ Checks if the user is on the blocklist """
