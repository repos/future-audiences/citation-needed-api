from pydantic import BaseModel
from app.services.heuristic_section_ranker import RankedSection
from app.utils.string_matching import get_best_match


class QuoteMatch(BaseModel):
    quote: str
    section: RankedSection


def quote_matcher(quote: str, sections: list[RankedSection]) -> QuoteMatch | None:
    """
    Given a quote and a list of sections, find the best match for the quote in the sections.
    Returns a QuoteMatch object if a match is found, otherwise returns None.
    """
    best_matching_section_index = -1
    best_match = None
    for i, section in enumerate(sections):
        match = get_best_match(quote, section.section_text, step=5, case_sensitive=False)
        if best_match is None or match[1] > best_match[1]:
            best_match = match
            best_matching_section_index = i
    
    if best_matching_section_index == -1:
        return None
    
    # Find letter-by-letter best match
    best_match, best_match_score = get_best_match(quote, sections[best_matching_section_index].section_text, step=1, case_sensitive=True)

    if best_match_score < 0.5:
        return None
    
    matching_section = sections[best_matching_section_index]

    return QuoteMatch(quote=best_match, section=matching_section)
