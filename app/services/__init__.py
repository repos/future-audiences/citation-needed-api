from functools import lru_cache
from app.services.chatgpt_llm_service import ChatGPTLLMService
from app.services.wikipedia_search_service import WikipediaSearchService


@lru_cache()
def get_wikipedia_search_service():
    return WikipediaSearchService()

@lru_cache()
def get_chatpgt_llm_service():
    return ChatGPTLLMService()
