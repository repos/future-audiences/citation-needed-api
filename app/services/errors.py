""" This module contains shared error classes """

class LLMInteractionError(Exception):
    """Exception raised when an error with a LLM interaction occurs"""
    def __init__(self, message):
        super().__init__(message)