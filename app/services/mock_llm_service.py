""" This module contains the MockLLMService class for interacting with the ChatGPT API."""
from typing import List, Tuple

from app.models.llm.interaction import KeywordExtactionMetadata, KeywordExtractionResult, QuoteSelectionMetadata, QuoteSelectionResult, SectionSelectionMetadata, SectionSelectionResult
from app.services.errors import LLMInteractionError
from app.utils.settings_types import LLMProviderEnum

from structlog import getLogger

from app.services.interfaces import LLMService
from app.utils.string_matching import get_best_match

logger = getLogger(__name__)

class MockLLMService(LLMService):
    """ Wraps the ChatGPT API """

    def __init__(self, do_text_lookup: bool = False):
        """ Initializes the MockLLMService class """
        self.do_text_lookup = do_text_lookup

    def extract_keyword(self,
                       selection: str,
                       context: str | None = None,
                       *,
                       instructions: str | None = None) -> KeywordExtractionResult:
        """ This function extracts keywords from the passed selection. """
        metadata = metadata=KeywordExtactionMetadata(term=selection, 
                                                     language=self.search_language, 
                                                     provider=LLMProviderEnum.MockLLM,
                                                     model="mock-model")

        result = KeywordExtractionResult(keywords=["Taylor Swift", "The Eras Tour", "Lumen Field", "Seattle", "earthquake"],
                                         unparsed_return_value="Taylor Swift The Eras Tour Lumen Field Seattle earthquake",
                                         search_terms=["Taylor Swift", "The Eras Tour", "Lumen Field", "Seattle", "earthquake"],
                                         metadata=metadata,
                                         error=None)
        
        return result

    def select_sections(self,
                       tocs: List[Tuple[str, List[str]]],
                       instructions: str | None = None,
                       selection: str | None = None) -> SectionSelectionResult:
        """ This function requests sections from a list of table of contents. """
        
        tocs_without_weighting = []
        for entry in tocs:
            key = entry[0]
            sections = entry[1]
            sections_str_list = [section[0] for section in sections]
            tocs_without_weighting.append((key, sections_str_list))

        output = []
        for entry in tocs_without_weighting:
            key = entry[0]
            sections = entry[1]
            sections_str = "\",\"".join(sections)
            toc_str = f'"{key}": ["{sections_str}"]'
            output.append(toc_str)
        output_str = "{" +", ".join(output) + "}"

        sections = [('Impact of the Eras Tour', 'Seismic activity'),
                    ('Impact of the Eras Tour', 'References'),
                    ('Lumen Field', 'Seismic experiments')]

        metadata = SectionSelectionMetadata(tocs=tocs_without_weighting, 
                                            parsed_tocs=output_str,
                                            language=self.search_language, 
                                            provider=LLMProviderEnum.MockLLM,
                                            model="mock-model")
        
        
        result = SectionSelectionResult(sections=sections,
                                        metadata=metadata,
                                        unparsed_return_value="{\"Impact of the Eras Tour\": [\"Seismic activity\",\"References\"], \"Lumen Field\": [\"Seismic experiments\",\"Facility contracts and naming rights\"]}",
                                        error=None)

        return result


    def _extract_response_from_quote_selection(self, 
                                               sections: List[Tuple[str, str, str]],
                                               message: str) -> Tuple[str, str, str]:


        best_matching_section_index = -1
        best_match = None
        for i in range(len(sections)):
            section = sections[i]
            match = get_best_match(message, section[2], step=5, case_sensitive=False)
            if best_match is None or match[1] > best_match[1]:
                best_match = match
                best_matching_section_index = i
        
        if best_matching_section_index == -1:
            raise LLMInteractionError("MockLLM failed to return a quote")
        
        # Find letter-by-letter best match
        best_match = get_best_match(message, sections[best_matching_section_index][2], step=1, case_sensitive=True)

        if best_match[1] < 0.5:
            raise LLMInteractionError("MockLLM returned an unexpected or bad quote")
        
        matching_section = sections[best_matching_section_index]

        return [matching_section[0], matching_section[1], best_match[0]]

    def select_quote(self,
                     sections: List[Tuple[str, str, str]],
                     instructions: str | None = None,
                     selection: str | None = None) -> QuoteSelectionResult:
        
        """ This function selects a quote from the passed in text passages 
        that best verifies or rejects the original statement. """

        metadata = QuoteSelectionMetadata(sections=sections, 
                                            language=self.search_language, 
                                            provider=LLMProviderEnum.MockLLM,
                                            model="mock-model")
        mock_quote = "Her Eras Tour (2023–2024) and its accompanying concert film became the highest-grossing tour and concert film of all time, respectively. "
        
        if self.do_text_lookup:
            quote = self._extract_response_from_quote_selection(sections=sections, message=mock_quote)
        else:
            quote = ("Taylor Swift", "Lead Section", mock_quote)

        result = QuoteSelectionResult(quote=quote,
                                      no_quote_selected=False,
                                      metadata=metadata,
                                      unparsed_return_value="{\"selected_quote\": \"During the tour's stop in Seattle at Lumen Field on July 22 and 23, 2023, fans in the area caused seismic activity equivalent to a 2.3-magnitude earthquake, nicknamed the \\\"Swift Quake\\\".\"}")
        
        return result
