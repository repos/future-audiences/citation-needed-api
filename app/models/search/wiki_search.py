from typing import Any
from pydantic import BaseModel, Field


class WikiPage(BaseModel):
    ns: int
    title: str
    missing: bool | None = None
    contentmodel: str | None = None
    pagelanguage: str | None = None
    pagelanguagehtmlcode: str | None = None
    pagelanguagedir: str | None = None
    pageid: int | None = None
    touched: str | None = None
    lastrevid: int | None = None
    length: int | None = None
    fullurl: str | None = None
    editurl: str | None = None
    canonicalurl: str | None = None
    pageprops: dict[str, str] | None = None
    index: int | None = None


class Normalization(BaseModel):
    fromencoded: str
    from_: str = Field(..., alias="from")
    to: str


class Redirect(BaseModel):
    from_: str = Field(..., alias="from")
    to: str


class WikiQuery(BaseModel):
    pages: list[WikiPage] | None = None
    normalized: list[Normalization] | None = None
    redirects: list[Redirect] | None = None


class WikiAPIResponse(BaseModel):
    warnings: dict[str, dict[str, str]] | None = None
    batchcomplete: bool
    continue_: dict[str, Any] | None = Field(alias="continue", default=None)
    query: WikiQuery | None = None