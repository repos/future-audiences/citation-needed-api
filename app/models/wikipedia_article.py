""" This module contains the WikipediaArticle class representing a generic Wikipedia article. """

import re
from urllib.parse import parse_qs, unquote, urlencode, urlparse, urlunparse

import bs4
import requests

from app.constants import USER_AGENT, WPRV_ARGUMENT
from pydantic import BaseModel, Field


class WikipediaArticle(BaseModel):
    """ A wrapper around a Wikipedia article """
    url: str = Field(description="The URL of the article")
    title: str = Field(description="The title of the article")
    language: str = Field(description="The language of the article text")
    text: str | None = Field(default=None, description="The text of the article")
    pageid: int = Field(default=None, description="The pageid of the article")

    @staticmethod
    def from_url(url: str, pageid: str) -> "WikipediaArticle":
        """ Create a WikipediaArticle from a URL """
        url = WikipediaArticle._add_query_param(url, {"wprov": [WPRV_ARGUMENT]})
        parsed_url = WikipediaArticle._parse_url(url)
        title = parsed_url["title"]
        language = parsed_url["language"]
        return WikipediaArticle(url=url, title=title, language=language, text=None, pageid=pageid)

    @staticmethod
    def title_from_url(url: str) -> str:
        """ Get the title from a Wikipedia URL """
        return WikipediaArticle._parse_url(url)["title"]

    @staticmethod
    def _add_query_param(url: str, param: dict[str, list[str]]) -> str:
        """ Adds a query parameter to the given url """
        url_parts = urlparse(url)
        query_params = parse_qs(url_parts.query)

        # Check if parameter is already present
        if not any(item in query_params.items() for item in param.items()):
            query_params.update(param)

        new_query_string = urlencode(query_params, doseq=True)
        new_url_parts = url_parts._replace(query=new_query_string)
        return urlunparse(new_url_parts)

    @staticmethod
    def _parse_url(url: str) -> dict[str, str]:
        """ Get the title and language from a Wikipedia URL """
        parsed_url = urlparse(url)
        # Title
        path_parts = parsed_url.path.split('/')
        page_title = path_parts[-1] if len(path_parts) > 1 else ''
        page_title = unquote(page_title).replace('_', ' ')
        # Language
        netloc_parts = parsed_url.netloc.split('.')
        lang = netloc_parts[0] if len(netloc_parts) > 1 else ''
        return {"title": page_title, "language": lang}

    def _html(self):
        base_url = f"https://{self.language}.wikipedia.org/w/api.php"
        params = {"action": "parse",
                  "page": self.title,
                  "prop": "text",
                  "format": "json",
                  "formatversion": 2}
        resp = requests.get(base_url, params=params, timeout=100, headers={'User-Agent': USER_AGENT})
        resp.raise_for_status()
        html = resp.json().get("parse", {}).get("text", "")
        return html

    def populate_text(self, paragraph_count: int = 12) -> None:
        """ Get the text of the article """
        html = self._html()
        soup = bs4.BeautifulSoup(html, "html.parser")
        pars = soup.select("div.mw-parser-output > p")
        non_empty_pars = [par.text.strip() for par in pars if par.text.strip()][:paragraph_count]
        text = "\n".join(non_empty_pars)
        infobox = soup.select_one("table.infobox")
        if infobox:
            text = infobox.get_text(" ", strip=True).replace("\n", ", ")[:1000] + "\n" + text
        # Regular expression pattern to match any number in brackets (i.e. [1], [2], [3], etc.)
        pattern = r"\[\d+\]"
        text = re.sub(pattern, "", text)
        self.text = text
