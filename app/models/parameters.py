""" This file contains the models used by the API """

from pydantic import BaseModel, Field

from app import constants


class ChatGPTRequest(BaseModel):
    """ The request sent to the plugin """
    query_language: str = Field(default="en", description="The language of the query.  Defaults to English.")
    query: str = Field(
        description="Used to as a search term to find relevant articles on Wikipedia",
        max_length=constants.MAX_INPUT_LENGTH,
        min_length=constants.MIN_INPUT_LENGTH)
    original_user_input: str = Field(description="The full text of the original user input")

class FactCheckerRequestInfo(BaseModel):
    selection: str = Field(description="The selection to fact check")
    client_id: str = Field(description="The unique id of the client")
    site_category: str = Field(description="The category of the site")
    site_reliability: str = Field(description="The reliability of the site")
    context: str | None = Field(description="Contextual infrmation about the selection")
    manual_article: str | None = Field(description="The URL of a manually selected article to use for fact checking", default=None)


class FactCheckerArticleResponse(BaseModel):
    url: str = Field(description="The URL of the article")
    page_url: str = Field(description="The URL of the article")
    section_url: str = Field(description="The URL of the section")
    title: str = Field(description="The title of the article")
    section: str | None = Field(description="The section selected from the article", default=None)
    snippet: str = Field(description="The snippet selected from the article")
    references: int | str = Field(description="The number of references in the article")
    contributors: int | str = Field(description="The number of contributors to the article")
    last_updated: str = Field(description="The date the article was last updated")

class FactCheckerResponse(BaseModel):
    """ The response from the plugin """
    result: str = Field(description="The result of the fact check, one of 'true', 'false', or 'unknown'")
    explanation: str = Field(description="The explanation of the fact check")
    article: FactCheckerArticleResponse | None = Field(description="The article that was used to fact check the statement")

class ArticleDetailsResult(BaseModel):
    """ This class represents a result of a details lookup """
    outgoing_links: int
    editors: int
    last_update: str