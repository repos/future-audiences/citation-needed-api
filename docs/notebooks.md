# Notebooks

## Context

Running analyses in Jupyter notebooks requires a bit of setup.

## Setup

The additional dependencies for notebooks are installed as [package extras](https://python-poetry.org/docs/pyproject/#extras). These are spelled out in the `[tool.poetry.extras]` section of our `pyproject.toml` file. You will need to install them separately, via the following command:

```bash
poetry install -E notebooks
```

This will install all dependencies specified under "notebooks" in the `pyproject.toml` file.

## Running and Debugging

To run and debug notebooks directly within VSCode:

1. Activate the python virtualenv for your poetry environment with :

```bash
poetry shell
```

1. Open the project within VSCode with `code .`

1. Run the below to start the notebook server in a way that can be debugged within VSCode itself:

```bash
jupyter notebook --no-browser --NotebookApp.allow_origin_pat=https://.\*vscode-cdn\.net
```

1. Open the `ipynb` file, then "Select Kernel" -> "Python Environments" and ensure your activated virual environment is selected

1. You're ready to go!
