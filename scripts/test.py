import grequests # noqa: F401
import pytest
import sys
import typer
app = typer.Typer()

@app.command()
def run_tests():
    """Run the tests."""
    sys.exit(pytest.main(["tests/"]))
    
if __name__ == "__main__":
    run_tests()
