import datetime
import os.path
import os
import logging

from google.auth.transport.requests import Request
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload
from google.oauth2 import service_account

import typer

# If modifying these scopes, delete the file token.json.
SCOPES = ["https://www.googleapis.com/auth/drive"]

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())

app = typer.Typer()

def connect():  
  """Shows basic usage of the Drive v3 API.
  Prints the names and ids of the first 10 files the user has access to.
  """
  creds = None
  # The file token.json stores the user's access and refresh tokens, and is
  # created automatically when the authorization flow completes for the first
  # time.
  # if os.path.exists("../google_token.json"):

  SERVICE_ACCOUNT_FILE = os.environ.get("TOOL_DATA_DIR") + '/google_service_account.json'

  creds = service_account.Credentials.from_service_account_file(
    SERVICE_ACCOUNT_FILE, scopes=SCOPES)

  logger.info(creds)
  if creds and creds.expired and creds.refresh_token:
    creds.refresh(Request())
  return creds

def move_file_to_gdrive(creds):
  try:
    service = build("drive", "v3", credentials=creds)

    folder_id = os.environ.get('LOG_GDRIVE_FOLDER_ID')

    # Upload a file to the specified folder
    LOG_FILE_PATH = os.environ.get("TOOL_DATA_DIR") + '/logs/default.log'
    TARGET_FILE_NAME = "server_log_" + str(datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")) + ".ndjson"
    file_metadata = {"name": TARGET_FILE_NAME, "parents": [folder_id]}
    media = MediaFileUpload(
        LOG_FILE_PATH, mimetype="application/x-ndjson", resumable=True
    )
    # pylint: disable=maybe-no-member
    file = (
        service.files()
        .create(body=file_metadata, media_body=media, fields="id", 
              supportsAllDrives=True)
        .execute()
    )
    logger.info(f'File ID: "{file.get("id")}".')
    return file.get("id")

  except HttpError as error:
    # TODO(developer) - Handle errors from drive API.
    logger.error(f"An error occurred: {error}")

@app.command()
def export_logs():
    move_file_to_gdrive(connect())


if __name__ == "__main__":
    app()
