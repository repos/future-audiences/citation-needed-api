[tool.poetry]
name = "citation-needed-api"
version = "0.1.0"
description = "An API utilizing ChatGPT for finding citations from Wikipedia"
authors = ["Elias Rut <erut-ctr@wikimedia.org>"]
readme = "README.md"
packages = [{include = "app"}]

[tool.poetry.dependencies]
python = "^3.11"
requests = "^2.30.0"
fastapi = "^0.111.0"
uvicorn = "^0.22.0"
structlog = "^23.1.0"
asgi-correlation-id = "^4.2.0"
html5lib = "^1.1"
beautifulsoup4 = "^4.12.2"
cffi = "^1.15.1"
soupsieve = "^2.4.1"
httpx = "^0.27.0"
jinja2 = "^3.1.2"
unleashclient = "^5.6.0"
locust = "^2.15.1"
google-api-python-client = "^2.94.0"
google-auth-httplib2 = "^0.1.0"
google-auth-oauthlib = "^1.0.0"
openai = "^1.8.0"
pydantic = "^2.7.4"
gunicorn = "^21.2.0"
mwparserfromhell = "^0.6.6"
wme = {git = "https://gitlab.wikimedia.org/repos/future-audiences/wme.git"}

# Notebook dependencies
jupyter = {"version" = "^1.0.0", optional = true }
pandas = {"version" = "^2.0.2", optional = true }
matplotlib = {"version" = "^3.7.2", optional = true }
scikit-learn = {"version" = "^1.4.1.post1", optional = true }
datasets = {version = "^2.18.0", optional = true}

# Script dependencies
boto3 = "^1.26.143"
typer = "^0.12.3"
torch = {version = "^2.2.1", optional = true}
grequests = "^0.7.0"
groq = "^0.5.0"
prometheus-client = "^0.20.0"
prometheus-fastapi-instrumentator = "^7.0.0"
langchain-groq = "^0.1.5"
pydantic-settings = "^2.3.1"
nltk = "^3.8.1"
langchain-openai = "^0.1.8"
langchain-community = "^0.2.5"

[tool.poetry.group.dev.dependencies]
black = "^23.3.0"
pytest = "^7.3.1"
flake8 = "^6.0.0"
ipython = "^8.14.0"
ruff = "^0.4.4"
pytest-asyncio = "^0.23.7"

[tool.poetry.scripts]
get_logs = { callable = "scripts.get_logs:app", extras = ["scripts"] }
parse_logs = { callable = "scripts.parse_logs:app", extras = ["scripts"] }
api = "app.main:main"
test = { callable = "scripts.test:app", extras = ["scripts"] }
export_logs = { callable = "scripts.export_logs:app", extras= ["scripts"]}
delete_logs = { callable = "scripts.delete_logs:app", extras= ["scripts"]}

[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"

[tool.poetry.extras]
notebooks = ["jupyter", "pandas", "matplotlib"]
scripts = ["boto3", "typer"]

# See more here: https://github.com/pydantic/pydantic/issues/1961
[tool.pylint.'MESSAGES CONTROL']
extension-pkg-whitelist = "pydantic"